//! Concrete [`rowan`] syntax tree produced by the parser.

use num_enum::{IntoPrimitive, TryFromPrimitive, TryFromPrimitiveError};
use rowan::Language;

use crate::lexer::TokenKind;

#[derive(
    Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord, IntoPrimitive, TryFromPrimitive,
)]
#[repr(u16)]
pub enum SyntaxKind {
    // Tokens
    Bool,
    Number,
    Symbol,
    LParen,
    RParen,
    Plus,
    Hyphen,
    Star,
    Slash,
    DoubleEqual,
    BangEqual,
    DoubleAmpersand,
    DoublePipe,
    LAngle,
    RAngle,
    LAngleEqual,
    RAngleEqual,
    Colon,
    RightArrow,
    Backslash,
    Dot,
    LetKw,
    InKw,
    Equal,
    IfKw,
    ThenKw,
    ElseKw,
    LBrace,
    RBrace,
    LBracePipe,
    RBracePipe,
    Comma,
    RecKw,
    AndKw,
    CaseKw,
    OfKw,
    ForAllKw,

    Whitespace,
    Error,

    // Nodes
    Root,
    InfixOp,
    PrefixOp,
    Application,
    Operator,
    Lambda,
    Let,
    LetBinding,
    Conditional,
    BoolLit,
    NumberLit,
    Identifier,
    ParenthesizedExpr,
    ForAll,
    Rec,

    Record,
    Field,
    FieldType,
    RecordAccess,
    Variant,
    Case,
    CaseArm,
    Pattern,
}

impl SyntaxKind {
    pub fn is_trivia(self) -> bool {
        let kind: u16 = self.into();
        TokenKind::try_from(kind)
            .map(TokenKind::is_trivia)
            .unwrap_or(false)
    }
}

impl From<TokenKind> for SyntaxKind {
    fn from(kind: TokenKind) -> Self {
        let kind: u16 = kind.into();
        Self::try_from(kind).unwrap()
    }
}

impl TryInto<TokenKind> for SyntaxKind {
    type Error = TryFromPrimitiveError<TokenKind>;

    fn try_into(self) -> Result<TokenKind, <Self as TryInto<TokenKind>>::Error> {
        let kind: u16 = self.into();
        TokenKind::try_from(kind)
    }
}

impl Into<rowan::SyntaxKind> for SyntaxKind {
    fn into(self) -> rowan::SyntaxKind {
        rowan::SyntaxKind(self.into())
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Lang {}

impl Language for Lang {
    type Kind = SyntaxKind;

    fn kind_from_raw(rowan::SyntaxKind(kind): rowan::SyntaxKind) -> Self::Kind {
        SyntaxKind::try_from(kind).unwrap()
    }

    fn kind_to_raw(kind: Self::Kind) -> rowan::SyntaxKind {
        rowan::SyntaxKind(kind.into())
    }
}

pub type SyntaxNode = rowan::SyntaxNode<Lang>;
pub type SyntaxNodeChildren = rowan::SyntaxNodeChildren<Lang>;
pub type SyntaxToken = rowan::SyntaxToken<Lang>;
pub type SyntaxElement = rowan::SyntaxElement<Lang>;
