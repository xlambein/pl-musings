use std::collections::HashMap;

use crate::{
    hir::{self, Expr, Identifier, RichExpr, Span},
    types,
};

use super::{eval, make_env, ClosureBody, Env, EvalError, StrictOrLazy, Value};

fn eval_ident<I>(env: &Env, ident: I) -> Result<Value, EvalError>
where
    I: Into<Identifier>,
{
    eval(
        env,
        RichExpr::new(Expr::Identifier(ident.into()), Span::None),
    )
}

fn int_to_real(env: &Env) -> Result<Value, EvalError> {
    let arg0 = eval_ident(env, "arg0")?;

    match arg0 {
        Value::Integer(arg0) => Ok(Value::Real(arg0 as _)),
        _ => panic!("type error"),
    }
}

fn real_to_int(env: &Env) -> Result<Value, EvalError> {
    let arg0 = eval_ident(env, "arg0")?;

    match arg0 {
        Value::Real(arg0) => Ok(Value::Integer(arg0 as _)),
        _ => panic!("type error"),
    }
}

fn sin(env: &Env) -> Result<Value, EvalError> {
    let arg0 = eval_ident(env, "arg0")?;

    match arg0 {
        Value::Real(arg0) => Ok(Value::Real(arg0.sin())),
        _ => panic!("type error"),
    }
}

fn log(env: &Env) -> Result<Value, EvalError> {
    let arg0 = eval_ident(env, "arg0")?;
    let arg1 = eval_ident(env, "arg1")?;

    match (arg0, arg1) {
        (Value::Real(value), Value::Real(base)) => Ok(Value::Real(value.log(base))),
        _ => panic!("type error"),
    }
}

fn pow(env: &Env) -> Result<Value, EvalError> {
    let arg0 = eval_ident(env, "arg0")?;
    let arg1 = eval_ident(env, "arg1")?;

    match (arg0, arg1) {
        (Value::Real(value), Value::Real(base)) => Ok(Value::Real(base.powf(value))),
        _ => panic!("type error"),
    }
}

fn make_builtin(arity: usize, func: fn(&Env) -> Result<Value, EvalError>) -> Value {
    assert!(arity > 0);
    Value::Closure {
        arg: Identifier::from(format!("arg{}", arity - 1)),
        env: Env::default(),
        body: ClosureBody::Builtin {
            arity: arity - 1,
            func: Box::new(func),
        },
    }
}

pub fn env() -> Env {
    let mut env = make_env([
        // Builtins
        (
            Identifier::from("int_to_real"),
            make_builtin(1, int_to_real),
        ),
        (
            Identifier::from("real_to_int"),
            make_builtin(1, real_to_int),
        ),
        (Identifier::from("sin"), make_builtin(1, sin)),
        (Identifier::from("log"), make_builtin(2, log)),
        (Identifier::from("pow"), make_builtin(2, pow)),
        // Consts
        (Identifier::from("tau"), Value::Real(std::f64::consts::TAU)),
    ]);

    // Functions
    env.insert(Identifier::from("id"), parse_and_eval(&env, r#"\x. x"#));
    env.insert(
        Identifier::from("not"),
        parse_and_eval(&env, r#"\x. if x then false else true"#),
    );
    env.insert(
        Identifier::from("flip"),
        parse_and_eval(&env, r#"\f.\a.\b. f b a"#),
    );
    env.insert(
        Identifier::from("abs_real"),
        parse_and_eval(&env, r#"\x. if x < 0.0 then -x else x"#),
    );
    env.insert(
        Identifier::from("abs_int"),
        parse_and_eval(&env, r#"\x. if x < 0 then -x else x"#),
    );

    env
}

fn parse_and_eval(env: &Env, buf: &str) -> StrictOrLazy {
    use rowan::ast::AstNode;

    let (node, errors) = crate::parser::parse(buf);
    if !errors.is_empty() {
        panic!("{}", errors.join("\n"));
    }
    let hir = crate::hir::lower(crate::ast::Root::cast(node).expect("no root node"));
    StrictOrLazy::Strict(eval(env, hir).expect("eval failed"))
}

macro_rules! mk_type {
    ($first:literal) => {
        types::Type::Var(crate::types::index_to_letters($first))
    };
    ($first:ident) => {
        types::Type::Concrete(types::ConcreteType::$first)
    };
    ($first:literal $(-> $rest:tt)+) => {
        types::Type::Concrete(types::AbsType::Fn {
            lhs: Box::new(mk_type!($first)),
            rhs: Box::new(mk_type!($($rest)->+)),
        })
    };
    ($first:ident $(-> $rest:tt)+) => {
        types::Type::Concrete(types::ConcreteType::Fn {
            lhs: Box::new(mk_type!($first)),
            rhs: Box::new(mk_type!($($rest)->+)),
        })
    };
}

pub fn type_env() -> HashMap<hir::Identifier, types::Type> {
    let mut env = HashMap::from([
        // Builtins
        (Identifier::from("int_to_real"), mk_type!(Integer -> Real)),
        (Identifier::from("real_to_int"), mk_type!(Real -> Integer)),
        (Identifier::from("sin"), mk_type!(Real -> Real)),
        (Identifier::from("log"), mk_type!(Real -> Real -> Real)),
        (Identifier::from("pow"), mk_type!(Real -> Real -> Real)),
        // Consts
        (Identifier::from("tau"), mk_type!(Real)),
    ]);

    // Functions
    env.insert(
        Identifier::from("id"),
        parse_and_type_check(&env, r#"\x. x"#),
    );
    env.insert(
        Identifier::from("not"),
        parse_and_type_check(&env, r#"\x. if x then false else true"#),
    );
    env.insert(
        Identifier::from("flip"),
        parse_and_type_check(&env, r#"\f.\a.\b. f b a"#),
    );
    env.insert(
        Identifier::from("abs_real"),
        parse_and_type_check(&env, r#"\x. if x < 0.0 then -x else x"#),
    );
    env.insert(
        Identifier::from("abs_int"),
        parse_and_type_check(&env, r#"\x. if x < 0 then -x else x"#),
    );

    env
}

fn parse_and_type_check(env: &HashMap<hir::Identifier, types::Type>, buf: &str) -> types::Type {
    use rowan::ast::AstNode;

    let (node, errors) = crate::parser::parse(buf);
    if !errors.is_empty() {
        panic!("{}", errors.join("\n"));
    }
    let hir = crate::hir::lower(crate::ast::Root::cast(node).expect("no root node"));
    crate::typecheck::type_check(&env, &hir).expect("type check failed")
}
