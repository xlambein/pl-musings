//! Interpreter for the language.

use std::{
    collections::{BTreeMap, HashMap},
    fmt,
    rc::{Rc, Weak},
};

use crate::hir::*;

pub mod stdlib;

pub type Env = HashMap<Identifier, StrictOrLazy>;

pub fn make_env(bindings: impl IntoIterator<Item = (Identifier, Value)>) -> Env {
    bindings
        .into_iter()
        .map(|(ident, value)| (ident, StrictOrLazy::Strict(value)))
        .collect()
}

#[derive(Debug, PartialEq)]
pub struct EvalError {
    pub kind: EvalErrorKind,
    pub span: Span,
}

impl EvalError {
    pub fn new(kind: EvalErrorKind, span: Span) -> Self {
        Self { kind, span }
    }

    pub fn undefined_identifier(span: Span, identifier: Identifier) -> Self {
        Self::new(EvalErrorKind::UndefinedIdentifier(identifier), span)
    }

    pub fn cyclic_definition(span: Span) -> Self {
        Self::new(EvalErrorKind::CyclicDefinition, span)
    }

    pub fn no_match(span: Span, value: Value) -> Self {
        Self::new(EvalErrorKind::NoMatch(value), span)
    }
}

#[derive(Debug, PartialEq)]
pub enum EvalErrorKind {
    UndefinedIdentifier(Identifier),
    CyclicDefinition,
    NoMatch(Value),
}

impl fmt::Display for EvalError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.kind {
            EvalErrorKind::UndefinedIdentifier(identifier) => {
                write!(f, "undefined identifier `{}`", identifier)
            }
            EvalErrorKind::CyclicDefinition => write!(f, "cyclic definition"),
            EvalErrorKind::NoMatch(value) => write!(f, "value `{value}` did not match any pattern"),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Value {
    Bool(bool),
    Integer(i64),
    Real(f64),
    Closure {
        arg: Identifier,
        env: Env,
        body: ClosureBody,
    },
    Record(BTreeMap<Identifier, Value>),
    EmptyVariant,
    Variant {
        field: Identifier,
        value: Box<Value>,
    },
}

#[derive(Debug, Clone)]
pub enum StrictOrLazy {
    Strict(Value),
    Lazy {
        // (Recursive) environment in which to evaluate the thunk.
        env: Weak<Env>,
        // Thunk to evaluate.
        expr: RichExpr,
        // Whether the thunk has already been forced.  This is needed for
        // cycle detection: if `forced` is set but `value` isn't, it means
        // `expr` contains a direct cycle.
        forced: Rc<once_cell::unsync::OnceCell<()>>,
        // Value of the thunk.
        value: Rc<once_cell::unsync::OnceCell<Value>>,
    },
}

impl StrictOrLazy {
    /// Force a lazy value to be computed.
    fn force(&self) -> Result<&Value, EvalError> {
        match self {
            StrictOrLazy::Strict(value) => Ok(value),
            StrictOrLazy::Lazy {
                env,
                expr,
                forced,
                value,
            } => {
                // Try and force the value
                if forced.set(()).is_ok() {
                    // It wasn't forced => evaluate the thunk
                    value.get_or_try_init(|| {
                        eval(
                            env.upgrade()
                                .expect("lazy value environment dropped")
                                .as_ref(),
                            expr.clone(),
                        )
                    })
                } else {
                    // It was forced => get the value or raise a cycle error
                    value
                        .get()
                        .ok_or_else(|| EvalError::cyclic_definition(expr.span))
                }
            }
        }
    }
}

/// Create an environment containing a self-referencing lazy value.
fn env_with_lazy(mut env: Env, bindings: Vec<LetBinding<RichExpr>>) -> Result<Rc<Env>, EvalError> {
    let mut values = vec![];
    let env = Rc::new_cyclic({
        let values = &mut values;
        move |this| {
            for LetBinding {
                identifier, body, ..
            } in bindings
            {
                let value = StrictOrLazy::Lazy {
                    env: this.clone(),
                    expr: body,
                    forced: Default::default(),
                    value: Default::default(),
                };
                env.insert(identifier, value.clone());
                values.push(value);
            }
            env.clone()
        }
    });

    for value in values {
        value.force()?;
    }

    Ok(env)
}

#[derive(Clone)]
pub enum ClosureBody {
    Expr(RichExpr),
    Builtin { arity: usize, func: Builtin },
}

pub type Builtin = Box<fn(&Env) -> Result<Value, EvalError>>;

impl fmt::Debug for ClosureBody {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Expr(arg0) => f.debug_tuple("RichExpr").field(arg0).finish(),
            Self::Builtin { arity, func } => f
                .debug_tuple("Builtin")
                .field(&format!(
                    "<{}-ary built-in at {:?}>",
                    arity, func as *const _
                ))
                .finish(),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Bool(l), Self::Bool(r)) => l == r,
            (Self::Integer(l), Self::Integer(r)) => l == r,
            _ => false,
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::Bool(value) => value.fmt(f),
            Value::Integer(value) => value.fmt(f),
            Value::Real(value) => write!(f, "{:?}", value), // Debug formatting to ensure we get decimals
            Value::Closure { .. } => write!(f, "<closure>"),
            Value::Record(record) => {
                write!(f, "{{")?;
                for (i, (name, value)) in record.iter().enumerate() {
                    write!(f, "{name} = {value}")?;
                    if i < record.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, "}}")
            }
            Value::EmptyVariant => write!(f, "{{||}}"),
            Value::Variant { field, value } => {
                write!(f, "{{|{field} = {value}|}}")
            }
        }
    }
}

pub fn eval(env: &Env, expr: RichExpr) -> Result<Value, EvalError> {
    match expr.expr() {
        Expr::Literal(literal) => match literal {
            Literal::Bool(value) => Ok(Value::Bool(*value)),
            Literal::Integer(value) => Ok(Value::Integer(*value)),
            Literal::Real(value) => Ok(Value::Real(*value)),
        },
        Expr::Identifier(identifier) => env
            .get(identifier)
            .map(StrictOrLazy::force)
            .transpose()?
            .cloned()
            .ok_or_else(|| EvalError::undefined_identifier(expr.span, identifier.clone())),
        Expr::InfixOp { op, lhs, rhs } => {
            use InfixOperator::*;
            use Value::*;

            let lhs = eval(env, lhs.clone())?;
            if *op == HasType {
                return Ok(lhs);
            }

            let rhs = eval(env, rhs.clone())?;
            match (lhs, rhs) {
                (Integer(lhs), Integer(rhs)) => Ok(match op {
                    Add => Integer(lhs + rhs),
                    Sub => Integer(lhs - rhs),
                    Mul => Integer(lhs * rhs),
                    Div => Integer(lhs / rhs),
                    Eq => Bool(lhs == rhs),
                    Neq => Bool(lhs != rhs),
                    Gt => Bool(lhs > rhs),
                    Lt => Bool(lhs < rhs),
                    Gte => Bool(lhs >= rhs),
                    Lte => Bool(lhs <= rhs),
                    _ => panic!("type error"),
                }),
                (Real(lhs), Real(rhs)) => Ok(match op {
                    Add => Real(lhs + rhs),
                    Sub => Real(lhs - rhs),
                    Mul => Real(lhs * rhs),
                    Div => Real(lhs / rhs),
                    Eq => Bool(lhs == rhs),
                    Neq => Bool(lhs != rhs),
                    Gt => Bool(lhs > rhs),
                    Lt => Bool(lhs < rhs),
                    Gte => Bool(lhs >= rhs),
                    Lte => Bool(lhs <= rhs),
                    _ => panic!("type error"),
                }),
                (Bool(lhs), Bool(rhs)) => Ok(match op {
                    // TODO short-circuiting
                    And => Bool(lhs && rhs),
                    Or => Bool(lhs || rhs),
                    Eq => Bool(lhs == rhs),
                    Neq => Bool(lhs != rhs),
                    _ => panic!("type error"),
                }),
                _ => panic!("type error"),
            }
        }
        Expr::PrefixOp { op, inner } => {
            let inner = eval(env, inner.clone())?;
            match inner {
                Value::Integer(value) => Ok(Value::Integer(match op {
                    PrefixOperator::Negative => -value,
                })),
                Value::Real(value) => Ok(Value::Real(match op {
                    PrefixOperator::Negative => -value,
                })),
                _ => panic!("type error"),
            }
        }
        Expr::Application { lhs, rhs } => {
            let lhs = eval(env, lhs.clone())?;
            let rhs = eval(env, rhs.clone())?;
            match (lhs, rhs) {
                (Value::Closure { arg, mut env, body }, rhs) => {
                    env.insert(arg, StrictOrLazy::Strict(rhs));
                    match body {
                        ClosureBody::Expr(expr) => eval(&env, expr),
                        ClosureBody::Builtin { arity: 0, func } => func(&env),
                        ClosureBody::Builtin { arity, func } => Ok(Value::Closure {
                            arg: format!("arg{}", arity - 1),
                            env,
                            body: ClosureBody::Builtin {
                                arity: arity - 1,
                                func,
                            },
                        }),
                    }
                }
                _ => panic!("type error"),
            }
        }
        Expr::Let {
            rec,
            bindings,
            body,
        } => {
            if *rec {
                let env = env_with_lazy(env.clone(), bindings.clone())?;
                eval(env.as_ref(), body.clone())
            } else {
                let mut env = env.clone();
                for LetBinding {
                    identifier, body, ..
                } in bindings
                {
                    env.insert(
                        identifier.clone(),
                        StrictOrLazy::Strict(eval(&env, body.clone())?),
                    );
                }
                eval(&env, body.clone())
            }
        }
        Expr::Lambda { identifier, body } => Ok(Value::Closure {
            arg: identifier.clone(),
            env: env.clone(),
            body: ClosureBody::Expr(body.clone()),
        }),
        Expr::Conditional {
            condition,
            then,
            r#else,
        } => {
            let condition = eval(env, condition.clone())?;
            match condition {
                Value::Bool(true) => eval(env, then.clone()),
                Value::Bool(false) => eval(env, r#else.clone()),
                _ => panic!("type error"),
            }
        }
        Expr::EmptyRecord => Ok(Value::Record(BTreeMap::new())),
        Expr::Record(fields) => Ok(Value::Record(
            fields
                .iter()
                .map(|(name, expr)| Ok((name.clone(), eval(env, expr.clone())?)))
                .collect::<Result<_, _>>()?,
        )),
        Expr::RecordAccess { record, field } => {
            let record = eval(env, record.clone())?;

            match record {
                Value::Record(record) => {
                    if let Some(value) = record
                        .into_iter()
                        .find(|(ident, _)| ident == field)
                        .map(|(_, value)| value)
                    {
                        Ok(value)
                    } else {
                        panic!("missing record field")
                    }
                }
                _ => panic!("type error"),
            }
        }
        Expr::EmptyVariant => Ok(Value::EmptyVariant),
        Expr::Variant { field, value } => Ok(Value::Variant {
            field: field.clone(),
            value: Box::new(eval(env, value.clone())?),
        }),
        Expr::Case { subject, arms } => {
            let subject = eval(env, subject.clone())?;
            for CaseArm { pattern, body } in arms {
                if let Some(bindings) = pattern_match(pattern, &subject) {
                    let mut env = env.clone();
                    for (identifier, value) in bindings {
                        env.insert(identifier.clone(), StrictOrLazy::Strict(value));
                    }
                    return eval(&env, body.clone());
                }
            }
            Err(EvalError::no_match(expr.span, subject))
        }

        Expr::RecordType(_) | Expr::VariantType(_) | Expr::LiteralType(_) | Expr::Rec { .. } => {
            panic!("type in HIR passed to `eval`")
        }

        Expr::Missing => panic!("missing expr"),
    }
}

fn pattern_match(pattern: &RichExpr, subject: &Value) -> Option<Vec<(Identifier, Value)>> {
    match (&*pattern.expr, subject) {
        (Expr::Literal(Literal::Bool(v0)), Value::Bool(v1)) if v0 == v1 => Some(vec![]),
        (Expr::Literal(Literal::Integer(v0)), Value::Integer(v1)) if v0 == v1 => Some(vec![]),
        (Expr::Literal(Literal::Real(v0)), Value::Real(v1)) if v0 == v1 => Some(vec![]),
        (Expr::Identifier(ident), value) => Some(vec![(ident.clone(), value.clone())]),
        (Expr::EmptyRecord, Value::Record(fields)) if fields.len() == 0 => Some(vec![]),
        (Expr::Record(fields0), Value::Record(fields1)) => {
            if fields0.len() != fields1.len() {
                return None;
            }
            let mut bindings = vec![];
            for ((ident0, expr0), (ident1, value1)) in fields0.iter().zip(fields1) {
                if ident0 != ident1 {
                    return None;
                }
                bindings.extend(pattern_match(expr0, value1)?);
            }
            Some(bindings)
        }
        (Expr::EmptyVariant, Value::EmptyVariant) => Some(vec![]),
        (
            Expr::Variant {
                field: field0,
                value: value0,
            },
            Value::Variant {
                field: field1,
                value: value1,
            },
        ) if field0 == field1 => pattern_match(value0, value1),

        (
            Expr::InfixOp { .. }
            | Expr::PrefixOp { .. }
            | Expr::Application { .. }
            | Expr::Let { .. }
            | Expr::Lambda { .. }
            | Expr::Conditional { .. }
            | Expr::Missing
            | Expr::RecordType(_)
            | Expr::RecordAccess { .. }
            | Expr::VariantType(_)
            | Expr::Case { .. }
            | Expr::LiteralType(_),
            _,
        ) => panic!("invalid pattern"),

        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use rowan::{ast::AstNode, TextRange, TextSize};

    use crate::{ast, hir, parser};

    use super::*;

    fn parse(buf: &str) -> RichExpr {
        let (node, errors) = parser::parse(buf);
        if !errors.is_empty() {
            panic!("{}", errors.join("\n"));
        }
        hir::lower(ast::Root::cast(node).expect("no root node"))
    }

    macro_rules! assert_matches {
        ($lhs:expr, $(|)? $( $pattern:pat_param )|+ $( if $guard: expr )? $(,)?) => {
            let lhs = $lhs;
            assert!(
                match lhs {
                    $( $pattern )|+ $( if $guard )? => true,
                    _ => false
                },
                "'{:?}' does not match pattern",
                lhs
            );
        }
    }

    #[test]
    fn eval_arithmetics() {
        assert_eq!(
            eval(&Env::default(), parse("1 + 2 - 3 + 4")),
            Ok(Value::Integer(4))
        );
        assert_eq!(
            eval(&Env::default(), parse("-4 / (1 + 1)")),
            Ok(Value::Integer(-2))
        );
        assert_eq!(
            eval(&Env::default(), parse("4 * 3 / 2 - 4 * (3 / 2)")),
            Ok(Value::Integer(2))
        );
        assert_matches!(
            eval(&Env::default(), parse("3.2 + -1.5 / 2.0")),
            Ok(Value::Real(value)) if (value - 2.45).abs() < 1e-6
        );
    }

    #[test]
    fn eval_logic() {
        assert_eq!(
            eval(&Env::default(), parse("1 == 2 && 1 < 2")),
            Ok(Value::Bool(false))
        );
        assert_eq!(
            eval(&Env::default(), parse("1.1 != 2.0 || -0.5 >= 0.5")),
            Ok(Value::Bool(true))
        );
        assert_eq!(
            eval(&Env::default(), parse("false && true || true")),
            Ok(Value::Bool(true))
        );
        assert_matches!(
            eval(
                &Env::default(),
                parse("let x = (1.1 - 1.1) in x * x < 0.001")
            ),
            Ok(Value::Bool(true))
        );
    }

    #[test]
    #[should_panic(expected = "type error")]
    fn eval_arithmetics_wrong_type() {
        let _ = eval(&Env::default(), parse("1 + 3.2"));
    }

    #[test]
    fn eval_identifier() {
        assert_eq!(
            eval(
                &make_env([(Identifier::from("pi"), Value::Integer(3))]),
                parse("pi - 3")
            ),
            Ok(Value::Integer(0))
        );
    }

    #[test]
    fn eval_unknown_identifier() {
        assert_eq!(
            eval(
                &make_env([(Identifier::from("pi"), Value::Integer(3))]),
                parse("tau - 6")
            ),
            Err(EvalError::undefined_identifier(
                Span::Range(TextRange::up_to(TextSize::of("tau"))),
                Identifier::from("tau")
            ))
        );
    }

    #[test]
    #[should_panic(expected = "type error")]
    fn eval_plus_wrong_type() {
        let _ = eval(&Env::default(), parse(r#"1 + \x.x"#));
    }

    #[test]
    fn eval_application() {
        assert_eq!(
            eval(&Env::default(), parse(r#"(\x.\y.\z. (x + y) * z) 1 2 3"#)),
            Ok(Value::Integer(9))
        );
    }

    #[test]
    fn eval_let() {
        assert_eq!(
            eval(&Env::default(), parse(r#"let x = 1 in x + 2"#)),
            Ok(Value::Integer(3))
        );
    }

    #[test]
    fn eval_let_sequence() {
        assert_eq!(
            eval(&Env::default(), parse(r#"let x = 1 and y = 2 in x + y"#)),
            Ok(Value::Integer(3))
        );
    }

    #[test]
    fn eval_let_shadowing() {
        assert_eq!(
            eval(&Env::default(), parse(r#"let x = 1 and x = x + 1 in x"#)),
            Ok(Value::Integer(2))
        );
    }

    #[test]
    fn eval_let_rec() {
        assert_eq!(
            eval(
                &Env::default(),
                parse(
                    r#"
let rec fib = \n.
    if n == 0 then 0
    else if n == 1 then 1
    else fib (n - 1) + fib (n - 2)
in
    fib 12
                "#
                )
            ),
            Ok(Value::Integer(144))
        );
    }

    #[test]
    fn eval_let_rec_mutual() {
        assert_eq!(
            eval(
                &Env::default(),
                parse(
                    r#"
let
rec even = \n.
        if n == 0 then true
        else if n == 1 then false
        else odd (n - 1)
and odd = \n.
        if n == 0 then false
        else if n == 1 then true
        else odd (n - 1)
in
    even 25
                "#
                )
            ),
            Ok(Value::Bool(true))
        );
    }

    #[test]
    fn eval_let_rec_cyclic() {
        // Note that this fails even though we don't use `x`.  This language
        // is *not* lazy.
        assert_eq!(
            eval(&Env::default(), parse("let rec x = x + 1 in 0")),
            Err(EvalError::cyclic_definition(Span::Range(TextRange::new(
                12.into(),
                17.into()
            ))))
        );
    }

    #[test]
    #[should_panic(expected = "type error")]
    fn eval_apply_wrong_type() {
        let _ = eval(&Env::default(), parse("1 2"));
    }

    #[test]
    fn eval_conditional() {
        assert_eq!(
            eval(&Env::default(), parse(r#"if true then 1 else 0"#)),
            Ok(Value::Integer(1))
        );
        assert_eq!(
            eval(&Env::default(), parse(r#"if false then 1 else 0"#)),
            Ok(Value::Integer(0))
        );
    }

    #[test]
    fn eval_builtins() {
        assert_matches!(
            eval(&stdlib::env(), parse("int_to_real 1")),
            Ok(Value::Real(value)) if (value - 1.0).abs() < 1e-6
        );
        assert_eq!(
            eval(&stdlib::env(), parse("real_to_int 1.0")),
            Ok(Value::Integer(1))
        );
        assert_matches!(
            eval(&stdlib::env(), parse("log 2.0 8.0")),
            Ok(Value::Real(value)) if (value - 3.0).abs() < 1e-6
        );
    }

    #[test]
    fn eval_stdlib() {
        assert_eq!(
            eval(&stdlib::env(), parse("not true")),
            Ok(Value::Bool(false)),
        );
    }

    #[test]
    fn eval_type_annotation() {
        assert_eq!(
            eval(&Env::default(), parse(r#"123 : Integer"#)),
            Ok(Value::Integer(123))
        );
    }

    #[test]
    fn eval_record_access() {
        assert_eq!(
            eval(&Env::default(), parse(r#"{x = 123, y = true}.x"#)),
            Ok(Value::Integer(123))
        );
    }

    #[test]
    fn eval_variant_case() {
        assert_eq!(
            eval(
                &Env::default(),
                parse(r#"case {|x = 123|} of {|x = y|} -> y"#)
            ),
            Ok(Value::Integer(123))
        );
    }

    #[test]
    fn eval_case_no_match() {
        assert_matches!(
            eval(
                &Env::default(),
                parse(r#"case {|x = 123|} of {|y = y|} -> y"#)
            ),
            Err(EvalError {
                kind: EvalErrorKind::NoMatch(Value::Variant { .. }),
                span: Span::Range(range)
            }) if range ==  TextRange::up_to(34.into())
        );
    }
}
