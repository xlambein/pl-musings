//! Lexer for the language.

use std::fmt;

use num_enum::{IntoPrimitive, TryFromPrimitive};

/// A token of a given kind, with its source text.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Token<'src> {
    pub kind: TokenKind,
    pub src: &'src str,
}

impl<'src> Token<'src> {
    pub fn new(kind: TokenKind, src: &'src str) -> Self {
        Token { kind, src }
    }
}

impl<'src> fmt::Display for Token<'src> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.src)
    }
}

/// The kind of a given token (e.g., number, plus sign, whitespace, etc.).
#[derive(Debug, Clone, Copy, PartialEq, IntoPrimitive, TryFromPrimitive)]
#[repr(u16)]
pub enum TokenKind {
    Bool,
    Number,
    Symbol,
    LParen,
    RParen,
    Plus,
    Hyphen,
    Star,
    Slash,
    DoubleEqual,
    BangEqual,
    DoubleAmpersand,
    DoublePipe,
    LAngle,
    RAngle,
    LAngleEqual,
    RAngleEqual,
    Colon,
    RightArrow,
    Backslash,
    Dot,
    LetKw,
    InKw,
    Equal,
    IfKw,
    ThenKw,
    ElseKw,
    LBrace,
    RBrace,
    LBracePipe,
    RBracePipe,
    Comma,
    RecKw,
    AndKw,
    CaseKw,
    OfKw,
    ForAllKw,

    Whitespace,
    Error,
}

impl TokenKind {
    /// Whether the token is trivia (whitespace or comment).
    pub fn is_trivia(self) -> bool {
        matches!(self, TokenKind::Whitespace)
    }
}

impl fmt::Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use TokenKind::*;
        let s = match self {
            Bool => "bool",
            Number => "number",
            Symbol => "symbol",
            LParen => "(",
            RParen => ")",
            Plus => "+",
            Hyphen => "-",
            Star => "*",
            Slash => "/",
            DoubleEqual => "==",
            BangEqual => "!=",
            DoubleAmpersand => "&&",
            DoublePipe => "||",
            LAngle => "<",
            RAngle => ">",
            LAngleEqual => "<=",
            RAngleEqual => ">=",
            Colon => ":",
            RightArrow => "->",
            Backslash => "\\",
            Dot => ".",
            LetKw => "let",
            InKw => "in",
            IfKw => "if",
            ThenKw => "then",
            ElseKw => "else",
            Equal => "=",
            LBrace => "{",
            RBrace => "}",
            LBracePipe => "{|",
            RBracePipe => "|}",
            Comma => ",",
            RecKw => "rec",
            AndKw => "and",
            CaseKw => "case",
            OfKw => "of",
            ForAllKw => "forall",

            Whitespace => "whitespace",
            Error => "error",
        };
        f.write_str(s)
    }
}

/// Convert a source text into a list of tokens.
pub fn tokenize(mut s: &str) -> Vec<Token> {
    let mut tokens = vec![];
    loop {
        let (t, token) = skip_whitespace(s);
        s = t;
        if let Some(token) = token {
            tokens.push(token);
        }
        match s.chars().next() {
            Some(c) if c.is_ascii_digit() => {
                let (t, token) = number(s);
                tokens.push(token);
                s = t;
            }
            Some(c) if c.is_alphabetic() || c == '_' => {
                let (t, token) = identifier(s);
                tokens.push(token);
                s = t;
            }
            Some(_) => {
                let (t, token) = characters(s);
                tokens.push(token);
                s = t;
            }
            None => {
                break;
            }
        }
    }
    tokens
}

fn characters(s: &str) -> (&str, Token) {
    use TokenKind::*;
    s.get(..2)
        .and_then(|chars| {
            let kind = match chars {
                "==" => DoubleEqual,
                "!=" => BangEqual,
                "&&" => DoubleAmpersand,
                "||" => DoublePipe,
                "<=" => LAngleEqual,
                ">=" => RAngleEqual,
                "->" => RightArrow,
                "{|" => LBracePipe,
                "|}" => RBracePipe,
                _ => return None,
            };
            Some((&s[2..], Token::new(kind, &s[..2])))
        })
        .unwrap_or_else(|| {
            let kind = match s.chars().next().unwrap() {
                '(' => LParen,
                ')' => RParen,
                '+' => Plus,
                '-' => Hyphen,
                '*' => Star,
                '/' => Slash,
                '<' => LAngle,
                '>' => RAngle,
                ':' => Colon,
                '\\' => Backslash,
                '.' => Dot,
                '=' => Equal,
                '{' => LBrace,
                '}' => RBrace,
                ',' => Comma,
                _ => Error,
            };
            (&s[1..], Token::new(kind, &s[..1]))
        })
}

fn number(s: &str) -> (&str, Token) {
    let mut i = 0;
    let mut kind = TokenKind::Number;
    for c in s.chars() {
        if c.is_ascii_digit() || matches!(c, 'e' | 'E' | '.') {
            i += c.len_utf8();
        } else if c.is_alphabetic() {
            i += c.len_utf8();
            kind = TokenKind::Error;
        } else {
            break;
        }
    }
    (&s[i..], Token::new(kind, &s[..i]))
}

fn identifier(s: &str) -> (&str, Token) {
    let mut i = 0;
    for c in s.chars() {
        if c.is_alphabetic() || c == '_' || (c.is_alphanumeric() && i != 0) {
            i += c.len_utf8();
        } else {
            break;
        }
    }
    let src = &s[..i];
    let kind = match src {
        "false" | "true" => TokenKind::Bool,
        "let" => TokenKind::LetKw,
        "in" => TokenKind::InKw,
        "if" => TokenKind::IfKw,
        "then" => TokenKind::ThenKw,
        "else" => TokenKind::ElseKw,
        "rec" => TokenKind::RecKw,
        "and" => TokenKind::AndKw,
        "case" => TokenKind::CaseKw,
        "of" => TokenKind::OfKw,
        "forall" => TokenKind::ForAllKw,
        _ => TokenKind::Symbol,
    };
    (&s[i..], Token::new(kind, src))
}

fn skip_whitespace(s: &str) -> (&str, Option<Token>) {
    let mut i = 0;
    for c in s.chars() {
        if c.is_whitespace() {
            i += c.len_utf8();
        } else {
            break;
        }
    }
    (
        &s[i..],
        if i == 0 {
            None
        } else {
            Some(Token::new(TokenKind::Whitespace, &s[..i]))
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn tokenize_bools() {
        assert_eq!(
            tokenize("false true"),
            vec![
                Token::new(TokenKind::Bool, "false"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Bool, "true"),
            ]
        );
    }

    #[test]
    fn tokenize_numbers() {
        assert_eq!(
            tokenize("1234 -5678 - 9 10.1e5"),
            vec![
                Token::new(TokenKind::Number, "1234"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Hyphen, "-"),
                Token::new(TokenKind::Number, "5678"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Hyphen, "-"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Number, "9"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Number, "10.1e5"),
            ]
        );
    }

    #[test]
    fn tokenize_punct() {
        assert_eq!(
            tokenize("+-*/()<> ==!=&&||<=>=:->{},{||}"),
            vec![
                Token::new(TokenKind::Plus, "+"),
                Token::new(TokenKind::Hyphen, "-"),
                Token::new(TokenKind::Star, "*"),
                Token::new(TokenKind::Slash, "/"),
                Token::new(TokenKind::LParen, "("),
                Token::new(TokenKind::RParen, ")"),
                Token::new(TokenKind::LAngle, "<"),
                Token::new(TokenKind::RAngle, ">"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::DoubleEqual, "=="),
                Token::new(TokenKind::BangEqual, "!="),
                Token::new(TokenKind::DoubleAmpersand, "&&"),
                Token::new(TokenKind::DoublePipe, "||"),
                Token::new(TokenKind::LAngleEqual, "<="),
                Token::new(TokenKind::RAngleEqual, ">="),
                Token::new(TokenKind::Colon, ":"),
                Token::new(TokenKind::RightArrow, "->"),
                Token::new(TokenKind::LBrace, "{"),
                Token::new(TokenKind::RBrace, "}"),
                Token::new(TokenKind::Comma, ","),
                Token::new(TokenKind::LBracePipe, "{|"),
                Token::new(TokenKind::RBracePipe, "|}"),
            ]
        );
    }

    #[test]
    fn tokenize_identifier() {
        assert_eq!(
            tokenize("foo bar123 _baz_hello"),
            vec![
                Token::new(TokenKind::Symbol, "foo"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Symbol, "bar123"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Symbol, "_baz_hello"),
            ]
        );
    }

    #[test]
    fn tokenize_lambda() {
        assert_eq!(
            tokenize(r#"\foo.1 + foo"#),
            vec![
                Token::new(TokenKind::Backslash, "\\"),
                Token::new(TokenKind::Symbol, "foo"),
                Token::new(TokenKind::Dot, "."),
                Token::new(TokenKind::Number, "1"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Plus, "+"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Symbol, "foo"),
            ]
        );
    }

    #[test]
    fn tokenize_let() {
        assert_eq!(
            tokenize(r#"let rec x = 1 in x"#),
            vec![
                Token::new(TokenKind::LetKw, "let"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::RecKw, "rec"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Symbol, "x"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Equal, "="),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Number, "1"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::InKw, "in"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Symbol, "x"),
            ]
        );
    }

    #[test]
    fn tokenize_if_then_else() {
        assert_eq!(
            tokenize("if true then 1 else 0"),
            vec![
                Token::new(TokenKind::IfKw, "if"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Bool, "true"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::ThenKw, "then"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Number, "1"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::ElseKw, "else"),
                Token::new(TokenKind::Whitespace, " "),
                Token::new(TokenKind::Number, "0"),
            ]
        );
    }

    #[test]
    fn tokenize_err() {
        assert_eq!(tokenize("?"), vec![Token::new(TokenKind::Error, "?")]);
        assert_eq!(
            tokenize("1234a"),
            vec![Token::new(TokenKind::Error, "1234a")]
        );
    }
}
