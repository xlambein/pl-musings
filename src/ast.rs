//! Abstract syntax tree wrapping a [`rowan`] tree.

use rowan::ast::{support, AstNode};

use crate::cst::{Lang, SyntaxKind, SyntaxNode, SyntaxToken};

#[derive(Debug)]
pub enum Expr {
    InfixOp(InfixOp),
    PrefixOp(PrefixOp),
    Application(Application),
    Lambda(Lambda),
    Let(Let),
    Conditional(Conditional),
    ParenthesizedExpr(ParenthesizedExpr),
    BoolLit(BoolLit),
    NumberLit(NumberLit),
    Identifier(Identifier),
    Record(Record),
    RecordAccess(RecordAccess),
    Variant(Variant),
    Case(Case),
    Rec(Rec),
}

impl AstNode for Expr {
    type Language = Lang;

    fn can_cast(kind: <Self::Language as rowan::Language>::Kind) -> bool
    where
        Self: Sized,
    {
        use SyntaxKind::*;
        matches!(
            kind,
            InfixOp
                | PrefixOp
                | Application
                | Lambda
                | Let
                | Conditional
                | ParenthesizedExpr
                | BoolLit
                | NumberLit
                | Identifier
                | Record
                | RecordAccess
                | Variant
                | Case
                | Rec
        )
    }

    fn cast(syntax: SyntaxNode) -> Option<Self> {
        Some(match syntax.kind() {
            SyntaxKind::InfixOp => Self::InfixOp(InfixOp { syntax }),
            SyntaxKind::PrefixOp => Self::PrefixOp(PrefixOp { syntax }),
            SyntaxKind::Application => Self::Application(Application { syntax }),
            SyntaxKind::Lambda => Self::Lambda(Lambda { syntax }),
            SyntaxKind::Let => Self::Let(Let { syntax }),
            SyntaxKind::Conditional => Self::Conditional(Conditional { syntax }),
            SyntaxKind::ParenthesizedExpr => Self::ParenthesizedExpr(ParenthesizedExpr { syntax }),
            SyntaxKind::BoolLit => Self::BoolLit(BoolLit { syntax }),
            SyntaxKind::NumberLit => Self::NumberLit(NumberLit { syntax }),
            SyntaxKind::Identifier => Self::Identifier(Identifier { syntax }),
            SyntaxKind::Record => Self::Record(Record { syntax }),
            SyntaxKind::RecordAccess => Self::RecordAccess(RecordAccess { syntax }),
            SyntaxKind::Variant => Self::Variant(Variant { syntax }),
            SyntaxKind::Case => Self::Case(Case { syntax }),
            SyntaxKind::Rec => Self::Rec(Rec { syntax }),
            _ => return None,
        })
    }

    fn syntax(&self) -> &rowan::SyntaxNode<Self::Language> {
        match self {
            Self::InfixOp(it) => it.syntax(),
            Self::PrefixOp(it) => it.syntax(),
            Self::Application(it) => it.syntax(),
            Self::Lambda(it) => it.syntax(),
            Self::Let(it) => it.syntax(),
            Self::Conditional(it) => it.syntax(),
            Self::ParenthesizedExpr(it) => it.syntax(),
            Self::BoolLit(it) => it.syntax(),
            Self::NumberLit(it) => it.syntax(),
            Self::Identifier(it) => it.syntax(),
            Self::Record(it) => it.syntax(),
            Self::RecordAccess(it) => it.syntax(),
            Self::Variant(it) => it.syntax(),
            Self::Case(it) => it.syntax(),
            Self::Rec(it) => it.syntax(),
        }
    }
}

#[derive(Debug)]
pub enum Pattern {
    BoolLit(BoolLit),
    NumberLit(NumberLit),
    Identifier(Identifier),
    Record(Record),
    Variant(Variant),
}

impl AstNode for Pattern {
    type Language = Lang;

    fn can_cast(kind: <Self::Language as rowan::Language>::Kind) -> bool
    where
        Self: Sized,
    {
        use SyntaxKind::*;
        matches!(kind, BoolLit | NumberLit | Identifier | Record | Variant)
    }

    fn cast(syntax: SyntaxNode) -> Option<Self> {
        Some(match syntax.kind() {
            SyntaxKind::BoolLit => Self::BoolLit(BoolLit { syntax }),
            SyntaxKind::NumberLit => Self::NumberLit(NumberLit { syntax }),
            SyntaxKind::Identifier => Self::Identifier(Identifier { syntax }),
            SyntaxKind::Record => Self::Record(Record { syntax }),
            SyntaxKind::Variant => Self::Variant(Variant { syntax }),
            _ => return None,
        })
    }

    fn syntax(&self) -> &rowan::SyntaxNode<Self::Language> {
        match self {
            Self::BoolLit(it) => it.syntax(),
            Self::NumberLit(it) => it.syntax(),
            Self::Identifier(it) => it.syntax(),
            Self::Record(it) => it.syntax(),
            Self::Variant(it) => it.syntax(),
        }
    }
}

#[derive(Debug)]
pub enum TypeAnnotation {
    ForAll(ForAll),
    Expr(Expr),
}

impl AstNode for TypeAnnotation {
    type Language = Lang;

    fn can_cast(kind: <Self::Language as rowan::Language>::Kind) -> bool
    where
        Self: Sized,
    {
        use SyntaxKind::*;
        matches!(kind, ForAll) || Expr::can_cast(kind)
    }

    fn cast(syntax: SyntaxNode) -> Option<Self> {
        match syntax.kind() {
            SyntaxKind::ForAll => Some(Self::ForAll(ForAll { syntax })),
            _ => Expr::cast(syntax).map(Self::Expr),
        }
    }

    fn syntax(&self) -> &rowan::SyntaxNode<Self::Language> {
        match self {
            TypeAnnotation::ForAll(it) => it.syntax(),
            TypeAnnotation::Expr(it) => it.syntax(),
        }
    }
}

macro_rules! impl_ast_node {
    ($ident:ident) => {
        #[derive(Debug)]
        pub struct $ident {
            syntax: SyntaxNode,
        }

        impl AstNode for $ident {
            type Language = Lang;

            fn can_cast(kind: <Self::Language as rowan::Language>::Kind) -> bool
            where
                Self: Sized,
            {
                kind == SyntaxKind::$ident
            }

            fn cast(syntax: SyntaxNode) -> Option<Self> {
                match syntax.kind() {
                    SyntaxKind::$ident => Some(Self { syntax }),
                    _ => None,
                }
            }

            fn syntax(&self) -> &rowan::SyntaxNode<Self::Language> {
                &self.syntax
            }
        }
    };
}

macro_rules! impl_ast_nodes {
    ($($ident:ident),* $(,)?) => {
        $(impl_ast_node!($ident);)*
    };
}

impl_ast_nodes![
    Root,
    InfixOp,
    PrefixOp,
    Operator,
    Application,
    Lambda,
    Let,
    LetBinding,
    ForAll,
    Conditional,
    ParenthesizedExpr,
    BoolLit,
    NumberLit,
    Identifier,
    Record,
    Field,
    FieldType,
    RecordAccess,
    Variant,
    Case,
    CaseArm,
    Rec,
];

impl Root {
    pub fn expr(&self) -> Option<Expr> {
        support::child(self.syntax())
    }
}

impl InfixOp {
    pub fn lhs(&self) -> Option<Expr> {
        support::child(self.syntax())
    }

    pub fn rhs(&self) -> Option<Expr> {
        support::children(self.syntax()).nth(1)
    }

    pub fn operator(&self) -> Option<Operator> {
        support::child(self.syntax())
    }
}

impl PrefixOp {
    pub fn inner(&self) -> Option<Expr> {
        support::child(self.syntax())
    }

    pub fn operator(&self) -> Option<Operator> {
        support::child(self.syntax())
    }
}

impl Operator {
    pub fn token(&self) -> Option<SyntaxToken> {
        self.syntax()
            .children_with_tokens()
            .filter_map(|n| n.into_token())
            .filter(|n| !n.kind().is_trivia())
            .next()
    }
}

impl Application {
    pub fn lhs(&self) -> Option<Expr> {
        support::child(self.syntax())
    }

    pub fn rhs(&self) -> Option<Expr> {
        support::children(self.syntax()).nth(1)
    }
}

impl Lambda {
    pub fn backslash_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Backslash)
    }

    pub fn symbol_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Symbol)
    }

    pub fn dot_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Dot)
    }

    pub fn body(&self) -> Option<Expr> {
        support::child(self.syntax())
    }
}

impl Let {
    pub fn let_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::LetKw)
    }

    pub fn rec_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::RecKw)
    }

    pub fn bindings(&self) -> impl Iterator<Item = LetBinding> {
        support::children(self.syntax())
    }

    pub fn in_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::InKw)
    }

    pub fn body(&self) -> Option<Expr> {
        support::child(self.syntax())
    }
}

impl LetBinding {
    pub fn symbol_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Symbol)
    }

    pub fn colon_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Colon)
    }

    pub fn type_annotation(&self) -> Option<TypeAnnotation> {
        self.colon_token()
            .and_then(|_| support::child(self.syntax()))
    }

    pub fn equal_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Equal)
    }

    pub fn value(&self) -> Option<Expr> {
        support::children(self.syntax()).last()
    }
}

impl ForAll {
    pub fn forall_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::ForAllKw)
    }

    pub fn variable(&self) -> Option<Identifier> {
        support::child(self.syntax())
    }

    pub fn dot_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Dot)
    }

    pub fn inner(&self) -> Option<TypeAnnotation> {
        support::children(self.syntax()).last()
    }
}

impl Conditional {
    pub fn if_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::IfKw)
    }

    pub fn then_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::ThenKw)
    }

    pub fn else_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::ElseKw)
    }

    pub fn condition(&self) -> Option<Expr> {
        support::child(self.syntax())
    }

    pub fn then(&self) -> Option<Expr> {
        support::children(self.syntax()).nth(1)
    }

    pub fn r#else(&self) -> Option<Expr> {
        support::children(self.syntax()).nth(2)
    }
}

impl ParenthesizedExpr {
    pub fn lparen_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::LParen)
    }

    pub fn rparen_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::RParen)
    }

    pub fn expr(&self) -> Option<Expr> {
        support::child(self.syntax())
    }
}

impl BoolLit {
    pub fn bool_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Bool)
    }
}

impl NumberLit {
    pub fn number_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Number)
    }
}

impl Identifier {
    pub fn symbol_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Symbol)
    }
}

impl Record {
    pub fn fields(&self) -> impl Iterator<Item = Field> {
        support::children(self.syntax())
    }

    pub fn field_types(&self) -> impl Iterator<Item = FieldType> {
        support::children(self.syntax())
    }
}

impl Field {
    pub fn name(&self) -> Option<Identifier> {
        support::child(self.syntax())
    }

    pub fn expr(&self) -> Option<Expr> {
        support::children(self.syntax()).nth(1)
    }
}

impl FieldType {
    pub fn name(&self) -> Option<Identifier> {
        support::child(self.syntax())
    }

    pub fn expr(&self) -> Option<Expr> {
        support::children(self.syntax()).nth(1)
    }
}

impl RecordAccess {
    pub fn record(&self) -> Option<Expr> {
        support::child(self.syntax())
    }

    pub fn field(&self) -> Option<Identifier> {
        if let Expr::Identifier(identifier) = support::children::<Expr>(self.syntax()).nth(1)? {
            Some(identifier)
        } else {
            None
        }
    }
}

impl Variant {
    pub fn fields(&self) -> impl Iterator<Item = Field> {
        support::children(self.syntax())
    }

    pub fn field_types(&self) -> impl Iterator<Item = FieldType> {
        support::children(self.syntax())
    }
}

impl Case {
    pub fn case_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::CaseKw)
    }

    pub fn subject(&self) -> Option<Expr> {
        support::child(self.syntax())
    }

    pub fn of_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::OfKw)
    }

    pub fn arms(&self) -> impl Iterator<Item = CaseArm> {
        support::children(self.syntax())
    }
}

impl CaseArm {
    pub fn pattern(&self) -> Option<Pattern> {
        support::child(
            &self
                .syntax()
                .children()
                .find(|n| n.kind() == SyntaxKind::Pattern)?,
        )
    }

    pub fn right_arrow_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::RightArrow)
    }

    pub fn body(&self) -> Option<Expr> {
        support::child(self.syntax())
    }
}

impl Rec {
    pub fn rec_kw_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::RecKw)
    }

    pub fn variable(&self) -> Option<Identifier> {
        support::child(self.syntax())
    }

    pub fn dot_token(&self) -> Option<SyntaxToken> {
        support::token(self.syntax(), SyntaxKind::Dot)
    }

    pub fn inner(&self) -> Option<Expr> {
        support::children(self.syntax()).last()
    }
}
