use std::fmt;

use crate::hir::*;

impl fmt::Display for RichExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.expr() {
            Expr::Literal(literal) => match literal {
                Literal::Bool(value) => write!(f, "{}", value),
                Literal::Integer(value) => write!(f, "{}", value),
                Literal::Real(value) => write!(f, "{}", value),
            },
            Expr::Identifier(identifier) => write!(f, "{}", identifier),
            Expr::InfixOp { op, lhs, rhs } => {
                use InfixOperator::*;
                // Parenthesize only if the precedence is lower
                let lhs_parens = match (op, lhs.expr()) {
                    // Always parenthesize lambdas
                    (_, Expr::Lambda { .. }) => true,
                    // Don't parenthesize atoms
                    (
                        _,
                        Expr::Literal(_)
                        | Expr::LiteralType(_)
                        | Expr::Identifier(_)
                        | Expr::EmptyRecord
                        | Expr::Record(_)
                        | Expr::RecordType(_)
                        | Expr::EmptyVariant
                        | Expr::Variant { .. }
                        | Expr::VariantType(_),
                    ) => false,
                    // Applications have highest precedence
                    (_, Expr::Application { .. }) => false,
                    (Add | Sub, _) => false,
                    (Mul | Div, Expr::InfixOp { op: Mul | Div, .. }) => false,
                    _ => true,
                };
                // Left-associativity means rhs expressions should always be parenthesized
                let rhs_parens = match (op, rhs.expr()) {
                    // Always parenthesize lambdas
                    (_, Expr::Lambda { .. }) => true,
                    (
                        _,
                        Expr::Literal(_)
                        | Expr::LiteralType(_)
                        | Expr::Identifier(_)
                        | Expr::EmptyRecord
                        | Expr::Record(_)
                        | Expr::RecordType(_)
                        | Expr::EmptyVariant
                        | Expr::Variant { .. }
                        | Expr::VariantType(_),
                    ) => false,
                    (_, Expr::Application { .. }) => false,
                    (Add | Sub, Expr::InfixOp { op: Add | Sub, .. }) => true,
                    (Add | Sub, _) => false,
                    _ => true,
                };
                if lhs_parens {
                    write!(f, "({})", lhs)?;
                } else {
                    write!(f, "{}", lhs)?;
                }
                write!(f, " {} ", op)?;
                if rhs_parens {
                    write!(f, "({})", rhs)?;
                } else {
                    write!(f, "{}", rhs)?;
                }
                Ok(())
            }
            Expr::PrefixOp { op, inner } => {
                let parens = match inner.expr() {
                    Expr::Literal(_)
                    | Expr::LiteralType(_)
                    | Expr::Identifier(_)
                    | Expr::EmptyRecord
                    | Expr::Record(_)
                    | Expr::RecordType(_)
                    | Expr::EmptyVariant
                    | Expr::Variant { .. }
                    | Expr::VariantType(_) => false,
                    _ => true,
                };
                write!(f, "{}", op)?;
                if parens {
                    write!(f, "({})", inner)?;
                } else {
                    write!(f, "{}", inner)?;
                }
                Ok(())
            }
            Expr::Application { lhs, rhs } => {
                // Parenthesize only if the precedence is lower
                let lhs_parens = match lhs.expr() {
                    Expr::Lambda { .. } => true,
                    Expr::Literal(_) | Expr::LiteralType(_) | Expr::Identifier(_) => false,
                    Expr::Application { .. } => false,
                    _ => true,
                };
                // Left-associativity means rhs expressions should always be parenthesized
                let rhs_parens = match rhs.expr() {
                    Expr::Lambda { .. } => true,
                    Expr::Literal(_) | Expr::LiteralType(_) | Expr::Identifier(_) => false,
                    _ => true,
                };
                if lhs_parens {
                    write!(f, "({})", lhs)?;
                } else {
                    write!(f, "{}", lhs)?;
                }
                write!(f, " ")?;
                if rhs_parens {
                    write!(f, "({})", rhs)?;
                } else {
                    write!(f, "{}", rhs)?;
                }
                Ok(())
            }
            Expr::Let {
                rec,
                bindings,
                body,
            } => {
                write!(
                    f,
                    "let{rec} {bindings} in {body}",
                    rec = if *rec { " rec" } else { "" },
                    bindings = bindings
                        .iter()
                        .map(
                            |LetBinding {
                                 identifier,
                                 type_annotation,
                                 body,
                             }| format!(
                                "{identifier}{type_annotation} = {body}",
                                type_annotation = if let Some(type_annotation) = type_annotation {
                                    format!(" : {type_annotation}")
                                } else {
                                    String::new()
                                }
                            )
                        )
                        .collect::<Vec<_>>()
                        .join(" and ")
                )
            }
            Expr::Conditional {
                condition,
                then,
                r#else,
            } => {
                write!(f, "if {} then {} else {}", condition, then, r#else)
            }
            // TODO make it so that lambdas aren't always parenthesized
            //   (non-trivial, e.g. `1 + \x.x + 2` vs `1 + (\x.x) + 2`)
            Expr::Lambda { identifier, body } => write!(f, "\\{}. {}", identifier, body),
            Expr::EmptyRecord => {
                write!(f, "{{}}")
            }
            Expr::Record(fields) => {
                write!(f, "{{")?;
                for (i, (name, expr)) in fields.iter().enumerate() {
                    write!(f, "{name} = {expr}")?;
                    if i < fields.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, "}}")
            }
            Expr::RecordType(fields) => {
                write!(f, "{{")?;
                for (i, (name, expr)) in fields.iter().enumerate() {
                    write!(f, "{name} : {expr}")?;
                    if i < fields.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, "}}")
            }
            Expr::RecordAccess { record, field } => write!(f, "{record}.{field}"),
            Expr::EmptyVariant => {
                write!(f, "{{||}}")
            }
            Expr::Variant { field, value } => write!(f, "{{|{field} = {value}|}}"),
            Expr::VariantType(fields) => {
                write!(f, "{{|")?;
                for (i, (name, expr)) in fields.iter().enumerate() {
                    write!(f, "{name} : {expr}")?;
                    if i < fields.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, "|}}")
            }
            Expr::Case { subject, arms } => {
                write!(f, "case {subject} of ")?;
                for (i, CaseArm { pattern, body }) in arms.iter().enumerate() {
                    write!(f, "{pattern} -> {body}")?;
                    if i < arms.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                Ok(())
            }

            Expr::LiteralType(ty) => match ty {
                LiteralType::Bool => write!(f, "Bool"),
                LiteralType::Integer => write!(f, "Integer"),
                LiteralType::Real => write!(f, "Real"),
            },
            Expr::Rec { variable, inner } => write!(f, "rec {}. {}", variable, inner),

            Expr::Missing => write!(f, "<_>"),
        }
    }
}

impl fmt::Display for InfixOperator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            InfixOperator::Add => "+",
            InfixOperator::Sub => "-",
            InfixOperator::Mul => "*",
            InfixOperator::Div => "/",
            InfixOperator::And => "&&",
            InfixOperator::Or => "||",
            InfixOperator::Eq => "==",
            InfixOperator::Neq => "!=",
            InfixOperator::Gt => ">",
            InfixOperator::Lt => "<",
            InfixOperator::Gte => ">=",
            InfixOperator::Lte => "<=",

            InfixOperator::HasType => ":",
            InfixOperator::FunArrow => "->",
        };
        f.write_str(s)
    }
}

impl fmt::Display for PrefixOperator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            PrefixOperator::Negative => "-",
        };
        f.write_str(s)
    }
}

impl<E: fmt::Display> fmt::Display for TypeAnnotation<E> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            TypeAnnotation::ForAll { variable, inner } => write!(f, "forall {variable}. {inner}"),
            TypeAnnotation::Expr(expr) => expr.fmt(f),
        }
    }
}
