//! High-level intermediate representation (i.e., lowered AST).

use std::{
    collections::{BTreeMap, HashSet},
    rc::Rc,
};

use rowan::{ast::AstNode, TextRange};

use crate::{ast, cst::SyntaxKind};

mod format;

pub type Identifier = String;

#[derive(Debug, Clone, PartialEq)]
pub enum Expr<E> {
    Literal(Literal),
    Identifier(Identifier),
    InfixOp {
        op: InfixOperator,
        lhs: E,
        rhs: E,
    },
    PrefixOp {
        op: PrefixOperator,
        inner: E,
    },
    Application {
        lhs: E,
        rhs: E,
    },
    Let {
        rec: bool,
        bindings: Vec<LetBinding<E>>,
        body: E,
    },
    Lambda {
        identifier: Identifier,
        body: E,
    },
    Conditional {
        condition: E,
        then: E,
        r#else: E,
    },
    EmptyRecord,
    Record(BTreeMap<Identifier, E>),
    RecordType(BTreeMap<Identifier, E>),
    RecordAccess {
        record: E,
        field: Identifier,
    },
    EmptyVariant,
    Variant {
        field: Identifier,
        value: E,
    },
    VariantType(BTreeMap<Identifier, E>),
    Case {
        subject: E,
        arms: Vec<CaseArm<E>>,
    },

    LiteralType(LiteralType),
    Rec {
        variable: Identifier,
        inner: E,
    },

    Missing,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Literal {
    Bool(bool),
    Integer(i64),
    Real(f64),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LiteralType {
    Bool,
    Integer,
    Real,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum InfixOperator {
    Add,
    Sub,
    Mul,
    Div,
    And,
    Or,
    Eq,
    Neq,
    Gt,
    Lt,
    Gte,
    Lte,

    HasType,
    FunArrow,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PrefixOperator {
    Negative,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TypeAnnotation<E> {
    ForAll {
        variable: Identifier,
        inner: Box<TypeAnnotation<E>>,
    },
    Expr(E),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct LetBinding<E> {
    pub identifier: Identifier,
    pub type_annotation: Option<TypeAnnotation<E>>,
    pub body: E,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CaseArm<E> {
    // TODO it would be better to have a specific `Pattern` type
    pub pattern: E,
    pub body: E,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Span {
    None,
    Range(TextRange),
}

impl<T> From<&T> for Span
where
    T: AstNode,
{
    fn from(elem: &T) -> Self {
        Span::Range(elem.syntax().text_range())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct RichExpr {
    pub expr: Rc<Expr<RichExpr>>,
    pub span: Span,
}

impl RichExpr {
    pub fn new(expr: Expr<RichExpr>, span: impl Into<Span>) -> Self {
        Self {
            expr: Rc::new(expr),
            span: span.into(),
        }
    }

    pub fn expr(&self) -> &Expr<RichExpr> {
        self.expr.as_ref()
    }
}

pub fn lower(node: ast::Root) -> RichExpr {
    lower_expr(node.expr())
}

fn lower_expr(node: Option<ast::Expr>) -> RichExpr {
    let span = node.as_ref().map_or(Span::None, Span::from);
    node.and_then(|node| match node {
        ast::Expr::InfixOp(it) => lower_infix_op(it),
        ast::Expr::PrefixOp(it) => lower_prefix_op(it),
        ast::Expr::Application(it) => lower_application(it),
        ast::Expr::Lambda(it) => lower_lambda(it),
        ast::Expr::Let(it) => lower_let(it),
        ast::Expr::Conditional(it) => lower_conditional(it),
        ast::Expr::ParenthesizedExpr(it) => Some(lower_expr(it.expr())),
        ast::Expr::BoolLit(it) => lower_bool_lit(it),
        ast::Expr::NumberLit(it) => lower_number_lit(it),
        ast::Expr::Identifier(it) => lower_identifier(it),
        ast::Expr::Record(it) => lower_record(it),
        ast::Expr::RecordAccess(it) => lower_record_access(it),
        ast::Expr::Variant(it) => lower_variant(it),
        ast::Expr::Case(it) => lower_case(it),
        ast::Expr::Rec(it) => lower_rec(it),
    })
    .unwrap_or_else(|| RichExpr::new(Expr::Missing, span))
}

fn lower_infix_op(node: ast::InfixOp) -> Option<RichExpr> {
    Some(RichExpr::new(
        if let Some(op) = node.operator().as_ref().and_then(ast::Operator::token) {
            use InfixOperator::*;
            use SyntaxKind::*;
            let op = match op.kind() {
                Plus => Add,
                Hyphen => Sub,
                Star => Mul,
                Slash => Div,
                DoubleEqual => Eq,
                BangEqual => Neq,
                DoubleAmpersand => And,
                DoublePipe => Or,
                LAngle => Lt,
                RAngle => Gt,
                LAngleEqual => Lte,
                RAngleEqual => Gte,
                Colon => HasType,
                RightArrow => FunArrow,
                _ => return None,
            };
            Expr::InfixOp {
                op,
                lhs: lower_expr(node.lhs()),
                rhs: lower_expr(node.rhs()),
            }
        } else {
            Expr::Missing
        },
        &node,
    ))
}

fn lower_prefix_op(node: ast::PrefixOp) -> Option<RichExpr> {
    Some(RichExpr::new(
        if let Some(op) = node.operator().as_ref().and_then(ast::Operator::token) {
            let op = match op.kind() {
                SyntaxKind::Hyphen => PrefixOperator::Negative,
                _ => return None,
            };
            Expr::PrefixOp {
                op,
                inner: lower_expr(node.inner()),
            }
        } else {
            Expr::Missing
        },
        &node,
    ))
}

fn lower_application(node: ast::Application) -> Option<RichExpr> {
    Some(RichExpr::new(
        Expr::Application {
            lhs: lower_expr(node.lhs()),
            rhs: lower_expr(node.rhs()),
        },
        &node,
    ))
}

fn lower_lambda(node: ast::Lambda) -> Option<RichExpr> {
    Some(RichExpr::new(
        Expr::Lambda {
            identifier: node.symbol_token()?.text().to_owned(),
            body: lower_expr(node.body()),
        },
        &node,
    ))
}

fn lower_let(node: ast::Let) -> Option<RichExpr> {
    let bindings = node
        .bindings()
        .map(|binding| {
            Some(LetBinding {
                identifier: binding.symbol_token()?.text().to_owned(),
                type_annotation: binding.type_annotation().and_then(lower_type_annotation),
                body: lower_expr(binding.value()),
            })
        })
        .collect::<Option<_>>()?;
    Some(RichExpr::new(
        Expr::Let {
            rec: node.rec_kw_token().is_some(),
            bindings,
            body: lower_expr(node.body()),
        },
        &node,
    ))
}

fn lower_type_annotation(node: ast::TypeAnnotation) -> Option<TypeAnnotation<RichExpr>> {
    match node {
        ast::TypeAnnotation::ForAll(node) => lower_forall(node),
        ast::TypeAnnotation::Expr(node) => Some(TypeAnnotation::Expr(lower_expr(Some(node)))),
    }
}

fn lower_forall(node: ast::ForAll) -> Option<TypeAnnotation<RichExpr>> {
    Some(TypeAnnotation::ForAll {
        variable: node.variable()?.symbol_token()?.text().to_owned(),
        inner: Box::new(lower_type_annotation(node.inner()?)?),
    })
}

fn lower_conditional(node: ast::Conditional) -> Option<RichExpr> {
    Some(RichExpr::new(
        Expr::Conditional {
            condition: lower_expr(node.condition()),
            then: lower_expr(node.then()),
            r#else: lower_expr(node.r#else()),
        },
        &node,
    ))
}

fn lower_bool_lit(node: ast::BoolLit) -> Option<RichExpr> {
    let literal = match node.bool_token()?.text() {
        "false" => false,
        "true" => true,
        _ => return None,
    };
    Some(RichExpr::new(Expr::Literal(Literal::Bool(literal)), &node))
}

fn lower_number_lit(node: ast::NumberLit) -> Option<RichExpr> {
    let token = node.number_token()?;
    let literal = token
        .text()
        .parse()
        .ok()
        .map(Literal::Integer)
        .or_else(|| token.text().parse().ok().map(Literal::Real))?;
    Some(RichExpr::new(Expr::Literal(literal), &node))
}

fn lower_identifier(node: ast::Identifier) -> Option<RichExpr> {
    let symbol_token = node.symbol_token()?;
    let ident_text = symbol_token.text();
    Some(RichExpr::new(
        lower_literal_type(ident_text)
            .map(Expr::LiteralType)
            .unwrap_or_else(|| Expr::Identifier(ident_text.to_owned())),
        &node,
    ))
}

fn lower_record(node: ast::Record) -> Option<RichExpr> {
    // Check if keys are unique
    if has_duplicates(node.fields().flat_map(|field| field.name()?.symbol_token()))
        || has_duplicates(
            node.field_types()
                .flat_map(|field| field.name()?.symbol_token()),
        )
    {
        panic!("record has duplicate keys");
    }

    Some(RichExpr::new(
        match (node.fields().next(), node.field_types().next()) {
            (None, None) => Expr::EmptyRecord,
            (Some(_), None) => Expr::Record(node.fields().map(lower_field).collect::<Option<_>>()?),
            (None, Some(_)) => Expr::RecordType(
                node.field_types()
                    .map(lower_field_type)
                    .collect::<Option<_>>()?,
            ),
            (Some(_), Some(_)) => panic!("record has both values and annotations"),
        },
        &node,
    ))
}

fn lower_field(node: ast::Field) -> Option<(Identifier, RichExpr)> {
    Some((
        node.name()?.symbol_token()?.text().to_owned(),
        lower_expr(node.expr()),
    ))
}

fn lower_field_type(node: ast::FieldType) -> Option<(Identifier, RichExpr)> {
    Some((
        node.name()?.symbol_token()?.text().to_owned(),
        lower_expr(node.expr()),
    ))
}

fn has_duplicates(idents: impl Iterator<Item = crate::cst::SyntaxToken>) -> bool {
    let mut acc = HashSet::new();
    for ident in idents {
        if acc.contains(&ident) {
            return true;
        } else {
            acc.insert(ident);
        }
    }
    false
}

fn lower_record_access(node: ast::RecordAccess) -> Option<RichExpr> {
    // TODO we might want to lower this to a pattern match in the future
    let symbol_token = node.field()?.symbol_token()?;
    let ident_text = symbol_token.text();
    Some(RichExpr::new(
        Expr::RecordAccess {
            record: lower_expr(node.record()),
            field: Identifier::from(ident_text),
        },
        &node,
    ))
}

fn lower_variant(node: ast::Variant) -> Option<RichExpr> {
    // In case of variant value, check that there's only one field
    if node.fields().count() > 1 {
        panic!("variant has more than one field");
    }

    // Check if keys are unique
    if has_duplicates(node.fields().flat_map(|field| field.name()?.symbol_token()))
        || has_duplicates(
            node.field_types()
                .flat_map(|field| field.name()?.symbol_token()),
        )
    {
        panic!("variant type has duplicate keys");
    }

    Some(RichExpr::new(
        match (node.fields().next(), node.field_types().next()) {
            (None, None) => Expr::EmptyVariant,
            (Some(field), None) => {
                let (field, value) = lower_field(field)?;
                Expr::Variant { field, value }
            }
            (None, Some(_)) => Expr::VariantType(
                node.field_types()
                    .map(lower_field_type)
                    .collect::<Option<_>>()?,
            ),
            (Some(_), Some(_)) => panic!("variant has both values and annotations"),
        },
        &node,
    ))
}

fn lower_case(node: ast::Case) -> Option<RichExpr> {
    Some(RichExpr::new(
        Expr::Case {
            subject: lower_expr(node.subject()),
            arms: node.arms().flat_map(|arm| lower_case_arm(arm)).collect(),
        },
        &node,
    ))
}

fn lower_case_arm(node: ast::CaseArm) -> Option<CaseArm<RichExpr>> {
    Some(CaseArm {
        pattern: lower_pattern(node.pattern()?)?,
        body: lower_expr(node.body()),
    })
}

fn lower_rec(node: ast::Rec) -> Option<RichExpr> {
    Some(RichExpr::new(
        Expr::Rec {
            variable: node.variable()?.symbol_token()?.text().to_owned(),
            inner: lower_expr(node.inner()),
        },
        &node,
    ))
}

fn lower_pattern(node: ast::Pattern) -> Option<RichExpr> {
    match node {
        ast::Pattern::BoolLit(it) => lower_bool_lit(it),
        ast::Pattern::NumberLit(it) => lower_number_lit(it),
        ast::Pattern::Identifier(it) => lower_identifier(it),
        ast::Pattern::Record(it) => lower_record(it),
        ast::Pattern::Variant(it) => lower_variant(it),
    }
}

fn lower_literal_type(name: &str) -> Option<LiteralType> {
    Some(match name {
        "Bool" => LiteralType::Bool,
        "Integer" => LiteralType::Integer,
        "Real" => LiteralType::Real,
        _ => return None,
    })
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use rowan::ast::AstNode;

    use crate::parser;

    use super::*;

    /// Checks whether parsing, lowering, then formatting is identity.
    #[test]
    fn parse_format_eq() {
        fn assert_parse_format_eq(input: &str) {
            let (node, errors) = parser::parse(input);
            eprint!("{:#?}", node);
            if !errors.is_empty() {
                panic!("{}", errors.join("\n"));
            }
            let expr = lower(ast::Root::cast(node).expect("no root node"));

            eprintln!("{:?}", expr);
            assert_eq!(input, &expr.to_string());
        }

        assert_parse_format_eq("false true");
        assert_parse_format_eq("1234");
        assert_parse_format_eq("-456");
        assert_parse_format_eq("123 + 456");
        assert_parse_format_eq("1 - 2 + 3 * 5 / 6");
        assert_parse_format_eq("123 + 456 - 789");
        assert_parse_format_eq("(1 - 2) / 3");
        assert_parse_format_eq("1 + (2 - 3 * (4 / 5))");
        assert_parse_format_eq("foo + (bar - 1)");
        assert_parse_format_eq("foo bar baz");
        assert_parse_format_eq("foo (bar baz)");
        assert_parse_format_eq("foo bar + 1");
        assert_parse_format_eq("foo (bar + 1)");
        assert_parse_format_eq(r#"\x. x + 1"#);
        assert_parse_format_eq(r#"(\x. x) + 1"#);
        assert_parse_format_eq(r#"1 + (\x. x) + 2"#);
        assert_parse_format_eq("if true then 1 else 0");
        assert_parse_format_eq("Bool -> Integer");
        assert_parse_format_eq("10 : Integer");
        assert_parse_format_eq(r#"(\x. x) : (Real -> Real)"#);
        assert_parse_format_eq("{bar = {}, foo = 10} : {bar : {}, foo : Integer}");
        assert_parse_format_eq("{x = 10, y = true}.x");
        assert_parse_format_eq("let x = 1 in x + 2");
        assert_parse_format_eq("let x = 1 and y = 2 in x + y");
        assert_parse_format_eq("let rec x = x + 1 in x");
        assert_parse_format_eq("{|foo = 10|} : {|bar : {||}, foo : Integer|}");
        assert_parse_format_eq("case {|foo = 10|} of {|bar = baz|} -> baz");
        assert_parse_format_eq(r#"let id : forall a. a -> a = \x. x in id"#);
        // assert_parse_format_eq(r#"1 + \x. x + 2"#);
    }
}
