use anyhow::{anyhow, Result};
use directories::ProjectDirs;
use pl_musings::{
    ast,
    cst::{SyntaxElement, SyntaxKind, SyntaxNode, SyntaxToken},
    hir::{self, Span},
    interpreter::{eval, stdlib, EvalError},
    parser::{parse, parse_incomplete, ParseError},
    typecheck::type_check,
    types::TypeCheckError,
};
use rowan::{
    ast::{support, AstNode},
    NodeOrToken, TextRange, TextSize, TokenAtOffset, WalkEvent,
};
use rustyline::{
    error::ReadlineError,
    highlight::Highlighter,
    validate::{ValidationContext, ValidationResult, Validator},
};
use rustyline_derive::{Completer, Helper, Hinter};
use std::{
    borrow::Cow,
    fmt::{self, Write},
    fs, io,
};

#[derive(Debug)]
enum ReplError {
    ParseError(ParseError),
    EvalError(EvalError),
    TypeError(TypeCheckError),
}

impl fmt::Display for ReplError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "\x1b[1;31merror\x1b[0m: ")?;
        match self {
            ReplError::ParseError(error) => error.fmt(f),
            ReplError::EvalError(error) => error.fmt(f),
            ReplError::TypeError(error) => error.fmt(f),
        }
    }
}

#[derive(Debug)]
struct ReplErrors(Vec<ReplError>);

impl fmt::Display for ReplErrors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for error in &self.0 {
            write!(f, "{}\n\n", error)?;
        }
        Ok(())
    }
}

enum Command {
    Eval,
    Cst,
    TypeCheck,
}

fn parse_command(line: &str) -> Result<(Command, &str), ()> {
    if let Some(line) = line.strip_prefix(':') {
        let (command, line) = line.split_once(char::is_whitespace).unwrap_or((line, ""));
        match command {
            "eval" => Ok((Command::Eval, line)),
            "cst" => Ok((Command::Cst, line)),
            "tc" => Ok((Command::TypeCheck, line)),
            _ => Err(()),
        }
    } else {
        Ok((Command::Eval, line))
    }
}

fn pretty_print_node<F: io::Write>(f: &mut F, node: &SyntaxNode) -> io::Result<()> {
    let mut depth: usize = 0;
    for event in node.preorder_with_tokens() {
        match event {
            WalkEvent::Enter(elem) => match elem {
                NodeOrToken::Node(node) => {
                    writeln!(
                        f,
                        "\x1b[2m{indent}\x1b[0m\x1b[{style}m{kind:?}\x1b[0m@\x1b[35m{range:?}\x1b[0m",
                        indent = "│ ".repeat(depth),
                        style = if node.kind() == SyntaxKind::Error { "1;31" } else { "1;34" },
                        kind = node.kind(),
                        range = node.text_range(),
                    )?;
                    depth += 1;
                }
                NodeOrToken::Token(token) => {
                    writeln!(
                        f,
                        "\x1b[2m{indent}\x1b[0m\x1b[{style}m{kind:?}\x1b[0m@\x1b[35m{range:?}\x1b[0m \x1b[43;30m{text:?}\x1b[0m",
                        indent = "│ ".repeat(depth),
                        style = if token.kind().is_trivia() { "1" } else if token.kind() == SyntaxKind::Error { "1;31" } else { "1;33" },
                        kind = token.kind(),
                        range = token.text_range(),
                        text = token.text(),
                    )?;
                }
            },
            WalkEvent::Leave(elem) => match elem {
                NodeOrToken::Node(_) => {
                    depth -= 1;
                }
                NodeOrToken::Token(_) => {}
            },
        }
    }
    Ok(())
}

#[derive(Completer, Helper, Hinter)]
struct LangHelper;

impl Validator for LangHelper {
    fn validate(&self, ctx: &mut ValidationContext) -> rustyline::Result<ValidationResult> {
        use ValidationResult::{Incomplete, Invalid, Valid};
        let input = ctx.input();
        let (command, input) = parse_command(input).unwrap_or((Command::Eval, input));

        // Skip empty lines
        if input.trim().is_empty() {
            return Ok(Valid(None));
        }

        if matches!(command, Command::Cst) {
            return Ok(Valid(None));
        }

        let result = match parse_incomplete(&input) {
            None => Incomplete,
            Some((_, errors)) => {
                if errors.is_empty() {
                    Valid(None)
                } else {
                    Invalid(Some(format!(
                        "\n{}",
                        ReplErrors(errors.into_iter().map(ReplError::ParseError).collect())
                            .to_string(),
                    )))
                }
            }
        };

        Ok(result)
    }
}

fn find_matching_paren(token: SyntaxToken) -> Option<SyntaxToken> {
    let parent = token
        .parent()
        .filter(|n| n.kind() == SyntaxKind::ParenthesizedExpr)?;
    match token.kind() {
        SyntaxKind::LParen => support::token(&parent, SyntaxKind::RParen),
        SyntaxKind::RParen => support::token(&parent, SyntaxKind::LParen),
        _ => None,
    }
}

fn parent_is_kind(elem: &SyntaxElement, kind: SyntaxKind) -> bool {
    elem.parent().map(|e| e.kind() == kind).unwrap_or(false)
}

impl Highlighter for LangHelper {
    fn highlight<'l>(&self, line: &'l str, pos: usize) -> Cow<'l, str> {
        let (_, rest) = parse_command(line).unwrap_or((Command::Eval, line));
        let command_text = &line[..line.len() - rest.len()];
        let line = rest;

        let (node, _) = parse(line);
        let pos = TextSize::from((pos - command_text.len()) as u32);

        let matching_paren = match node.token_at_offset(pos) {
            TokenAtOffset::Single(t) if t.text_range().start() == pos => Some(t),
            TokenAtOffset::Between(_, r) => Some(r),
            _ => None,
        }
        .and_then(find_matching_paren);

        let mut buf = String::new();
        write!(buf, "\x1b[1;34m{}\x1b[0m", command_text).unwrap();
        for event in node.preorder_with_tokens() {
            match event {
                WalkEvent::Enter(elem) => {
                    use SyntaxKind::*;
                    buf += match elem.kind() {
                        Error => "\x1b[41m",
                        NumberLit => "\x1b[1;32m",
                        Backslash | Dot | Equal => "\x1b[1m",
                        Symbol if parent_is_kind(&elem, Lambda) => "\x1b[34m",
                        LetKw | InKw | RecKw | AndKw => "\x1b[1;35m",
                        _ => {
                            if matching_paren
                                .as_ref()
                                .zip(elem.as_token())
                                .map(|(l, r)| l == r)
                                .unwrap_or(false)
                            {
                                "\x1b[1;30;45m"
                            } else {
                                ""
                            }
                        }
                    };
                    if let Some(token) = elem.as_token() {
                        write!(buf, "{}", token).unwrap();
                    }
                }
                WalkEvent::Leave(elem) => {
                    use SyntaxKind::*;
                    buf += match elem.kind() {
                        Error | NumberLit | Backslash | Dot | LetKw | RecKw | AndKw | InKw
                        | Equal => "\x1b[0m",
                        Symbol if parent_is_kind(&elem, Lambda) => "\x1b[0m",
                        _ => {
                            if matching_paren
                                .as_ref()
                                .zip(elem.as_token())
                                .map(|(l, r)| l == r)
                                .unwrap_or(false)
                            {
                                "\x1b[0m"
                            } else {
                                ""
                            }
                        }
                    };
                }
            }
        }

        Cow::Owned(buf)
    }

    fn highlight_char(&self, _line: &str, _pos: usize) -> bool {
        true
    }
}

pub struct TextLine<'src> {
    line: usize,
    range: TextRange,
    text: &'src str,
}

impl<'src> fmt::Display for TextLine<'src> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", self.text)?;
        write!(
            f,
            "{}{}",
            " ".repeat(self.range.start().into()),
            "^".repeat(self.range.len().into())
        )
    }
}

pub struct Lines<'src> {
    lines: Vec<&'src str>,
}

impl<'src> Lines<'src> {
    pub fn new(text: &'src str) -> Self {
        Self {
            lines: text.split('\n').collect(),
        }
    }

    pub fn find(&'src self, range: TextRange) -> impl Iterator<Item = TextLine<'src>> + 'src {
        self.lines
            .iter()
            .scan(0, |acc, line| {
                let start = *acc;
                *acc += line.len() as u32 + 1;
                Some((line, TextSize::from(start), TextSize::from(*acc)))
            })
            .enumerate()
            .filter(move |(_, (_, start, end))| *start <= range.end() && range.start() <= *end)
            .map(move |(i, (line, start, end))| TextLine {
                line: i,
                range: TextRange::new(
                    range
                        .start()
                        .checked_sub(start)
                        .unwrap_or(TextSize::from(0)),
                    range.end().min(end) - start, // sub OK because the bounds are always checked
                ),
                text: line,
            })
    }
}

fn main() -> Result<()> {
    let mut editor = rustyline::Editor::new();

    editor.set_helper(Some(LangHelper));

    let dirs = ProjectDirs::from(
        "org",            // qualifier
        "Xavier Lambein", // organization
        "PL Musings",     // application
    )
    .unwrap();
    fs::create_dir_all(dirs.data_local_dir())?;
    let history_file = dirs.data_local_dir().join("history.txt");
    if !history_file.exists() {
        fs::write(&history_file, "")?;
    }

    editor.load_history(&history_file)?;

    loop {
        match editor.readline("> ") {
            Ok(line) => {
                // Skip empty lines
                if line.trim().is_empty() {
                    continue;
                }

                editor.add_history_entry(&line);

                let (command, input) = parse_command(&line).unwrap_or((Command::Eval, &line));

                match command {
                    Command::Eval => {
                        let (node, _) = parse(input);
                        let expr = hir::lower(
                            ast::Root::cast(node.clone()).ok_or(anyhow!("no root node"))?,
                        );
                        let ty = match type_check(&stdlib::type_env(), &expr) {
                            Err(err) => {
                                let span = err.span;
                                eprintln!("{}", ReplError::TypeError(err));
                                if let Span::Range(range) = span {
                                    let lines = Lines::new(input);
                                    for (i, line) in lines.find(range).enumerate() {
                                        if i == 0 {
                                            eprintln!(
                                                "in REPL, on line {}, column {}:",
                                                line.line + 1,
                                                u32::from(line.range.start()) + 1
                                            );
                                            eprintln!();
                                        }
                                        eprintln!("{}{}", " ".repeat(4), line.text);
                                        eprintln!(
                                            "{}{}\x1b[1;31m{}\x1b[0m",
                                            " ".repeat(4),
                                            " ".repeat(line.range.start().into()),
                                            "^".repeat(line.range.len().into())
                                        );
                                    }
                                    eprintln!();
                                }
                                continue;
                            }
                            Ok(ty) => ty,
                        };
                        match eval(&stdlib::env(), expr) {
                            Ok(value) => println!("{} : {}", value, ty),
                            Err(err) => {
                                let span = err.span;
                                eprintln!("{}", ReplError::EvalError(err));
                                if let Span::Range(range) = span {
                                    let lines = Lines::new(input);
                                    for (i, line) in lines.find(range).enumerate() {
                                        if i == 0 {
                                            eprintln!(
                                                "in REPL, on line {}, column {}:",
                                                line.line + 1,
                                                u32::from(line.range.start()) + 1
                                            );
                                            eprintln!();
                                        }
                                        eprintln!("{}{}", " ".repeat(4), line.text);
                                        eprintln!(
                                            "{}{}\x1b[1;31m{}\x1b[0m",
                                            " ".repeat(4),
                                            " ".repeat(line.range.start().into()),
                                            "^".repeat(line.range.len().into())
                                        );
                                    }
                                    eprintln!();
                                }
                            }
                        }
                    }
                    Command::Cst => {
                        let (node, _) = parse(input);
                        pretty_print_node(&mut std::io::stdout(), &node)?;
                    }
                    Command::TypeCheck => {
                        let (node, _) = parse(input);
                        let expr = hir::lower(
                            ast::Root::cast(node.clone()).ok_or(anyhow!("no root node"))?,
                        );
                        match type_check(&stdlib::type_env(), &expr) {
                            Ok(ty) => println!("{}", ty),
                            Err(err) => {
                                eprintln!("{}", ReplError::TypeError(err));
                            }
                        }
                    }
                }
            }
            Err(ReadlineError::Interrupted) => {
                continue;
            }
            Err(ReadlineError::Eof) => {
                if atty::is(atty::Stream::Stdin) {
                    eprintln!("Goodbye :>");
                }
                break;
            }
            Err(err) => {
                eprintln!("{}", err);
                break;
            }
        }
    }

    editor.append_history(&history_file)?;

    Ok(())
}
