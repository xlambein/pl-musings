//! Parser for the language.
//!
//! This parser is event-based, meaning that the parser internally produces a
//! vector of events rather than a syntax tree, where each event indicates
//! whether to start/finish a node, add the next token to the current node, or
//! record a parsing error.  Only at a later stage is the syntax tree
//! reconstructed from the input tokens and the event stream.
//!
//! Parsing a syntax into an event stream allows us to ignore trivia token
//! (i.e., whitespace and comments) while parsing, and then to heuristically
//! attach them to specific nodes when constructing the syntax tree.
//!
//! Event-based parsing with [`nom`] is a bit complicated.  As a refresher,
//! `nom` parser combinators are functions with the signature `fn<Input,
//! Output>(input: Input) -> Result<(Input, Output), nom::Err>`, where the input
//! type is typically a slice of characters or tokens.  A parser reads from the
//! input, then either returns whatever it parsed along with the remaining
//! unparsed input, or produces an error.
//!
//! A simple but naive way of doing event-based parsing within this framework
//! would be for `Output` to be `Vec<Event>`: each parser returns a stream of
//! events, and a sequence combinator concatenates the result of its inner
//! parsers.  The main problem is that this requires a lot of short-lived memory
//! allocations, which is relatively inefficient.
//!
//! The alternative we use here is to "cheat" the signature of `nom` combinators
//! by passing the stream of events along with the input slice.  The signature
//! of each combinator becomes: `fn(input: ParsingState) ->
//! Result<(ParsingState, ()), nom::Err>` (note that the output is now always an
//! empty tuple `()`).  This allows us to "thread" the stream of events through
//! each sub-combinator, adding the events they produce without requiring extra
//! allocations.  It also makes the combinators easier to write, as the events
//! are automatically concatenated together.
//!
//! Combinators that check multiple alternative parsers need to `Clone` the
//! input and pass it to each sub-parser.  For this reason,
//! [`ParsingState`][`state::ParsingState`] is implemented such that cloning is
//! cheap, by making clones share the events they have in common.  Currently,
//! this is done with [`im::Vector`], but it might be possible to make a safe
//! alternative with better performance specifically tailored to our use case.

use nom::Finish;

use crate::{cst::SyntaxNode, lexer::tokenize};

mod combinators;
mod grammar;
mod state;

/// Parse an input string, producing a syntax tree and a vector of parsing
/// errors.
pub fn parse(src: &str) -> (SyntaxNode, Vec<ParseError>) {
    let tokens = tokenize(src);
    let parser = state::ParsingState::new(&tokens);
    grammar::root(parser)
        .finish()
        .expect("there should be no parsing errors")
        .0
        .finish()
}

/// Incomplete version of [`parse`], returning `None` if more input is needed.
pub fn parse_incomplete(src: &str) -> Option<(SyntaxNode, Vec<ParseError>)> {
    let tokens = tokenize(src);
    let parser = state::ParsingState::new(&tokens);
    match grammar::root(parser) {
        Ok((parser, ())) => {
            if parser.is_complete() {
                Some(parser.finish())
            } else {
                None
            }
        }
        Err(_) => {
            panic!("there should be no parsing error");
        }
    }
}

/// An error produced while parsing.
pub type ParseError = String;

/// Input type for parser combinators.
type Input<'t, 'src> = state::ParsingState<'t, 'src>;

/// Error type for parser combinators.
type IError<'t, 'src> = nom::error::Error<Input<'t, 'src>>;

/// Result type for parser combinators.
type IResult<'t, 'src> = nom::IResult<Input<'t, 'src>, (), IError<'t, 'src>>;

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    fn assert_parse_eq(src: &str, tree: &str) {
        let (node, _) = parse(src);
        assert_eq!(format!("{:#?}", node).trim(), tree.trim())
    }

    fn assert_parse_eq_with_errors(src: &str, tree: &str, expected_errors: &[&str]) {
        let (node, errors) = parse(src);
        assert_eq!(format!("{:#?}", node).trim(), tree.trim());
        assert_eq!(
            errors.into_iter().map(String::from).collect::<Vec<_>>(),
            expected_errors
        );
    }

    #[test]
    fn parse_expr() {
        assert_parse_eq(
            "-foo bar baz - 1 * -2 + 3 / pi * false - true",
            r#"
Root@0..45
  InfixOp@0..45
    InfixOp@0..38
      InfixOp@0..21
        PrefixOp@0..12
          Operator@0..1
            Hyphen@0..1 "-"
          Application@1..12
            Application@1..8
              Identifier@1..4
                Symbol@1..4 "foo"
              Whitespace@4..5 " "
              Identifier@5..8
                Symbol@5..8 "bar"
            Whitespace@8..9 " "
            Identifier@9..12
              Symbol@9..12 "baz"
        Whitespace@12..13 " "
        Operator@13..14
          Hyphen@13..14 "-"
        Whitespace@14..15 " "
        InfixOp@15..21
          NumberLit@15..16
            Number@15..16 "1"
          Whitespace@16..17 " "
          Operator@17..18
            Star@17..18 "*"
          Whitespace@18..19 " "
          PrefixOp@19..21
            Operator@19..20
              Hyphen@19..20 "-"
            NumberLit@20..21
              Number@20..21 "2"
      Whitespace@21..22 " "
      Operator@22..23
        Plus@22..23 "+"
      Whitespace@23..24 " "
      InfixOp@24..38
        InfixOp@24..30
          NumberLit@24..25
            Number@24..25 "3"
          Whitespace@25..26 " "
          Operator@26..27
            Slash@26..27 "/"
          Whitespace@27..28 " "
          Identifier@28..30
            Symbol@28..30 "pi"
        Whitespace@30..31 " "
        Operator@31..32
          Star@31..32 "*"
        Whitespace@32..33 " "
        BoolLit@33..38
          Bool@33..38 "false"
    Whitespace@38..39 " "
    Operator@39..40
      Hyphen@39..40 "-"
    Whitespace@40..41 " "
    BoolLit@41..45
      Bool@41..45 "true"
            "#,
        );
        assert_parse_eq(
            "foo && bar || baz == quux",
            r#"
Root@0..25
  InfixOp@0..25
    InfixOp@0..10
      Identifier@0..3
        Symbol@0..3 "foo"
      Whitespace@3..4 " "
      Operator@4..6
        DoubleAmpersand@4..6 "&&"
      Whitespace@6..7 " "
      Identifier@7..10
        Symbol@7..10 "bar"
    Whitespace@10..11 " "
    Operator@11..13
      DoublePipe@11..13 "||"
    Whitespace@13..14 " "
    InfixOp@14..25
      Identifier@14..17
        Symbol@14..17 "baz"
      Whitespace@17..18 " "
      Operator@18..20
        DoubleEqual@18..20 "=="
      Whitespace@20..21 " "
      Identifier@21..25
        Symbol@21..25 "quux"
        "#,
        );
    }

    #[test]
    fn parse_lambda() {
        assert_parse_eq(
            r#"\foo. 1 + foo"#,
            r#"
Root@0..13
  Lambda@0..13
    Backslash@0..1 "\\"
    Symbol@1..4 "foo"
    Dot@4..5 "."
    Whitespace@5..6 " "
    InfixOp@6..13
      NumberLit@6..7
        Number@6..7 "1"
      Whitespace@7..8 " "
      Operator@8..9
        Plus@8..9 "+"
      Whitespace@9..10 " "
      Identifier@10..13
        Symbol@10..13 "foo"
            "#,
        )
    }

    #[test]
    fn parse_let() {
        assert_parse_eq(
            r#"let x = 1 and y = 2 in x + y"#,
            r#"
Root@0..28
  Let@0..28
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..9
      Symbol@4..5 "x"
      Whitespace@5..6 " "
      Equal@6..7 "="
      Whitespace@7..8 " "
      NumberLit@8..9
        Number@8..9 "1"
    Whitespace@9..10 " "
    AndKw@10..13 "and"
    Whitespace@13..14 " "
    LetBinding@14..19
      Symbol@14..15 "y"
      Whitespace@15..16 " "
      Equal@16..17 "="
      Whitespace@17..18 " "
      NumberLit@18..19
        Number@18..19 "2"
    Whitespace@19..20 " "
    InKw@20..22 "in"
    Whitespace@22..23 " "
    InfixOp@23..28
      Identifier@23..24
        Symbol@23..24 "x"
      Whitespace@24..25 " "
      Operator@25..26
        Plus@25..26 "+"
      Whitespace@26..27 " "
      Identifier@27..28
        Symbol@27..28 "y"
            "#,
        )
    }

    #[test]
    fn parse_let_rec() {
        assert_parse_eq(
            r#"let rec x = 1 in x"#,
            r#"
Root@0..18
  Let@0..18
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    RecKw@4..7 "rec"
    Whitespace@7..8 " "
    LetBinding@8..13
      Symbol@8..9 "x"
      Whitespace@9..10 " "
      Equal@10..11 "="
      Whitespace@11..12 " "
      NumberLit@12..13
        Number@12..13 "1"
    Whitespace@13..14 " "
    InKw@14..16 "in"
    Whitespace@16..17 " "
    Identifier@17..18
      Symbol@17..18 "x"
            "#,
        )
    }

    #[test]
    fn parse_parens() {
        assert_parse_eq(
            "1 / (2 + 3) * 4",
            r#"
Root@0..15
  InfixOp@0..15
    InfixOp@0..11
      NumberLit@0..1
        Number@0..1 "1"
      Whitespace@1..2 " "
      Operator@2..3
        Slash@2..3 "/"
      Whitespace@3..4 " "
      ParenthesizedExpr@4..11
        LParen@4..5 "("
        InfixOp@5..10
          NumberLit@5..6
            Number@5..6 "2"
          Whitespace@6..7 " "
          Operator@7..8
            Plus@7..8 "+"
          Whitespace@8..9 " "
          NumberLit@9..10
            Number@9..10 "3"
        RParen@10..11 ")"
    Whitespace@11..12 " "
    Operator@12..13
      Star@12..13 "*"
    Whitespace@13..14 " "
    NumberLit@14..15
      Number@14..15 "4"
            "#,
        )
    }

    #[test]
    fn parse_conditional() {
        assert_parse_eq(
            "if true then 1 else 0",
            r#"
Root@0..21
  Conditional@0..21
    IfKw@0..2 "if"
    Whitespace@2..3 " "
    BoolLit@3..7
      Bool@3..7 "true"
    Whitespace@7..8 " "
    ThenKw@8..12 "then"
    Whitespace@12..13 " "
    NumberLit@13..14
      Number@13..14 "1"
    Whitespace@14..15 " "
    ElseKw@15..19 "else"
    Whitespace@19..20 " "
    NumberLit@20..21
      Number@20..21 "0"
            "#,
        )
    }

    #[test]
    fn parse_type_annotation() {
        assert_parse_eq(
            r#"(\f.\x.f x) : (Integer -> Real) -> Integer -> Real"#,
            r#"
Root@0..50
  InfixOp@0..50
    ParenthesizedExpr@0..11
      LParen@0..1 "("
      Lambda@1..10
        Backslash@1..2 "\\"
        Symbol@2..3 "f"
        Dot@3..4 "."
        Lambda@4..10
          Backslash@4..5 "\\"
          Symbol@5..6 "x"
          Dot@6..7 "."
          Application@7..10
            Identifier@7..8
              Symbol@7..8 "f"
            Whitespace@8..9 " "
            Identifier@9..10
              Symbol@9..10 "x"
      RParen@10..11 ")"
    Whitespace@11..12 " "
    Operator@12..13
      Colon@12..13 ":"
    Whitespace@13..14 " "
    InfixOp@14..50
      ParenthesizedExpr@14..31
        LParen@14..15 "("
        InfixOp@15..30
          Identifier@15..22
            Symbol@15..22 "Integer"
          Whitespace@22..23 " "
          Operator@23..25
            RightArrow@23..25 "->"
          Whitespace@25..26 " "
          Identifier@26..30
            Symbol@26..30 "Real"
        RParen@30..31 ")"
      Whitespace@31..32 " "
      Operator@32..34
        RightArrow@32..34 "->"
      Whitespace@34..35 " "
      InfixOp@35..50
        Identifier@35..42
          Symbol@35..42 "Integer"
        Whitespace@42..43 " "
        Operator@43..45
          RightArrow@43..45 "->"
        Whitespace@45..46 " "
        Identifier@46..50
          Symbol@46..50 "Real"
          "#,
        )
    }

    #[test]
    fn parse_empty_record() {
        assert_parse_eq(
            "{}",
            r#"
Root@0..2
  Record@0..2
    LBrace@0..1 "{"
    RBrace@1..2 "}"
            "#,
        )
    }

    #[test]
    fn parse_record() {
        assert_parse_eq(
            r#"{foo=123, bar=\x.x,}"#,
            r#"
Root@0..20
  Record@0..20
    LBrace@0..1 "{"
    Field@1..8
      Identifier@1..4
        Symbol@1..4 "foo"
      Equal@4..5 "="
      NumberLit@5..8
        Number@5..8 "123"
    Comma@8..9 ","
    Whitespace@9..10 " "
    Field@10..18
      Identifier@10..13
        Symbol@10..13 "bar"
      Equal@13..14 "="
      Lambda@14..18
        Backslash@14..15 "\\"
        Symbol@15..16 "x"
        Dot@16..17 "."
        Identifier@17..18
          Symbol@17..18 "x"
    Comma@18..19 ","
    RBrace@19..20 "}"
            "#,
        )
    }

    #[test]
    fn parse_record_type() {
        assert_parse_eq(
            r#"{foo : Integer, bar : Bool -> Bool}"#,
            r#"
Root@0..35
  Record@0..35
    LBrace@0..1 "{"
    FieldType@1..14
      Identifier@1..4
        Symbol@1..4 "foo"
      Whitespace@4..5 " "
      Colon@5..6 ":"
      Whitespace@6..7 " "
      Identifier@7..14
        Symbol@7..14 "Integer"
    Comma@14..15 ","
    Whitespace@15..16 " "
    FieldType@16..34
      Identifier@16..19
        Symbol@16..19 "bar"
      Whitespace@19..20 " "
      Colon@20..21 ":"
      Whitespace@21..22 " "
      InfixOp@22..34
        Identifier@22..26
          Symbol@22..26 "Bool"
        Whitespace@26..27 " "
        Operator@27..29
          RightArrow@27..29 "->"
        Whitespace@29..30 " "
        Identifier@30..34
          Symbol@30..34 "Bool"
    RBrace@34..35 "}"
            "#,
        )
    }

    #[test]
    fn parse_record_access() {
        assert_parse_eq(
            r#"{x = 1}.x"#,
            r#"
Root@0..9
  RecordAccess@0..9
    Record@0..7
      LBrace@0..1 "{"
      Field@1..6
        Identifier@1..2
          Symbol@1..2 "x"
        Whitespace@2..3 " "
        Equal@3..4 "="
        Whitespace@4..5 " "
        NumberLit@5..6
          Number@5..6 "1"
      RBrace@6..7 "}"
    Dot@7..8 "."
    Identifier@8..9
      Symbol@8..9 "x"
            "#,
        )
    }

    #[test]
    fn parse_empty_variant() {
        assert_parse_eq(
            "{||}",
            r#"
Root@0..4
  Variant@0..4
    LBracePipe@0..2 "{|"
    RBracePipe@2..4 "|}"
            "#,
        )
    }

    #[test]
    fn parse_variant() {
        assert_parse_eq(
            r#"{|x = 1|}"#,
            r#"
Root@0..9
  Variant@0..9
    LBracePipe@0..2 "{|"
    Field@2..7
      Identifier@2..3
        Symbol@2..3 "x"
      Whitespace@3..4 " "
      Equal@4..5 "="
      Whitespace@5..6 " "
      NumberLit@6..7
        Number@6..7 "1"
    RBracePipe@7..9 "|}"
            "#,
        )
    }

    // TODO should this be a parse error?
    // #[test]
    // fn parse_multiple_variants() {
    //     assert_parse_eq_with_errors(
    //         r#"{|x = 1, y = true|}"#,
    //         r#"
    //         "#,
    //         &[],
    //     )
    // }

    #[test]
    fn parse_variant_type() {
        assert_parse_eq(
            r#"{|x : Integer, y : Bool|}"#,
            r#"
Root@0..25
  Variant@0..25
    LBracePipe@0..2 "{|"
    FieldType@2..13
      Identifier@2..3
        Symbol@2..3 "x"
      Whitespace@3..4 " "
      Colon@4..5 ":"
      Whitespace@5..6 " "
      Identifier@6..13
        Symbol@6..13 "Integer"
    Comma@13..14 ","
    Whitespace@14..15 " "
    FieldType@15..23
      Identifier@15..16
        Symbol@15..16 "y"
      Whitespace@16..17 " "
      Colon@17..18 ":"
      Whitespace@18..19 " "
      Identifier@19..23
        Symbol@19..23 "Bool"
    RBracePipe@23..25 "|}"
            "#,
        )
    }

    #[test]
    fn parse_variant_mixed() {
        assert_parse_eq_with_errors(
            r#"{|x = 1, y : Bool|}"#,
            r#"
Root@0..19
  Variant@0..19
    LBracePipe@0..2 "{|"
    Field@2..7
      Identifier@2..3
        Symbol@2..3 "x"
      Whitespace@3..4 " "
      Equal@4..5 "="
      Whitespace@5..6 " "
      NumberLit@6..7
        Number@6..7 "1"
    Comma@7..8 ","
    Whitespace@8..9 " "
    FieldType@9..17
      Identifier@9..10
        Symbol@9..10 "y"
      Whitespace@10..11 " "
      Colon@11..12 ":"
      Whitespace@12..13 " "
      Identifier@13..17
        Symbol@13..17 "Bool"
    RBracePipe@17..19 "|}"
            "#,
            &["found both values and type annotations"],
        )
    }

    #[test]
    fn parse_case_single() {
        assert_parse_eq(
            r#"case 1 + 2 of 3 -> 4"#,
            r#"
Root@0..20
  Case@0..20
    CaseKw@0..4 "case"
    Whitespace@4..5 " "
    InfixOp@5..10
      NumberLit@5..6
        Number@5..6 "1"
      Whitespace@6..7 " "
      Operator@7..8
        Plus@7..8 "+"
      Whitespace@8..9 " "
      NumberLit@9..10
        Number@9..10 "2"
    Whitespace@10..11 " "
    OfKw@11..13 "of"
    Whitespace@13..14 " "
    CaseArm@14..20
      Pattern@14..15
        NumberLit@14..15
          Number@14..15 "3"
      Whitespace@15..16 " "
      RightArrow@16..18 "->"
      Whitespace@18..19 " "
      NumberLit@19..20
        Number@19..20 "4"
            "#,
        )
    }

    #[test]
    fn parse_case_multiple() {
        assert_parse_eq(
            r#"case {|hello = world|} of 3 -> 4, {foo = bar} -> true,"#,
            r#"
Root@0..54
  Case@0..54
    CaseKw@0..4 "case"
    Whitespace@4..5 " "
    Variant@5..22
      LBracePipe@5..7 "{|"
      Field@7..20
        Identifier@7..12
          Symbol@7..12 "hello"
        Whitespace@12..13 " "
        Equal@13..14 "="
        Whitespace@14..15 " "
        Identifier@15..20
          Symbol@15..20 "world"
      RBracePipe@20..22 "|}"
    Whitespace@22..23 " "
    OfKw@23..25 "of"
    Whitespace@25..26 " "
    CaseArm@26..32
      Pattern@26..27
        NumberLit@26..27
          Number@26..27 "3"
      Whitespace@27..28 " "
      RightArrow@28..30 "->"
      Whitespace@30..31 " "
      NumberLit@31..32
        Number@31..32 "4"
    Comma@32..33 ","
    Whitespace@33..34 " "
    CaseArm@34..53
      Pattern@34..45
        Record@34..45
          LBrace@34..35 "{"
          Field@35..44
            Identifier@35..38
              Symbol@35..38 "foo"
            Whitespace@38..39 " "
            Equal@39..40 "="
            Whitespace@40..41 " "
            Identifier@41..44
              Symbol@41..44 "bar"
          RBrace@44..45 "}"
      Whitespace@45..46 " "
      RightArrow@46..48 "->"
      Whitespace@48..49 " "
      BoolLit@49..53
        Bool@49..53 "true"
    Comma@53..54 ","
            "#,
        )
    }

    #[test]
    fn parse_case_bad_pattern() {
        assert_parse_eq_with_errors(
            r#"case x of 1 + 2 -> 3"#,
            r#"
Root@0..20
  InfixOp@0..20
    InfixOp@0..15
      Case@0..11
        CaseKw@0..4 "case"
        Whitespace@4..5 " "
        Identifier@5..6
          Symbol@5..6 "x"
        Whitespace@6..7 " "
        OfKw@7..9 "of"
        Whitespace@9..10 " "
        CaseArm@10..11
          Pattern@10..11
            NumberLit@10..11
              Number@10..11 "1"
      Whitespace@11..12 " "
      Operator@12..13
        Plus@12..13 "+"
      Whitespace@13..14 " "
      NumberLit@14..15
        Number@14..15 "2"
    Whitespace@15..16 " "
    Operator@16..18
      RightArrow@16..18 "->"
    Whitespace@18..19 " "
    NumberLit@19..20
      Number@19..20 "3"
            "#,
            &["missing `->`", "missing expression"],
        )
    }

    #[test]
    fn parse_paren_missing_expr() {
        assert_parse_eq_with_errors(
            "1 + (",
            r#"
Root@0..5
  InfixOp@0..5
    NumberLit@0..1
      Number@0..1 "1"
    Whitespace@1..2 " "
    Operator@2..3
      Plus@2..3 "+"
    Whitespace@3..4 " "
    ParenthesizedExpr@4..5
      LParen@4..5 "("
            "#,
            &["missing expression after `(`", "missing `)`"],
        )
    }

    #[test]
    fn parse_missing_paren() {
        assert_parse_eq_with_errors(
            "(1 + 2",
            r#"
Root@0..6
  ParenthesizedExpr@0..6
    LParen@0..1 "("
    InfixOp@1..6
      NumberLit@1..2
        Number@1..2 "1"
      Whitespace@2..3 " "
      Operator@3..4
        Plus@3..4 "+"
      Whitespace@4..5 " "
      NumberLit@5..6
        Number@5..6 "2"
            "#,
            &["missing `)`"],
        )
    }

    #[test]
    fn parse_extra_paren() {
        assert_parse_eq_with_errors(
            "1 + 2)",
            r#"
Root@0..6
  InfixOp@0..5
    NumberLit@0..1
      Number@0..1 "1"
    Whitespace@1..2 " "
    Operator@2..3
      Plus@2..3 "+"
    Whitespace@3..4 " "
    NumberLit@4..5
      Number@4..5 "2"
  Error@5..6
    RParen@5..6 ")"
            "#,
            &["unexpected trailing tokens"],
        )
    }

    #[test]
    fn parse_missing_operand() {
        assert_parse_eq_with_errors(
            "1 + ",
            r#"
Root@0..4
  InfixOp@0..3
    NumberLit@0..1
      Number@0..1 "1"
    Whitespace@1..2 " "
    Operator@2..3
      Plus@2..3 "+"
  Whitespace@3..4 " "
            "#,
            &["missing operand after operator"],
        )
    }

    #[test]
    fn parse_missing_lambda_argument() {
        assert_parse_eq_with_errors(
            r#"\"#,
            r#"
Root@0..1
  Lambda@0..1
    Backslash@0..1 "\\"
            "#,
            &[
                "missing lambda argument after `\\`",
                "missing `.` after lambda header",
                "missing lambda body",
            ],
        )
    }

    #[test]
    fn parse_missing_lambda_dot() {
        assert_parse_eq_with_errors(
            r#"\x"#,
            r#"
Root@0..2
  Lambda@0..2
    Backslash@0..1 "\\"
    Symbol@1..2 "x"
            "#,
            &["missing `.` after lambda header", "missing lambda body"],
        );
        assert_parse_eq_with_errors(
            r#"\x y"#,
            r#"
Root@0..4
  Lambda@0..4
    Backslash@0..1 "\\"
    Symbol@1..2 "x"
    Whitespace@2..3 " "
    Identifier@3..4
      Symbol@3..4 "y"
            "#,
            &["missing `.` after lambda header"],
        )
    }

    #[test]
    fn parse_missing_lambda_body() {
        assert_parse_eq_with_errors(
            r#"\x."#,
            r#"
Root@0..3
  Lambda@0..3
    Backslash@0..1 "\\"
    Symbol@1..2 "x"
    Dot@2..3 "."
            "#,
            &["missing lambda body"],
        )
    }

    #[test]
    fn parse_missing_let_identifier() {
        assert_parse_eq_with_errors(
            r#"let = 1 in x"#,
            r#"
Root@0..12
  Let@0..12
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..7
      Equal@4..5 "="
      Whitespace@5..6 " "
      NumberLit@6..7
        Number@6..7 "1"
    Whitespace@7..8 " "
    InKw@8..10 "in"
    Whitespace@10..11 " "
    Identifier@11..12
      Symbol@11..12 "x"
            "#,
            &["missing identifier in let binding"],
        )
    }

    #[test]
    fn parse_missing_let_equal() {
        assert_parse_eq_with_errors(
            r#"let x 1 in x"#,
            r#"
Root@0..12
  Let@0..12
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..7
      Symbol@4..5 "x"
      Whitespace@5..6 " "
      NumberLit@6..7
        Number@6..7 "1"
    Whitespace@7..8 " "
    InKw@8..10 "in"
    Whitespace@10..11 " "
    Identifier@11..12
      Symbol@11..12 "x"
            "#,
            &["missing `=` after identifier in let binding"],
        )
    }

    #[test]
    fn parse_missing_let_value() {
        assert_parse_eq_with_errors(
            r#"let x = in x"#,
            r#"
Root@0..12
  Let@0..12
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..7
      Symbol@4..5 "x"
      Whitespace@5..6 " "
      Equal@6..7 "="
    Whitespace@7..8 " "
    InKw@8..10 "in"
    Whitespace@10..11 " "
    Identifier@11..12
      Symbol@11..12 "x"
            "#,
            &["missing value in let binding"],
        )
    }

    #[test]
    fn parse_missing_let_in_and_body() {
        assert_parse_eq_with_errors(
            r#"let x = 1"#,
            r#"
Root@0..9
  Let@0..9
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..9
      Symbol@4..5 "x"
      Whitespace@5..6 " "
      Equal@6..7 "="
      Whitespace@7..8 " "
      NumberLit@8..9
        Number@8..9 "1"
"#,
            &[
                "missing `in` keyword after value in let expression",
                "missing body in let expression",
            ],
        )
    }

    #[test]
    fn parse_missing_let_body() {
        assert_parse_eq_with_errors(
            r#"let x = 1 in"#,
            r#"
Root@0..12
  Let@0..12
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..9
      Symbol@4..5 "x"
      Whitespace@5..6 " "
      Equal@6..7 "="
      Whitespace@7..8 " "
      NumberLit@8..9
        Number@8..9 "1"
    Whitespace@9..10 " "
    InKw@10..12 "in"
"#,
            &["missing body in let expression"],
        )
    }

    #[test]
    fn parse_missing_let_trailing_and() {
        assert_parse_eq_with_errors(
            r#"let x = 1 and in x"#,
            r#"
Root@0..18
  Let@0..18
    LetKw@0..3 "let"
    Whitespace@3..4 " "
    LetBinding@4..9
      Symbol@4..5 "x"
      Whitespace@5..6 " "
      Equal@6..7 "="
      Whitespace@7..8 " "
      NumberLit@8..9
        Number@8..9 "1"
    Whitespace@9..10 " "
    AndKw@10..13 "and"
    Whitespace@13..14 " "
    LetBinding@14..14
    InKw@14..16 "in"
    Whitespace@16..17 " "
    Identifier@17..18
      Symbol@17..18 "x"
"#,
            &[
                "missing identifier in let binding",
                "missing `=` after identifier in let binding",
                "missing value in let binding",
            ],
        )
    }

    #[test]
    fn parse_record_duplicate_key() {
        assert_parse_eq_with_errors(
            r#"{a = 1, a = true}"#,
            r#"
Root@0..17
  Record@0..17
    LBrace@0..1 "{"
    Field@1..6
      Identifier@1..2
        Symbol@1..2 "a"
      Whitespace@2..3 " "
      Equal@3..4 "="
      Whitespace@4..5 " "
      NumberLit@5..6
        Number@5..6 "1"
    Comma@6..7 ","
    Whitespace@7..8 " "
    Field@8..16
      Identifier@8..9
        Symbol@8..9 "a"
      Whitespace@9..10 " "
      Equal@10..11 "="
      Whitespace@11..12 " "
      BoolLit@12..16
        Bool@12..16 "true"
    RBrace@16..17 "}"
"#,
            &["duplicate field 'a'"],
        )
    }

    #[test]
    fn parse_record_mixed() {
        assert_parse_eq_with_errors(
            r#"{a = 1, b : Bool}"#,
            r#"
Root@0..17
  Record@0..17
    LBrace@0..1 "{"
    Field@1..6
      Identifier@1..2
        Symbol@1..2 "a"
      Whitespace@2..3 " "
      Equal@3..4 "="
      Whitespace@4..5 " "
      NumberLit@5..6
        Number@5..6 "1"
    Comma@6..7 ","
    Whitespace@7..8 " "
    FieldType@8..16
      Identifier@8..9
        Symbol@8..9 "b"
      Whitespace@9..10 " "
      Colon@10..11 ":"
      Whitespace@11..12 " "
      Identifier@12..16
        Symbol@12..16 "Bool"
    RBrace@16..17 "}"
"#,
            &["found both values and type annotations"],
        )
    }
}
