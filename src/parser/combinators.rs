//! Custom [`nom`] combinators for event-based parsing.

use nom::{combinator::complete, InputLength};

use crate::{
    cst::SyntaxKind,
    lexer::TokenKind,
    parser::{IError, IResult, Input, ParseError},
};

/// Wrap a parser in a node.
pub(super) fn node<'t, 'src: 't, P, O>(
    mut parser: P,
    kind: SyntaxKind,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    P: nom::Parser<Input<'t, 'src>, O, IError<'t, 'src>>,
{
    move |mut i: Input<'t, 'src>| {
        i.start_node(kind);
        let (mut i, _) = parser.parse(i)?;
        i.end_node();
        Ok((i, ()))
    }
}

/// Parse a non-associative infix operation.
///
/// Recognizes `inner (op inner)`.
pub(super) fn infix<'t, 'src: 't, Op, Inner>(
    op: Op,
    mut inner: Inner,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    Op: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    Inner: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>> + Copy,
{
    let mut op = node(complete(op), SyntaxKind::Operator);
    move |mut i: Input<'t, 'src>| {
        let ckpt = i.start_node_multiple(SyntaxKind::InfixOp);
        let (i, ()) = inner.parse(i)?;

        match op(i.clone()) {
            Ok((i, ())) => {
                let (mut i, ()) = expect(inner, "missing operand after operator")(i)?;
                i.end_node_multiple(ckpt);

                return Ok((i, ()));
            }
            Err(nom::Err::Error(_)) => {
                return Ok((i, ()));
            }
            Err(e) => return Err(e),
        }
    }
}

/// Parse a left-associative infix operation.
///
/// Recognizes `inner (op inner)*`, and groups terms in a left-associative
/// fashion.
pub(super) fn infix_left<'t, 'src: 't, Op, Inner>(
    op: Op,
    mut inner: Inner,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    Op: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    Inner: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>> + Copy,
{
    let mut op = node(complete(op), SyntaxKind::Operator);
    move |mut i: Input<'t, 'src>| {
        let ckpt = i.start_node_multiple(SyntaxKind::InfixOp);
        let (mut input, ()) = inner.parse(i)?;

        loop {
            let len = input.input_len();
            match op(input.clone()) {
                Ok((i, ())) => {
                    // Infinite loop check
                    if i.input_len() == len {
                        use nom::error::ParseError;
                        return Err(nom::Err::Error(IError::from_error_kind(
                            input,
                            nom::error::ErrorKind::Many0,
                        )));
                    }

                    let (mut i, ()) = expect(inner, "missing operand after operator")(i)?;
                    i.end_node_multiple(ckpt);

                    input = i;
                }
                Err(nom::Err::Error(_)) => {
                    return Ok((input, ()));
                }
                Err(e) => return Err(e),
            }
        }
    }
}

/// Parse an asymmetric left-associative infix operation.
///
/// Recognizes `left (op right)*`, and groups terms in a left-associative
/// fashion.
///
/// When `right` is misparsed, produces a parsing error with message
/// `missing_right`, then keep parsing.
pub(super) fn asymmetric_infix_left<'t, 'src: 't, Op, Left, Right>(
    kind: SyntaxKind,
    mut left: Left,
    op: Op,
    right: Right,
    missing_right: &'static str,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    Op: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    Left: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    Right: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>> + Copy,
{
    let mut op = complete(op);
    move |mut i: Input<'t, 'src>| {
        let ckpt = i.start_node_multiple(kind);
        let (mut input, ()) = left.parse(i)?;

        loop {
            let len = input.input_len();
            match op(input.clone()) {
                Ok((i, ())) => {
                    // Infinite loop check
                    if i.input_len() == len {
                        use nom::error::ParseError;
                        return Err(nom::Err::Error(IError::from_error_kind(
                            input,
                            nom::error::ErrorKind::Many0,
                        )));
                    }

                    let (mut i, ()) = expect(right, missing_right)(i)?;
                    i.end_node_multiple(ckpt);

                    input = i;
                }
                Err(nom::Err::Error(_)) => {
                    return Ok((input, ()));
                }
                Err(e) => return Err(e),
            }
        }
    }
}

/// Parse a right-associative infix operation.
///
/// Recognizes `inner (op inner)*`, and groups terms in a right-associative
/// fashion.
pub(super) fn infix_right<'t, 'src: 't, Op, Inner>(
    op: Op,
    mut inner: Inner,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    Op: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    Inner: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>> + Copy,
{
    let mut op = node(complete(op), SyntaxKind::Operator);
    move |mut i: Input<'t, 'src>| {
        let mut ckpts = vec![i.start_node_multiple(SyntaxKind::InfixOp)];
        let (mut input, ()) = inner.parse(i)?;

        loop {
            let len = input.input_len();
            match op(input.clone()) {
                Ok((mut i, ())) => {
                    // Infinite loop check
                    if i.input_len() == len {
                        use nom::error::ParseError;
                        return Err(nom::Err::Error(IError::from_error_kind(
                            input,
                            nom::error::ErrorKind::Many0,
                        )));
                    }

                    ckpts.push(i.start_node_multiple(SyntaxKind::InfixOp));
                    let (i, ()) = expect(inner, "missing operand after operator")(i)?;

                    input = i;
                }
                Err(nom::Err::Error(_)) => {
                    ckpts.pop();
                    for ckpt in ckpts {
                        input.end_node_multiple(ckpt);
                    }
                    return Ok((input, ()));
                }
                Err(e) => return Err(e),
            }
        }
    }
}

/// Parse a separated syntax.
///
/// Recognizes `inner (sep inner)*`.  If `missing` is `None`, then accept an
/// optional trailing `sep`.  Otherwise, produce an error with `missing` as
/// message when `sep` is not followed by `inner`.
pub(super) fn separated1<'t, 'src: 't, Sep, Inner>(
    sep: Sep,
    mut inner: Inner,
    missing: Option<&'static str>,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    Sep: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    Inner: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>> + Copy,
{
    let mut sep = complete(sep);
    move |i: Input<'t, 'src>| {
        let (mut input, ()) = inner.parse(i)?;

        loop {
            let len = input.input_len();
            match sep(input.clone()) {
                Ok((i, ())) => {
                    // Infinite loop check
                    if i.input_len() == len {
                        use nom::error::ParseError;
                        return Err(nom::Err::Error(IError::from_error_kind(
                            input,
                            nom::error::ErrorKind::SeparatedList,
                        )));
                    }

                    if let Some(missing) = missing {
                        let (i, ()) = expect(inner, missing)(i)?;

                        input = i;
                    } else {
                        match inner.parse(i.clone()) {
                            Ok((i, ())) => {
                                input = i;
                            }
                            err @ Err(nom::Err::Failure(_)) => return err,
                            Err(_) => return Ok((i, ())),
                        }
                    }
                }
                Err(nom::Err::Error(_)) => {
                    return Ok((input, ()));
                }
                Err(e) => return Err(e),
            }
        }
    }
}

/// Convert a `nom` error into a parsing error, then succeed.
pub(super) fn expect<'t, 'src: 't, P, S>(
    mut parser: P,
    msg: S,
) -> impl FnMut(Input<'t, 'src>) -> IResult<'t, 'src>
where
    P: nom::Parser<Input<'t, 'src>, (), IError<'t, 'src>>,
    S: Into<ParseError>,
{
    let msg = msg.into();
    move |mut i: Input<'t, 'src>| {
        parser.parse(i.clone()).or_else(|err| match err {
            nom::Err::Error(_) => {
                i.error(msg.clone());
                Ok((i, ()))
            }
            nom::Err::Incomplete(_) => {
                i.incomplete(msg.clone());
                Ok((i, ()))
            }
            err => Err(err),
        })
    }
}

/// Read a token of a given kind.
pub(super) fn token<'t, 'src: 't>(t: TokenKind) -> impl Fn(Input<'t, 'src>) -> IResult<'t, 'src> {
    move |mut i: Input<'t, 'src>| {
        if i.eat(t) {
            Ok((i, ()))
        } else if i.is_empty() {
            Err(nom::Err::Incomplete(nom::Needed::Unknown))
        } else {
            Err(nom::Err::Error(nom::error::make_error(
                i,
                nom::error::ErrorKind::Tag,
            )))
        }
    }
}
