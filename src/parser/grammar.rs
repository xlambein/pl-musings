//! The actual grammar of the language, implemented as [`nom`] combinators.

use std::collections::HashSet;

use crate::{
    cst::SyntaxKind,
    lexer::{Token, TokenKind},
    parser::{
        combinators::{expect, infix_left, node, token},
        IResult, Input,
    },
};
use nom::{
    branch::alt,
    combinator::{opt, recognize},
    sequence::tuple,
};

use super::combinators::{asymmetric_infix_left, infix, infix_right, separated1};

/// Parse a file.
pub(super) fn root<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    let (mut i, _) = expect(expr, "missing expression")(i)?;
    // Eat extra tokens, if any, and wrap into an error
    if i.next().is_some() {
        i.error("unexpected trailing tokens".into());
        i.start_node(SyntaxKind::Error);
        while i.bump() {}
        i.end_node();
    }
    Ok((i, ()))
}

/// Parse an expression.
fn expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    type_annotation_expr(i)
}

/// Parse a type annotation.
fn type_annotation_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix(token(TokenKind::Colon), fun_arrow_expr)(i)
}

/// Parse a type function arrow.
fn fun_arrow_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix_right(token(TokenKind::RightArrow), rec_expr)(i)
}

/// Parse a recursive type.
fn rec_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    alt((
        node(
            tuple((
                token(TokenKind::RecKw),
                expect(identifier, "missing type variable after `rec`"),
                expect(token(TokenKind::Dot), "missing dot after type variable"),
                expect(rec_expr, "missing expression after rec"),
            )),
            SyntaxKind::Rec,
        ),
        or_expr,
    ))(i)
}

/// Parse a logical "or".
fn or_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix_right(token(TokenKind::DoublePipe), and_expr)(i)
}

/// Parse a logical "or".
fn and_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix_right(token(TokenKind::DoubleAmpersand), eq_expr)(i)
}

/// Parse an (in)equality.
fn eq_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix(
        alt((
            token(TokenKind::DoubleEqual),
            token(TokenKind::BangEqual),
            token(TokenKind::LAngle),
            token(TokenKind::RAngle),
            token(TokenKind::LAngleEqual),
            token(TokenKind::RAngleEqual),
        )),
        add_expr,
    )(i)
}

/// Parse an addition or substraction.
fn add_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix_left(
        alt((token(TokenKind::Plus), token(TokenKind::Hyphen))),
        mul_expr,
    )(i)
}

/// Parse a multiplication or division.
fn mul_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    infix_left(
        alt((token(TokenKind::Star), token(TokenKind::Slash))),
        negative_prefix,
    )(i)
}

/// Parse a prefix `-`.
fn negative_prefix<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    alt((
        node(
            tuple((
                node(token(TokenKind::Hyphen), SyntaxKind::Operator),
                application,
            )),
            SyntaxKind::PrefixOp,
        ),
        application,
    ))(i)
}

/// Parse a function application.
fn application<'t, 'src>(mut i: Input<'t, 'src>) -> IResult<'t, 'src> {
    let ckpt = i.start_node_multiple(SyntaxKind::Application);
    let (mut input, ()) = record_access(i)?;

    loop {
        match record_access(input.clone()) {
            Ok((mut i, ())) => {
                i.end_node_multiple(ckpt);
                input = i;
            }
            Err(nom::Err::Error(_) | nom::Err::Incomplete(_)) => {
                return Ok((input, ()));
            }
            Err(e) => return Err(e),
        }
    }
}

/// Parse a record field access `record.field`.
fn record_access<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    asymmetric_infix_left(
        SyntaxKind::RecordAccess,
        atom,
        token(TokenKind::Dot),
        identifier,
        "expected identifier",
    )(i)
}

/// Parse an atom (value, identifier or parenthesized expression).
fn atom<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    alt((
        bool,
        number,
        identifier,
        lambda,
        r#let,
        conditional,
        case,
        paren_expr,
        record,
        variant,
    ))(i)
}

/// Parse a boolean.
fn bool<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(token(TokenKind::Bool), SyntaxKind::BoolLit)(i)
}

/// Parse a number.
fn number<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(token(TokenKind::Number), SyntaxKind::NumberLit)(i)
}

/// Parse a identifier.
fn identifier<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(token(TokenKind::Symbol), SyntaxKind::Identifier)(i)
}

/// Parse a function declaration.
fn lambda<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::Backslash),
            expect(
                token(TokenKind::Symbol),
                "missing lambda argument after `\\`",
            ),
            expect(token(TokenKind::Dot), "missing `.` after lambda header"),
            expect(expr, "missing lambda body"),
        )),
        SyntaxKind::Lambda,
    )(i)
}

/// Parse a let expression.
fn r#let<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::LetKw),
            opt(token(TokenKind::RecKw)),
            separated1(
                token(TokenKind::AndKw),
                let_binding,
                Some("missing let binding"),
            ),
            expect(
                token(TokenKind::InKw),
                "missing `in` keyword after value in let expression",
            ),
            expect(expr, "missing body in let expression"),
        )),
        SyntaxKind::Let,
    )(i)
}

/// Parse a let binding.
fn let_binding<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            expect(
                token(TokenKind::Symbol),
                "missing identifier in let binding",
            ),
            opt(tuple((token(TokenKind::Colon), forall_expr))),
            expect(
                token(TokenKind::Equal),
                "missing `=` after identifier in let binding",
            ),
            expect(expr, "missing value in let binding"),
        )),
        SyntaxKind::LetBinding,
    )(i)
}

/// Parse a forall type.
fn forall_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    alt((
        node(
            tuple((
                token(TokenKind::ForAllKw),
                expect(identifier, "missing type variable after `forall`"),
                expect(token(TokenKind::Dot), "missing dot after type variable"),
                expect(forall_expr, "missing expression after forall"),
            )),
            SyntaxKind::ForAll,
        ),
        fun_arrow_expr,
    ))(i)
}

/// Parse a function declaration.
fn conditional<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::IfKw),
            expect(expr, "missing condition after `if`"),
            expect(
                token(TokenKind::ThenKw),
                "missing `then` keyword in conditional",
            ),
            expect(expr, "missing 'then' branch of the conditional"),
            expect(
                token(TokenKind::ElseKw),
                "missing `else` keyword in conditional",
            ),
            expect(expr, "missing 'else' branch of the conditional"),
        )),
        SyntaxKind::Conditional,
    )(i)
}

/// Parse a case expression.
fn case<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::CaseKw),
            expect(expr, "missing expression"),
            expect(token(TokenKind::OfKw), "missing `of`"),
            separated1(token(TokenKind::Comma), case_arm, None),
        )),
        SyntaxKind::Case,
    )(i)
}

/// Parse an arm of a case expression.
fn case_arm<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            pattern,
            expect(token(TokenKind::RightArrow), "missing `->`"),
            expect(expr, "missing expression"),
        )),
        SyntaxKind::CaseArm,
    )(i)
}

/// Parse a pattern of a case arm.
fn pattern<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        alt((bool, number, identifier, record, variant)),
        SyntaxKind::Pattern,
    )(i)
}

/// Parse a parenthesized expression.
fn paren_expr<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::LParen),
            expect(expr, "missing expression after `(`"),
            expect(token(TokenKind::RParen), "missing `)`"),
        )),
        SyntaxKind::ParenthesizedExpr,
    )(i)
}

/// Parse the fields of a record or variant.
///
/// TODO this is not very declarative, to say the least.  I'm not sure how it
///   could be improved, unfortunately.
fn fields<'t, 'src>(mut input: Input<'t, 'src>) -> IResult<'t, 'src> {
    let mut idents = HashSet::new();

    // Kind of record: values (SyntaxKind::Equal) or annotations
    // (SyntaxKind::Colon)
    let mut kind = None;

    loop {
        let mut i = input.clone();

        // Prepare to make either a values or annotations record field
        // TODO it would be nicer if we could specify the node when calling
        //   `end_node_multiple`
        let ckpt_value = i.start_node_multiple(SyntaxKind::Field);
        let ckpt_type = i.start_node_multiple(SyntaxKind::FieldType);

        // Read identifier, checking for duplicates
        match recognize(identifier)(i.clone()) {
            Ok((rest, mut read)) => {
                i = rest;
                let ident = read.next().unwrap().src;
                if idents.contains(ident) {
                    // On duplicate, record error but keep parsing
                    i.error(format!("duplicate field '{ident}'"));
                } else {
                    idents.insert(ident);
                }
            }
            Err(err @ nom::Err::Failure(_)) => return Err(err),
            Err(_) => break,
        }

        // Read either `=` or `:`, checking that the record doesn't have both
        let ckpt = match i.next() {
            Some(Token {
                kind: next @ (TokenKind::Equal | TokenKind::Colon),
                ..
            }) => {
                if next != *kind.get_or_insert(next) {
                    // If field has the wrong kind, record error but keep
                    // parsing
                    i.error("found both values and type annotations".to_owned());
                }
                i.bump();
                match next {
                    TokenKind::Equal => ckpt_value,
                    TokenKind::Colon => ckpt_type,
                    _ => unreachable!(),
                }
            }
            // Unexpected token: don't advance and keep parsing
            Some(_) => {
                i.error("expected `=` or `:`".to_owned());
                // Defaulting to values arbitrarily
                ckpt_value
            }
            None => return Err(nom::Err::Incomplete(nom::Needed::Unknown)),
        };

        // Finally, parse the expression
        (i, ()) = expect(expr, "missing expression")(i)?;

        i.end_node_multiple(ckpt);

        input = i;

        // If no comma, this must be the end
        if !input.eat(TokenKind::Comma) {
            break;
        }
    }
    Ok((input, ()))
}

/// Parse a record.
fn record<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::LBrace),
            fields,
            expect(token(TokenKind::RBrace), "missing `}`"),
        )),
        SyntaxKind::Record,
    )(i)
}

/// Parse a variant.
fn variant<'t, 'src>(i: Input<'t, 'src>) -> IResult<'t, 'src> {
    node(
        tuple((
            token(TokenKind::LBracePipe),
            fields,
            expect(token(TokenKind::RBracePipe), "missing `|}`"),
        )),
        SyntaxKind::Variant,
    )(i)
}
