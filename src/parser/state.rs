//! Parsing state and event type for [`nom`] event-based parsing.

use im::Vector;
use nom::InputLength;
use rowan::GreenNodeBuilder;
use std::fmt;

use crate::{
    cst::{SyntaxKind, SyntaxNode},
    lexer::{Token, TokenKind},
    parser::ParseError,
};

/// An event produced during parsing.
#[derive(Debug, Clone, PartialEq)]
enum Event {
    /// Start zero or more nodes of a given kind.
    StartNode(SyntaxKind, usize),
    /// End the innermost node.
    EndNode,
    /// Add a token to the current node.
    Token,
    /// Mark a parsing error.
    Error(String),
}

/// Parsing state for event-based parsing.
#[derive(Clone)]
pub(super) struct ParsingState<'t, 'src> {
    /// Tokens being parsed.
    tokens: &'t [Token<'src>],
    /// Next unread token.
    next: usize,
    /// Current stack of events.
    events: Vector<Event>,
    /// Whether the current input is incomplete.
    incomplete: bool,
}

impl<'t, 'src> fmt::Debug for ParsingState<'t, 'src> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.tokens[self.next..].fmt(f)
    }
}

impl<'t, 'src> InputLength for ParsingState<'t, 'src> {
    fn input_len(&self) -> usize {
        self.tokens[self.next..]
            .iter()
            .filter(|t| !t.kind.is_trivia())
            .count()
    }
}

impl<'t, 'src> nom::Offset for ParsingState<'t, 'src> {
    fn offset(&self, second: &Self) -> usize {
        second.next - self.next
    }
}

impl<'t, 'src> nom::Slice<std::ops::RangeTo<usize>> for ParsingState<'t, 'src> {
    fn slice(&self, range: std::ops::RangeTo<usize>) -> Self {
        Self {
            tokens: &self.tokens[..self.next + range.end],
            ..self.clone()
        }
    }
}

/// Constructors, consumers.
impl<'t, 'src> ParsingState<'t, 'src> {
    /// Create a new [`ParsingState`] from a slice of tokens.
    pub fn new(tokens: &'t [Token<'src>]) -> Self {
        Self {
            tokens,
            next: 0,
            events: Vector::new(),
            incomplete: false,
        }
    }

    pub fn is_complete(&self) -> bool {
        !self.incomplete
    }

    /// Convert the state into a syntax node.
    pub fn finish(self) -> (SyntaxNode, Vec<ParseError>) {
        let Self { tokens, events, .. } = self;

        let mut builder = GreenNodeBuilder::new();
        let mut tokens = tokens.into_iter().peekable();
        let mut errors = vec![];

        builder.start_node(SyntaxKind::Root.into());
        for event in events {
            match event {
                Event::StartNode(kind, n) => {
                    // Trivia and error should go outside the node
                    while tokens
                        .peek()
                        .map(|t| t.kind.is_trivia() || t.kind == TokenKind::Error)
                        .unwrap_or(false)
                    {
                        let Token { kind, src } = tokens.next().unwrap();
                        builder.token(SyntaxKind::from(*kind).into(), src);
                    }
                    for _ in 0..n {
                        builder.start_node(kind.into());
                    }
                }
                Event::EndNode => builder.finish_node(),
                Event::Token => loop {
                    let Token { kind, src } = tokens.next().unwrap();
                    builder.token(SyntaxKind::from(*kind).into(), src);
                    if !kind.is_trivia() && *kind != TokenKind::Error {
                        break;
                    }
                },
                Event::Error(err) => {
                    errors.push(err.into());
                }
            }
        }
        // Eat extra token
        for Token { kind, src } in tokens {
            builder.token(SyntaxKind::from(*kind).into(), src);
        }
        builder.finish_node();

        (SyntaxNode::new_root(builder.finish()), errors)
    }
}

/// Node-level operations.
impl<'t, 'src> ParsingState<'t, 'src> {
    /// Start a node of the given kind.
    pub fn start_node(&mut self, kind: SyntaxKind) {
        self.push_event(Event::StartNode(kind, 1));
    }

    /// Finish the innermost ongoing node.
    pub fn end_node(&mut self) {
        self.push_event(Event::EndNode);
    }

    /// Start zero or more nodes of a given kind.
    pub fn start_node_multiple(&mut self, kind: SyntaxKind) -> usize {
        self.push_event(Event::StartNode(kind, 0));
        self.events.len() - 1
    }

    /// Finish the innermost ongoing node, assuming it was started with
    /// [`Self::start_node_multiple`].
    pub fn end_node_multiple(&mut self, start: usize) {
        let event = self.events.get_mut(start).expect("event does not exist");
        match &mut *event {
            Event::StartNode(_, count) => {
                *count += 1;
                drop(event);
                self.push_event(Event::EndNode);
            }
            _ => panic!("event is not a `StartNode`"),
        }
    }

    /// Record a parsing error.
    pub fn error(&mut self, msg: String) {
        self.push_event(Event::Error(msg));
    }

    /// Record a parsing error and mark the state as incomplete.
    pub fn incomplete(&mut self, msg: String) {
        self.error(msg);
        self.incomplete = true;
    }

    /// Record an event.
    fn push_event(&mut self, event: Event) {
        self.events.push_back(event);
    }
}

/// Token-level operations.
impl<'t, 'src> ParsingState<'t, 'src> {
    /// Retrieve the next non-trivia token, if there is one.
    pub fn next(&mut self) -> Option<Token<'src>> {
        self.split_first().map(|(t, _)| t)
    }

    /// Check whether the next non-trivia token is of the given kind.
    pub fn at(&mut self, kind: TokenKind) -> bool {
        self.split_first().filter(|(t, _)| t.kind == kind).is_some()
    }

    /// Consume a non-trivia token, adding it to the current node.
    pub fn bump(&mut self) -> bool {
        if let Some((_, next)) = self.split_first() {
            self.next = next;
            self.push_event(Event::Token);
            true
        } else {
            false
        }
    }

    /// Consume and add a non-trivia token to the current node if it is of the given kind.
    ///
    /// Returns whether the token was consumed or not.
    pub fn eat(&mut self, kind: TokenKind) -> bool {
        if let Some((_, next)) = self.split_first().filter(|(t, _)| t.kind == kind) {
            self.next = next;
            self.push_event(Event::Token);
            true
        } else {
            false
        }
    }

    /// Check whether the input only has trivia tokens left.
    pub fn is_empty(&mut self) -> bool {
        self.split_first().is_none()
    }

    /// Skip trivia and errors, then return the next token and the index after
    /// it.
    fn split_first(&mut self) -> Option<(Token<'src>, usize)> {
        let mut next = self.next;
        loop {
            match self.tokens.get(next) {
                Some(token) if token.kind.is_trivia() => {
                    next += 1;
                }
                Some(token) if token.kind == TokenKind::Error => {
                    next += 1;
                    self.error(format!("bad token: `{}`", token));
                }
                Some(token) => {
                    next += 1;
                    return Some((*token, next));
                }
                None => return None,
            }
        }
    }
}
