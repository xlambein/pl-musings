use std::{
    collections::{BTreeMap, HashMap, HashSet},
    fmt,
    hash::Hash,
};

use crate::{
    hir::{Identifier, RichExpr, Span},
    typecheck::UnificationError,
    types::format::VarToLetters,
};

#[derive(Debug, Clone, PartialEq)]
pub struct TypeCheckError {
    pub kind: TypeCheckErrorKind,
    pub span: Span,
}

#[derive(Debug, Clone, PartialEq)]
pub enum TypeCheckErrorKind {
    UnificationError(UnificationError),
    UnboundIdentifier(Identifier),
    TypeMismatch(Type, Type),
    ConstraintFailed(Type, Constraint),
    RemainingConstraints,
    BadTypeAnnotation(RichExpr),
    RecordMissingFields(Type, Vec<Identifier>),
    VariantMissingFields(Type, Vec<Identifier>),
    CasePatternTypeMismatch(Type, Type),
    CaseTypeMismatch(Type, Type),
}

impl fmt::Display for TypeCheckError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use TypeCheckErrorKind::*;
        match &self.kind {
            UnificationError(err) => err.fmt(f),
            UnboundIdentifier(ident) => {
                write!(f, "unbound identifier `{ident}`")
            }
            TypeMismatch(ty0, ty1) => {
                let var_to_letters = VarToLetters::new();
                write!(
                    f,
                    "type mismatch: expected `{}`, found `{}`",
                    ty0.print_with(&var_to_letters),
                    ty1.print_with(&var_to_letters),
                )
            }
            ConstraintFailed(ty, constraint) => write!(
                f,
                "type mismatch: expected one of {{{}}}, found `{ty}`",
                constraint
                    .into_iter()
                    .map(|ty| format!("`{ty}`"))
                    .collect::<Vec<_>>()
                    .join(", "),
            ),
            RemainingConstraints => {
                write!(
                    f,
                    "an unresolved variable has a constraint, and cannot be polymorphized"
                )
            }
            BadTypeAnnotation(expr) => {
                write!(f, "`{}` is not a valid type annotation", expr)
            }
            RecordMissingFields(ty, fields) => {
                write!(
                    f,
                    "record of type `{ty}` is missing fields {{{fields}}}",
                    fields = fields.join(", ")
                )
            }
            VariantMissingFields(ty, fields) => {
                write!(
                    f,
                    "variant type `{ty}` is missing fields {{{fields}}}",
                    fields = fields.join(", ")
                )
            }
            CasePatternTypeMismatch(pattern, subject) => {
                let var_to_letters = VarToLetters::new();
                write!(
                    f,
                    "type mismatch: arm pattern has type `{}`, but case subject has type `{}`",
                    pattern.print_with(&var_to_letters),
                    subject.print_with(&var_to_letters),
                )
            }
            CaseTypeMismatch(case, arm) => {
                let var_to_letters = VarToLetters::new();
                write!(
                    f,
                    "type mismatch: inferred type `{}` for case from previous arms, but found `{}`",
                    case.print_with(&var_to_letters),
                    arm.print_with(&var_to_letters)
                )
            }
        }
    }
}

impl TypeCheckError {
    pub fn new(kind: TypeCheckErrorKind, span: Span) -> Self {
        Self { kind, span }
    }

    pub fn unbound_identifier(span: Span, ident: Identifier) -> Self {
        Self::new(TypeCheckErrorKind::UnboundIdentifier(ident), span)
    }

    pub fn type_mismatch(span: Span, ty0: Type, ty1: Type) -> Self {
        Self::new(TypeCheckErrorKind::TypeMismatch(ty0, ty1), span)
    }

    pub fn constraint_failed(span: Span, ty: Type, cst: Constraint) -> Self {
        Self::new(TypeCheckErrorKind::ConstraintFailed(ty, cst), span)
    }

    pub fn from_unification_error(err: UnificationError, span: Span) -> Self {
        let kind = match err {
            UnificationError::TypeMismatch(ty0, ty1) => {
                TypeCheckErrorKind::TypeMismatch(ty0.into(), ty1.into())
            }
            UnificationError::DomainMismatch(ty0, ty1, _) => {
                TypeCheckErrorKind::TypeMismatch(ty0.into(), ty1.into())
            }
            UnificationError::CodomainMismatch(ty0, ty1, _) => {
                TypeCheckErrorKind::TypeMismatch(ty0.into(), ty1.into())
            }
            UnificationError::ConstraintFailed(ty, constraints) => {
                TypeCheckErrorKind::ConstraintFailed(ty.into(), constraints)
            }
            // UnificationError::Cycle(_, _) => todo!(),
            // UnificationError::IncompatibleConstraints(_, _) => todo!(),
            UnificationError::RecordMissingFields(ty, _, fields) => {
                TypeCheckErrorKind::RecordMissingFields(ty.into(), fields)
            }
            UnificationError::VariantMissingFields(ty, fields) => {
                TypeCheckErrorKind::VariantMissingFields(ty.into(), fields)
            }
            err => {
                return TypeCheckError {
                    kind: TypeCheckErrorKind::UnificationError(err),
                    span: Span::None,
                }
            }
        };
        TypeCheckError { kind, span }
    }

    pub fn remaining_constraints(span: Span) -> Self {
        Self::new(TypeCheckErrorKind::RemainingConstraints, span)
    }

    pub fn bad_type_annotation(expr: RichExpr) -> Self {
        let span = expr.span;
        Self::new(TypeCheckErrorKind::BadTypeAnnotation(expr), span)
    }

    pub fn case_arm_error(err: UnificationError, span: Span) -> Self {
        let kind = match err {
            UnificationError::TypeMismatch(ty0, ty1) => {
                TypeCheckErrorKind::CasePatternTypeMismatch(ty0.into(), ty1.into())
            }
            UnificationError::RecordMissingFields(ty0, ty1, _fields) => {
                TypeCheckErrorKind::CasePatternTypeMismatch(ty0.into(), ty1.into())
            }
            _ => return Self::from_unification_error(err, span),
        };
        TypeCheckError { kind, span }
    }

    pub fn case_type_mismatch(span: Span, ty0: Type, ty1: Type) -> Self {
        Self::new(TypeCheckErrorKind::CaseTypeMismatch(ty0, ty1), span)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ConcreteType<Ty> {
    Type,
    Bool,
    Integer,
    Real,
    Fn { lhs: Ty, rhs: Ty },
    EmptyRecord,
    OpenRecord { field: Identifier, ty: Ty, rest: Ty },
    EmptyVariant,
    OpenVariant { field: Identifier, ty: Ty, rest: Ty },
}

impl<T> ConcreteType<T> {
    pub fn try_map<U, E, F>(self, mut f: F) -> Result<ConcreteType<U>, E>
    where
        F: FnMut(T) -> Result<U, E>,
    {
        Ok(match self {
            ConcreteType::Type => ConcreteType::Type,
            ConcreteType::Bool => ConcreteType::Bool,
            ConcreteType::Integer => ConcreteType::Integer,
            ConcreteType::Real => ConcreteType::Real,
            ConcreteType::Fn { lhs, rhs } => ConcreteType::Fn {
                lhs: f(lhs)?,
                rhs: f(rhs)?,
            },
            ConcreteType::EmptyRecord => ConcreteType::EmptyRecord,
            ConcreteType::OpenRecord { field, ty, rest } => ConcreteType::OpenRecord {
                field,
                ty: f(ty)?,
                rest: f(rest)?,
            },
            ConcreteType::EmptyVariant => ConcreteType::EmptyVariant,
            ConcreteType::OpenVariant { field, ty, rest } => ConcreteType::OpenVariant {
                field,
                ty: f(ty)?,
                rest: f(rest)?,
            },
        })
    }

    pub fn map<U, F>(self, mut f: F) -> ConcreteType<U>
    where
        F: FnMut(T) -> U,
    {
        self.try_map::<_, std::convert::Infallible, _>(|t| Ok(f(t)))
            .unwrap()
    }
}

impl fmt::Display for ConcreteType<()> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ConcreteType::Type => write!(f, "Type"),
            ConcreteType::Bool => write!(f, "Bool"),
            ConcreteType::Integer => write!(f, "Integer"),
            ConcreteType::Real => write!(f, "Real"),

            ConcreteType::Fn { .. } => panic!("trying to display an Fn constraint type"),
            ConcreteType::EmptyRecord => panic!("trying to display an EmptyRecord constraint type"),
            ConcreteType::OpenRecord { .. } => {
                panic!("trying to display an OpenRecord constraint type")
            }
            ConcreteType::EmptyVariant { .. } => {
                panic!("trying to display an EmptyVariant constraint type")
            }
            ConcreteType::OpenVariant { .. } => {
                panic!("trying to display an OpenVariant constraint type")
            }
        }
    }
}

pub trait AsConcreteType: Sized {
    type Ty: AsRef<Self>;

    fn as_concrete_type(&self) -> Option<&ConcreteType<Self::Ty>>;

    fn dfs(&self) -> TypeDepthFirst<'_, Self> {
        TypeDepthFirst { stack: vec![self] }
    }
}

pub struct TypeDepthFirst<'a, Ty>
where
    Ty: AsConcreteType,
{
    stack: Vec<&'a Ty>,
}

impl<'a, Ty> Iterator for TypeDepthFirst<'a, Ty>
where
    Ty: AsConcreteType,
{
    type Item = &'a Ty;

    fn next(&mut self) -> Option<Self::Item> {
        let ty = self.stack.pop()?;
        if let Some(ty) = ty.as_concrete_type() {
            match ty {
                ConcreteType::Type
                | ConcreteType::Bool
                | ConcreteType::Integer
                | ConcreteType::Real
                | ConcreteType::EmptyRecord
                | ConcreteType::EmptyVariant => {}

                ConcreteType::Fn { lhs, rhs } => {
                    self.stack.push(rhs.as_ref());
                    self.stack.push(lhs.as_ref());
                }
                ConcreteType::OpenRecord { ty, rest, .. } => {
                    self.stack.push(ty.as_ref());
                    self.stack.push(rest.as_ref());
                }
                ConcreteType::OpenVariant { ty, rest, .. } => {
                    self.stack.push(ty.as_ref());
                    self.stack.push(rest.as_ref());
                }
            }
        }
        Some(ty)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum MonoType<V> {
    Concrete(ConcreteType<Box<MonoType<V>>>),
    Var(V),
    Recursive(V, Box<MonoType<V>>),
}

impl<V: Eq + Hash> MonoType<V> {
    pub fn free_vars(&self) -> impl Iterator<Item = &V> {
        let mut bound = HashSet::new();
        self.dfs()
            .flat_map(|ty| match ty {
                MonoType::Concrete(_) => None,
                MonoType::Var(v) => {
                    if bound.contains(v) {
                        None
                    } else {
                        Some(v)
                    }
                }
                MonoType::Recursive(v, _) => {
                    bound.insert(v);
                    None
                }
            })
            .collect::<HashSet<_>>()
            .into_iter()
    }
}

impl<V> AsConcreteType for MonoType<V> {
    type Ty = Box<MonoType<V>>;

    fn as_concrete_type(&self) -> Option<&ConcreteType<Self::Ty>> {
        match self {
            MonoType::Concrete(ty) => Some(ty),
            MonoType::Var(_) => None,
            MonoType::Recursive(_, inner) => inner.as_concrete_type(),
        }
    }
}

impl<V> MonoType<V> {
    pub fn fields(&self) -> BTreeMap<&Identifier, &Self> {
        let mut tail = self;
        let mut acc = BTreeMap::new();

        loop {
            match tail {
                MonoType::Concrete(
                    ConcreteType::OpenRecord { field, ty, rest }
                    | ConcreteType::OpenVariant { field, ty, rest },
                ) => {
                    acc.insert(field, ty.as_ref());
                    tail = rest;
                }

                MonoType::Recursive(..) => break,

                MonoType::Concrete(ConcreteType::EmptyRecord | ConcreteType::EmptyVariant)
                | MonoType::Var(_) => break,

                MonoType::Concrete(_) => panic!("fields are malformed"),
            }
        }

        acc
    }

    pub fn fields_rest(&self) -> &Self {
        let mut tail = self;
        loop {
            match tail {
                MonoType::Concrete(
                    ConcreteType::OpenRecord { rest, .. } | ConcreteType::OpenVariant { rest, .. },
                ) => {
                    tail = rest;
                }

                MonoType::Recursive(..) => break,

                MonoType::Concrete(ConcreteType::EmptyRecord | ConcreteType::EmptyVariant)
                | MonoType::Var(_) => break,

                MonoType::Concrete(_) => panic!("fields are malformed"),
            }
        }

        tail
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Var(usize);

impl From<usize> for Var {
    fn from(idx: usize) -> Self {
        Var(idx)
    }
}

impl Into<usize> for Var {
    fn into(self) -> usize {
        self.0
    }
}

pub type Type = MonoType<Var>;

pub type Constraint = HashSet<ConcreteType<()>>;

mod format;

impl Type {
    pub fn normalize(self) -> Self {
        self.normalize_(&mut HashMap::new())
    }

    fn normalize_(self, subs: &mut HashMap<Var, Var>) -> Self {
        match self {
            MonoType::Concrete(cty) => {
                MonoType::Concrete(cty.map(|ty| Box::new(ty.normalize_(subs))))
            }
            MonoType::Var(v) => {
                let next = subs.len();
                MonoType::Var(subs.entry(v).or_insert_with(|| Var(next)).clone())
            }
            MonoType::Recursive(v, inner) => {
                let next = subs.len();
                let u = Var(next);
                subs.insert(v, u.clone());
                MonoType::Recursive(u, Box::new(inner.normalize_(subs)))
            }
        }
    }
}
