use std::{
    cell::{RefCell, RefMut},
    collections::HashMap,
    fmt::{self, Write},
    hash::Hash,
};

use super::{ConcreteType, MonoType};

pub fn index_to_letters(mut idx: usize) -> String {
    if idx == 0 {
        return "a".into();
    }
    let mut res = vec![];
    while idx != 0 {
        res.push(('a' as u8 + (idx % 26) as u8) as char);
        idx = idx / 26;
    }
    res.iter().rev().collect()
}

pub struct VarToLetters<'a, V> {
    var_to_letters: RefCell<HashMap<&'a V, String>>,
}

impl<'a, V: Eq + Hash> VarToLetters<'a, V> {
    pub fn new() -> Self {
        Self {
            var_to_letters: RefCell::new(HashMap::new()),
        }
    }

    fn get(&self, var: &'a V) -> RefMut<str> {
        RefMut::map(self.var_to_letters.borrow_mut(), |m| {
            let next_index = m.len();
            m.entry(var)
                .or_insert_with(|| {
                    let s = index_to_letters(next_index);
                    s
                })
                .as_mut_str()
        })
    }
}

pub struct TypePrettyPrinter<'ty, 'vtl, V> {
    ty: &'ty MonoType<V>,
    var_to_letters: &'vtl VarToLetters<'ty, V>,
    indent: u8,
}

impl<'ty, 'vtl, V> TypePrettyPrinter<'ty, 'vtl, V> {
    fn new(ty: &'ty MonoType<V>, var_to_letters: &'vtl VarToLetters<'ty, V>) -> Self {
        Self {
            ty,
            var_to_letters,
            indent: 0,
        }
    }

    fn indent(&self) -> Self {
        Self {
            indent: self.indent + 2,
            ..*self
        }
    }

    fn with_type(&self, ty: &'ty MonoType<V>) -> Self {
        Self { ty, ..*self }
    }

    fn newline(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if f.alternate() {
            f.write_char('\n')?;
            for _ in 0..self.indent {
                f.write_char(' ')?;
            }
        }
        Ok(())
    }
}

impl<'ty, 'vtl, V: Eq + Hash> fmt::Display for TypePrettyPrinter<'ty, 'vtl, V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.ty {
            MonoType::Concrete(ty) => match ty {
                ConcreteType::Type => write!(f, "Type"),
                ConcreteType::Bool => write!(f, "Bool"),
                ConcreteType::Integer => write!(f, "Integer"),
                ConcreteType::Real => write!(f, "Real"),
                ConcreteType::Fn { lhs, rhs } => {
                    let paren = match lhs.as_ref() {
                        MonoType::Concrete(ConcreteType::Fn { .. }) => true,
                        _ => false,
                    };
                    if paren {
                        f.write_char('(')?;
                    }
                    self.with_type(lhs).fmt(f)?;
                    if paren {
                        f.write_char(')')?;
                    }
                    f.write_str(" -> ")?;
                    self.with_type(rhs).fmt(f)
                }
                ConcreteType::EmptyRecord => write!(f, "{{}}"),
                ConcreteType::OpenRecord { .. } => {
                    let fields = self.ty.fields();
                    let rest = self.ty.fields_rest();

                    f.write_char('{')?;
                    let indented = self.indent();
                    for (i, (name, ty)) in fields.iter().enumerate() {
                        indented.newline(f)?;
                        f.write_str(name)?;
                        f.write_str(" : ")?;
                        indented.with_type(ty).fmt(f)?;
                        if i < fields.len() - 1 {
                            f.write_str(", ")?;
                        }
                    }

                    self.newline(f)?;
                    if !matches!(rest, MonoType::Concrete(ConcreteType::EmptyRecord)) {
                        if !f.alternate() {
                            f.write_char(' ')?;
                        }
                        f.write_str("| ")?;
                        self.with_type(rest).fmt(f)?;
                    }

                    f.write_char('}')
                }
                ConcreteType::EmptyVariant => write!(f, "{{||}}"),
                ConcreteType::OpenVariant { .. } => {
                    let fields = self.ty.fields();
                    let rest = self.ty.fields_rest();

                    f.write_str("{|")?;
                    let indented = self.indent();
                    for (i, (name, ty)) in fields.iter().enumerate() {
                        indented.newline(f)?;
                        f.write_str(name)?;
                        f.write_str(" : ")?;
                        indented.with_type(ty).fmt(f)?;
                        if i < fields.len() - 1 {
                            f.write_str(", ")?;
                        }
                    }

                    self.newline(f)?;
                    if !matches!(rest, MonoType::Concrete(ConcreteType::EmptyVariant)) {
                        if !f.alternate() {
                            f.write_char(' ')?;
                        }
                        f.write_str("| ")?;
                        self.with_type(rest).fmt(f)?;
                    }

                    f.write_str("|}")
                }
            },
            MonoType::Var(v) => {
                write!(f, "{}", self.var_to_letters.get(v))
            }
            MonoType::Recursive(v, inner) => {
                // We have to split them to avoid double borrow
                write!(f, "rec {}. ", self.var_to_letters.get(v))?;
                self.with_type(inner).fmt(f)
            }
        }
    }
}

impl<'ty, 'vtl, V: Eq + Hash> MonoType<V> {
    pub fn print_with(
        &'ty self,
        var_to_letters: &'vtl VarToLetters<'ty, V>,
    ) -> TypePrettyPrinter<'ty, 'vtl, V> {
        TypePrettyPrinter::new(self, var_to_letters)
    }
}

impl<V: Eq + Hash> fmt::Display for MonoType<V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        TypePrettyPrinter::new(self, &VarToLetters::new()).fmt(f)
    }
}
