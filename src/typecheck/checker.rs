use std::collections::{HashMap, HashSet};

use crate::{
    hir::{self, InfixOperator, RichExpr, Span},
    types::{ConcreteType, Type, TypeCheckError},
};

use super::{table::UnificationTable, PolyType, UnifType};

pub fn type_check(
    env: &HashMap<hir::Identifier, Type>,
    expr: &hir::RichExpr,
) -> Result<Type, TypeCheckError> {
    let mut tc = TypeChecker::new();
    let env = env
        .clone()
        .into_iter()
        .map(|(ident, ty)| (ident, PolyType::from(ty)))
        .collect();
    let ty = tc.expr(&env, expr)?;

    // This checks for a (temporary) unsoundness in the conversion of `UnifType`
    // to `Type`: if the source type contains an unresolved variable with a
    // constraint, that constraint is lost during conversion.  This means a
    // function like `\x. x + x` could be polymorphized, and then instantiated
    // for e.g. `x : Bool`, which is not type-correct.
    if ty
        .free_vars()
        .any(|var| tc.table.constraints_on(*var).is_some())
    {
        return Err(TypeCheckError::remaining_constraints(expr.span));
    }

    let ty = ty.into();
    Ok(ty)
}

type Env = HashMap<hir::Identifier, PolyType>;

struct TypeChecker {
    table: UnificationTable,
}

impl TypeChecker {
    fn new() -> Self {
        Self {
            table: UnificationTable::new(),
        }
    }

    fn expr(&mut self, env: &Env, expr: &hir::RichExpr) -> Result<UnifType, TypeCheckError> {
        use hir::Expr::*;
        let ty = match expr.expr() {
            Literal(it) => self.literal(env, it),
            Identifier(it) => self.identifier(env, it, expr.span),
            InfixOp { op, lhs, rhs } => self.infix_op(env, op, lhs, rhs),
            PrefixOp { op, inner } => self.prefix_op(env, op, inner),
            Application { lhs, rhs } => self.application(env, lhs, rhs),
            Let {
                rec,
                bindings,
                body,
            } => self.r#let(env, *rec, bindings, body),
            Lambda { identifier, body } => self.lambda(env, identifier, body),
            Conditional {
                condition,
                then,
                r#else,
            } => self.conditional(env, condition, then, r#else),
            EmptyRecord => self.record(env, &HashMap::new()),
            Record(fields) => self.record(env, fields.into_iter()),
            RecordType(fields) => self.record_type(env, fields.into_iter()),
            RecordAccess { record, field } => self.record_access(env, record, field),
            EmptyVariant => self.empty_variant(env),
            Variant { field, value } => self.variant(env, field, value),
            VariantType(fields) => self.variant_type(env, fields.into_iter()),
            Case { subject, arms } => self.case(env, subject, arms),

            LiteralType(ty) => self.literal_type(env, ty),
            Rec { variable, inner } => self.rec(env, variable, inner),

            Missing => self.missing(env),
        }?;
        Ok(self.table.find_deep(ty))
    }

    fn literal(&self, _env: &Env, literal: &hir::Literal) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Concrete(match literal {
            hir::Literal::Bool(_) => ConcreteType::Bool,
            hir::Literal::Integer(_) => ConcreteType::Integer,
            hir::Literal::Real(_) => ConcreteType::Real,
        }))
    }

    fn identifier(
        &mut self,
        env: &Env,
        ident: &hir::Identifier,
        span: Span,
    ) -> Result<UnifType, TypeCheckError> {
        if let Some(ty) = env.get(ident) {
            Ok(ty.clone().instantiate(&mut self.table))
        } else {
            Err(TypeCheckError::unbound_identifier(span, ident.clone()))
        }
    }

    fn infix_op(
        &mut self,
        env: &Env,
        op: &hir::InfixOperator,
        lhs: &RichExpr,
        rhs: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        use hir::InfixOperator::*;

        if let HasType = op {
            let ty0 = self.expr(env, lhs)?;
            let ty1 = self.eval_type(&HashMap::new(), rhs)?;
            self.table
                .unify(ty0.clone(), ty1)
                .map_err(|err| TypeCheckError::from_unification_error(err, lhs.span))?;

            return Ok(ty0);
        }

        let ty0 = self.expr(env, lhs)?;
        let ty1 = self.expr(env, rhs)?;

        if let FunArrow = op {
            let t = UnifType::Concrete(ConcreteType::Type);
            return Ok(t);
        }

        let (target, result) = match op {
            Add | Sub | Mul | Div => {
                let t = UnifType::Var(self.table.new_type_var_with_constraints(HashSet::from([
                    ConcreteType::Integer,
                    ConcreteType::Real,
                ])));
                (t.clone(), t)
            }
            Gt | Lt | Gte | Lte => {
                let t = UnifType::Var(self.table.new_type_var_with_constraints(HashSet::from([
                    ConcreteType::Integer,
                    ConcreteType::Real,
                ])));
                (t, UnifType::Concrete(ConcreteType::Bool))
            }
            And | Or => (
                UnifType::Concrete(ConcreteType::Bool),
                UnifType::Concrete(ConcreteType::Bool),
            ),
            Eq | Neq => {
                let t = UnifType::Var(self.table.new_type_var_with_constraints(HashSet::from([
                    ConcreteType::Bool,
                    ConcreteType::Integer,
                    ConcreteType::Real,
                ])));
                (t, UnifType::Concrete(ConcreteType::Bool))
            }

            // We handled those above
            HasType | FunArrow => unreachable!(),
        };

        self.table
            .unify(target.clone(), ty0)
            .map_err(|err| TypeCheckError::from_unification_error(err, lhs.span))?;
        self.table
            .unify(target.clone(), ty1)
            .map_err(|err| TypeCheckError::from_unification_error(err, rhs.span))?;

        Ok(result)
    }

    fn prefix_op(
        &mut self,
        env: &Env,
        op: &hir::PrefixOperator,
        inner: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let ty = self.expr(env, inner)?;
        match op {
            hir::PrefixOperator::Negative => {
                let t = UnifType::Var(self.table.new_type_var_with_constraints(HashSet::from([
                    ConcreteType::Integer,
                    ConcreteType::Real,
                ])));
                self.table
                    .unify(t.clone(), ty)
                    .map_err(|err| TypeCheckError::from_unification_error(err, inner.span))?;
                Ok(t)
            }
        }
    }

    fn application(
        &mut self,
        env: &Env,
        lhs: &RichExpr,
        rhs: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let ty0 = self.expr(env, lhs)?;
        let ty1 = self.expr(env, rhs)?;
        let t = UnifType::Var(self.table.new_type_var());
        self.table
            .unify(
                ty0,
                UnifType::Concrete(ConcreteType::Fn {
                    lhs: Box::new(ty1),
                    rhs: Box::new(t.clone()),
                }),
            )
            .map_err(|err| TypeCheckError::from_unification_error(err, rhs.span))?;
        Ok(t)
    }

    fn lambda(
        &mut self,
        env: &Env,
        ident: &hir::Identifier,
        body: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let lhs = UnifType::Var(self.table.new_type_var());
        let mut env = env.clone();
        env.insert(ident.clone(), PolyType::Mono(lhs.clone()));
        let rhs = self.expr(&env, body)?;
        Ok(UnifType::Concrete(ConcreteType::Fn {
            lhs: Box::new(lhs),
            rhs: Box::new(rhs),
        }))
    }

    fn r#let(
        &mut self,
        env: &Env,
        rec: bool,
        bindings: &Vec<hir::LetBinding<RichExpr>>,
        body: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let mut env = env.clone();
        if rec {
            // FIXME we should compute the graph of dependencies in order to
            //   know how we can group definitions.
            let mut vars = vec![];
            for hir::LetBinding { identifier, .. } in bindings {
                let t = UnifType::Var(self.table.new_type_var());
                env.insert(identifier.clone(), PolyType::Mono(t.clone()));
                vars.push(t);
            }

            for (hir::LetBinding { body, .. }, t) in bindings.iter().zip(vars.iter()) {
                let ty0 = self.expr(&env, body)?;
                self.table
                    .unify(ty0.clone(), t.clone())
                    .map_err(|err| TypeCheckError::from_unification_error(err, body.span))?;
            }

            for (
                hir::LetBinding {
                    identifier,
                    type_annotation,
                    ..
                },
                ty0,
            ) in bindings.iter().zip(vars.into_iter())
            {
                let ty0 = self.table.find(ty0);
                if let Some(annot) = type_annotation {
                    let (annot, span) = self.eval_type_annotation(&HashMap::new(), annot)?;
                    let t = annot.clone().instantiate(&mut self.table);
                    self.table
                        .unify(t.clone(), ty0)
                        .map_err(|err| TypeCheckError::from_unification_error(err, span))?;

                    let t0: Type = t.clone().into();
                    let t0 = t0.normalize();
                    let t1: Type = self.table.find_deep(t).into();
                    let t1 = t1.normalize();
                    if t0 != t1 {
                        // FIXME the error is set on the annotation, but that's
                        //   not very helpful.
                        return Err(TypeCheckError::type_mismatch(span, t0, t1));
                    }

                    env.insert(identifier.clone(), annot);
                } else {
                    env.insert(identifier.clone(), PolyType::Mono(ty0));
                }
            }

            self.expr(&env, body)
        } else {
            for hir::LetBinding {
                identifier,
                type_annotation,
                body,
                ..
            } in bindings
            {
                let ty0 = self.expr(&env, body)?;
                if let Some(annot) = type_annotation {
                    let (annot, span) = self.eval_type_annotation(&HashMap::new(), annot)?;
                    let t = annot.clone().instantiate(&mut self.table);
                    self.table
                        .unify(t.clone(), ty0)
                        .map_err(|err| TypeCheckError::from_unification_error(err, span))?;

                    let t0: Type = t.clone().into();
                    let t0 = t0.normalize();
                    let t1: Type = self.table.find_deep(t).into();
                    let t1 = t1.normalize();
                    if t0 != t1 {
                        // FIXME the error is set on the annotation, but that's
                        //   not very helpful.
                        return Err(TypeCheckError::type_mismatch(span, t0, t1));
                    }

                    env.insert(identifier.clone(), annot);
                } else {
                    env.insert(identifier.clone(), PolyType::Mono(ty0));
                }
            }

            self.expr(&env, body)
        }
    }

    fn let_generalize(
        &mut self,
        env: &Env,
        ident: &hir::Identifier,
        value: &RichExpr,
        body: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let ty0 = self.expr(env, value)?;
        // Check for constraints before polymorphizing
        if ty0
            .free_vars()
            .any(|var| self.table.constraints_on(*var).is_some())
        {
            return Err(TypeCheckError::remaining_constraints(value.span));
        }
        let ty0 = ty0.generalize();
        let mut env = env.clone();
        env.insert(ident.clone(), ty0);
        self.expr(&env, body)
    }

    fn conditional(
        &mut self,
        env: &Env,
        condition: &RichExpr,
        then: &RichExpr,
        r#else: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let ty0 = self.expr(env, condition)?;
        let ty1 = self.expr(env, then)?;
        let ty2 = self.expr(env, r#else)?;
        self.table
            .unify(UnifType::Concrete(ConcreteType::Bool), ty0)
            .map_err(|err| TypeCheckError::from_unification_error(err, condition.span))?;
        self.table
            .unify(ty1.clone(), ty2)
            .map_err(|err| TypeCheckError::from_unification_error(err, r#else.span))?;
        Ok(ty1)
    }

    fn record<'a>(
        &mut self,
        env: &Env,
        fields: impl IntoIterator<Item = (&'a hir::Identifier, &'a RichExpr)>,
    ) -> Result<UnifType, TypeCheckError> {
        let mut ty = UnifType::Concrete(ConcreteType::EmptyRecord);

        for (field, expr) in fields {
            ty = UnifType::Concrete(ConcreteType::OpenRecord {
                field: field.clone(),
                ty: Box::new(self.expr(env, expr)?),
                rest: Box::new(ty),
            });
        }

        Ok(ty)
    }

    fn record_type<'a>(
        &self,
        _env: &Env,
        _fields: impl IntoIterator<Item = (&'a hir::Identifier, &'a RichExpr)>,
    ) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Concrete(ConcreteType::Type))
    }

    fn record_access<'a>(
        &mut self,
        env: &Env,
        record: &RichExpr,
        field: &hir::Identifier,
    ) -> Result<UnifType, TypeCheckError> {
        let ty = self.expr(env, record)?;
        let field_ty = UnifType::Var(self.table.new_type_var());
        let record_ty = UnifType::Concrete(ConcreteType::OpenRecord {
            field: field.clone(),
            ty: Box::new(field_ty.clone()),
            rest: Box::new(UnifType::Var(self.table.new_type_var())),
        });

        self.table
            .unify(record_ty, ty)
            .map_err(|err| TypeCheckError::from_unification_error(err, record.span))?;

        Ok(field_ty)
    }

    fn empty_variant(&self, _env: &Env) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Concrete(ConcreteType::EmptyVariant))
    }

    fn variant(
        &mut self,
        env: &Env,
        field: &hir::Identifier,
        value: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        let rest = UnifType::Var(self.table.new_type_var());
        Ok(UnifType::Concrete(ConcreteType::OpenVariant {
            field: field.clone(),
            ty: Box::new(self.expr(env, value)?),
            rest: Box::new(rest),
        }))
    }

    fn variant_type<'a>(
        &self,
        _env: &Env,
        _fields: impl IntoIterator<Item = (&'a hir::Identifier, &'a RichExpr)>,
    ) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Concrete(ConcreteType::Type))
    }

    fn case(
        &mut self,
        env: &Env,
        subject: &RichExpr,
        arms: &[hir::CaseArm<RichExpr>],
    ) -> Result<UnifType, TypeCheckError> {
        let subject_ty = self.expr(env, subject)?;
        let ty = UnifType::Var(self.table.new_type_var());
        for hir::CaseArm { pattern, body } in arms {
            // Unify the pattern and the subject, and add bindings to the environment
            let (pattern_ty, bindings) = self.pattern(pattern)?;
            self.table
                .unify(pattern_ty, subject_ty.clone())
                .map_err(|err| TypeCheckError::case_arm_error(err, pattern.span))?;
            let mut env = env.clone();
            for (ident, t) in bindings {
                env.insert(ident, PolyType::Mono(t));
            }
            // Check the arm's body in this environment
            let body_ty = self.expr(&env, body)?;
            // Unify the body's type with the case's type
            self.table.unify(ty.clone(), body_ty.clone()).map_err(|_| {
                TypeCheckError::case_type_mismatch(
                    body.span,
                    self.table.find(ty.clone()).into(),
                    body_ty.into(),
                )
            })?;
        }
        Ok(ty)
    }

    fn literal_type(&self, _env: &Env, _ty: &hir::LiteralType) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Concrete(ConcreteType::Type))
    }

    fn rec(
        &self,
        _env: &Env,
        _variable: &hir::Identifier,
        _inner: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Concrete(ConcreteType::Type))
    }

    fn missing(&mut self, _env: &Env) -> Result<UnifType, TypeCheckError> {
        Ok(UnifType::Var(self.table.new_type_var()))
    }

    fn eval_type_annotation(
        &mut self,
        env: &HashMap<hir::Identifier, UnifType>,
        annotation: &hir::TypeAnnotation<RichExpr>,
    ) -> Result<(PolyType, Span), TypeCheckError> {
        match annotation {
            hir::TypeAnnotation::ForAll { variable, inner } => {
                let v = self.table.new_type_var();
                let mut env = env.clone();
                env.insert(variable.clone(), UnifType::Var(v.clone()));
                let (inner, span) = self.eval_type_annotation(&env, inner)?;
                Ok((PolyType::ForAll(v, Box::new(inner)), span))
            }
            hir::TypeAnnotation::Expr(expr) => {
                Ok((PolyType::Mono(self.eval_type(env, expr)?), expr.span))
            }
        }
    }

    fn eval_type(
        &mut self,
        env: &HashMap<hir::Identifier, UnifType>,
        expr: &RichExpr,
    ) -> Result<UnifType, TypeCheckError> {
        match expr.expr() {
            hir::Expr::Identifier(ident) => env
                .get(ident)
                .cloned()
                .ok_or_else(|| TypeCheckError::bad_type_annotation(expr.clone())),

            hir::Expr::LiteralType(ty) => Ok(UnifType::Concrete(match ty {
                hir::LiteralType::Bool => ConcreteType::Bool,
                hir::LiteralType::Integer => ConcreteType::Integer,
                hir::LiteralType::Real => ConcreteType::Real,
            })),

            hir::Expr::InfixOp { lhs, rhs, op } if *op == InfixOperator::FunArrow => {
                let ty0 = self.eval_type(env, lhs)?;
                let ty1 = self.eval_type(env, rhs)?;
                Ok(UnifType::Concrete(ConcreteType::Fn {
                    lhs: Box::new(ty0),
                    rhs: Box::new(ty1),
                }))
            }

            hir::Expr::EmptyRecord => Ok(UnifType::Concrete(ConcreteType::EmptyRecord)),

            hir::Expr::RecordType(fields) => {
                let mut ty = UnifType::Concrete(ConcreteType::EmptyRecord);

                for (field, expr) in fields {
                    ty = UnifType::Concrete(ConcreteType::OpenRecord {
                        field: field.clone(),
                        ty: Box::new(self.eval_type(env, expr)?),
                        rest: Box::new(ty),
                    });
                }

                Ok(ty)
            }

            hir::Expr::EmptyVariant => Ok(UnifType::Concrete(ConcreteType::EmptyVariant)),

            hir::Expr::VariantType(fields) => {
                let mut ty = UnifType::Concrete(ConcreteType::EmptyVariant);

                for (field, expr) in fields {
                    ty = UnifType::Concrete(ConcreteType::OpenVariant {
                        field: field.clone(),
                        ty: Box::new(self.eval_type(env, expr)?),
                        rest: Box::new(ty),
                    });
                }

                Ok(ty)
            }

            hir::Expr::Rec { variable, inner } => {
                let var = self.table.new_type_var();

                let mut env = env.clone();
                env.insert(variable.clone(), UnifType::Var(var));

                let inner = self.eval_type(&env, inner)?;

                Ok(UnifType::Recursive(var, Box::new(inner)))
            }

            _ => Err(TypeCheckError::bad_type_annotation(expr.clone())),
        }
    }

    fn pattern(
        &mut self,
        pattern: &RichExpr,
    ) -> Result<(UnifType, Vec<(hir::Identifier, UnifType)>), TypeCheckError> {
        use hir::{Expr, Literal};
        match &*pattern.expr {
            Expr::Literal(Literal::Bool(_)) => Ok((UnifType::Concrete(ConcreteType::Bool), vec![])),
            Expr::Literal(Literal::Integer(_)) => {
                Ok((UnifType::Concrete(ConcreteType::Integer), vec![]))
            }
            Expr::Literal(Literal::Real(_)) => Ok((UnifType::Concrete(ConcreteType::Real), vec![])),
            Expr::Identifier(ident) => {
                let ty = UnifType::Var(self.table.new_type_var());
                Ok((ty.clone(), vec![(ident.clone(), ty)]))
            }
            Expr::EmptyRecord => Ok((UnifType::Concrete(ConcreteType::EmptyRecord), vec![])),
            Expr::Record(fields) => {
                // TODO match partial records
                let mut bindings = vec![];
                let mut ty = UnifType::Concrete(ConcreteType::EmptyRecord);
                for (field, sub_pattern) in fields {
                    let (field_ty, b) = self.pattern(sub_pattern)?;
                    ty = UnifType::Concrete(ConcreteType::OpenRecord {
                        field: field.clone(),
                        ty: Box::new(field_ty),
                        rest: Box::new(ty),
                    });
                    bindings.extend(b);
                }

                Ok((ty, bindings))
            }
            Expr::EmptyVariant => Ok((UnifType::Concrete(ConcreteType::EmptyVariant), vec![])),
            Expr::Variant {
                field,
                value: sub_pattern,
            } => {
                let (field_ty, bindings) = self.pattern(sub_pattern)?;
                let rest = UnifType::Var(self.table.new_type_var());
                let ty = UnifType::Concrete(ConcreteType::OpenVariant {
                    field: field.clone(),
                    ty: Box::new(field_ty),
                    rest: Box::new(rest),
                });

                Ok((ty, bindings))
            }

            Expr::InfixOp { .. }
            | Expr::PrefixOp { .. }
            | Expr::Application { .. }
            | Expr::Let { .. }
            | Expr::Lambda { .. }
            | Expr::Conditional { .. }
            | Expr::Missing
            | Expr::RecordType(_)
            | Expr::RecordAccess { .. }
            | Expr::VariantType(_)
            | Expr::Case { .. }
            | Expr::LiteralType(_)
            | Expr::Rec { .. } => panic!("invalid pattern"),
        }
    }
}
