mod checker;
mod table;

use std::{
    collections::{HashMap, HashSet},
    fmt,
};

use crate::{
    hir,
    types::{Constraint, MonoType, Type, Var},
};

use table::UnificationTable;

pub use checker::type_check;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UnifVar(usize);

type UnifType = MonoType<UnifVar>;

impl UnifType {
    fn substitute(self, subs: &HashMap<UnifVar, UnifVar>) -> UnifType {
        match self {
            UnifType::Concrete(ty) => {
                UnifType::Concrete(ty.map(|ty| Box::new(ty.substitute(subs))))
            }
            UnifType::Var(v) => UnifType::Var(subs.get(&v).cloned().unwrap_or(v)),
            UnifType::Recursive(v, inner) => {
                let v = subs.get(&v).cloned().unwrap_or(v);
                UnifType::Recursive(v, Box::new(inner.substitute(subs)))
            }
        }
    }

    pub fn generalize(self) -> PolyType {
        let free_vars: HashSet<_> = self.free_vars().copied().collect();

        let mut quant = PolyType::Mono(self);
        for var in free_vars {
            quant = PolyType::ForAll(var, Box::new(quant));
        }
        quant
    }

    fn instantiate(
        self,
        ts: &mut UnificationTable,
        subs: &mut HashMap<UnifVar, UnifVar>,
    ) -> UnifType {
        match self {
            UnifType::Concrete(cty) => {
                MonoType::Concrete(cty.map(|ty| Box::new(ty.instantiate(ts, subs))))
            }
            UnifType::Var(v) => UnifType::Var(subs.get(&v).cloned().unwrap_or(v)),
            UnifType::Recursive(v, inner) => {
                let u = ts.new_type_var();
                let w = subs.insert(v, u);
                let inner = inner.instantiate(ts, subs);
                if let Some(w) = w {
                    subs.insert(v, w);
                } else {
                    subs.remove(&v);
                }
                ts.unify(UnifType::Var(u), inner).unwrap();
                ts.find(UnifType::Var(u))
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PolyType {
    Mono(UnifType),
    ForAll(UnifVar, Box<PolyType>),
}

impl PolyType {
    pub fn instantiate(self, ts: &mut UnificationTable) -> UnifType {
        fn instantiate_(
            this: PolyType,
            ts: &mut UnificationTable,
            mut subs: HashMap<UnifVar, UnifVar>,
        ) -> UnifType {
            match this {
                PolyType::Mono(ty) => ty.instantiate(ts, &mut subs),
                PolyType::ForAll(var, ty) => {
                    subs.insert(var, ts.new_type_var());
                    instantiate_(*ty, ts, subs)
                }
            }
        }

        instantiate_(self, ts, HashMap::new())
    }
}

impl From<Type> for PolyType {
    fn from(ty: Type) -> Self {
        fn convert(ty: Type) -> UnifType {
            match ty {
                Type::Concrete(ty) => UnifType::Concrete(ty.map(|ty| Box::new(convert(*ty)))),
                Type::Var(v) => UnifType::Var(UnifVar(v.into())),
                Type::Recursive(v, inner) => {
                    UnifType::Recursive(UnifVar(v.into()), Box::new(convert(*inner)))
                }
            }
        }

        let mono = convert(ty);
        let vars: Vec<_> = mono.free_vars().copied().collect();

        let mut ty = PolyType::Mono(mono);
        for var in vars {
            ty = PolyType::ForAll(var, Box::new(ty));
        }
        ty
    }
}

impl Into<Type> for UnifType {
    fn into(self) -> Type {
        match self {
            UnifType::Concrete(ty) => Type::Concrete(ty.map(|ty| Box::new(Into::into(*ty)))),
            UnifType::Var(v) => Type::Var(Var::from(v.0)),
            UnifType::Recursive(v, inner) => {
                Type::Recursive(Var::from(v.0), Box::new((*inner).into()))
            }
        }
    }
}

impl Into<Type> for PolyType {
    fn into(self) -> Type {
        match self {
            PolyType::Mono(ty) => ty.into(),
            PolyType::ForAll(_, ty) => (*ty).into(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum UnificationError {
    Cycle(UnifVar, UnifType),
    TypeMismatch(UnifType, UnifType),
    IncompatibleConstraints(Constraint, Constraint),
    ConstraintFailed(UnifType, Constraint),
    DomainMismatch(UnifType, UnifType, Box<UnificationError>),
    CodomainMismatch(UnifType, UnifType, Box<UnificationError>),
    RecordMissingFields(UnifType, UnifType, Vec<hir::Identifier>),
    VariantMissingFields(UnifType, Vec<hir::Identifier>),
}

impl fmt::Display for UnificationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use UnificationError::*;
        match self {
            Cycle(_, _) => write!(f, "cycle encountered in unification"),
            TypeMismatch(_, _) => write!(f, "type mismatch"),
            IncompatibleConstraints(_, _) => write!(f, "incompatible constraints"),
            ConstraintFailed(_, _) => write!(f, "type did not check constraint"),
            DomainMismatch(_, _, _) => write!(f, "function domain type mismatch"),
            CodomainMismatch(_, _, inner) => {
                write!(f, "function codomain type mismatch: {}", inner)
            }
            RecordMissingFields(_, _, _) => write!(f, "record missing fields"),
            VariantMissingFields(_, _) => write!(f, "variant type missing fields"),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use rowan::{ast::AstNode, TextRange};

    use crate::{
        ast,
        hir::{lower, Identifier, Span},
        parser,
        types::{ConcreteType, Type, TypeCheckError, TypeCheckErrorKind, Var},
    };

    use super::UnificationError;

    macro_rules! assert_matches {
        ($lhs:expr, $(|)? $( $pattern:pat_param )|+ $( if $guard: expr )? $(,)?) => {
            let lhs = $lhs;
            assert!(
                match lhs {
                    $( $pattern )|+ $( if $guard )? => true,
                    _ => false
                },
                "'{:?}' does not match pattern",
                lhs
            );
        }
    }

    fn type_check(input: &str) -> Result<Type, TypeCheckError> {
        let (node, errors) = parser::parse(input);
        assert!(errors.is_empty(), "parse error: {}", errors.join("\n"));
        let expr = lower(ast::Root::cast(node).expect("no root node"));
        super::type_check(&Default::default(), &expr)
    }

    fn assert_type_check_pretty_print(input: &str, expected: &str) {
        let actual = type_check(input);
        assert!(actual.is_ok(), "{:#}", actual.unwrap_err());
        assert_eq!(actual.unwrap().to_string(), expected);
    }

    #[test]
    fn check_literal() {
        assert_type_check_pretty_print("true", "Bool");
        assert_type_check_pretty_print("1", "Integer");
        assert_type_check_pretty_print("1.0", "Real");
    }

    #[test]
    fn check_lambda() {
        assert_type_check_pretty_print(r#"\x. x + 1"#, "Integer -> Integer");
    }

    #[test]
    fn check_nested_lambda() {
        assert_type_check_pretty_print(r#"\f. f 1 + 1"#, "(Integer -> Integer) -> Integer");
    }

    #[test]
    fn check_identifier() {
        assert_type_check_pretty_print(r#"let x = -1 in x"#, "Integer");
    }

    #[test]
    fn check_polymorphic() {
        assert_type_check_pretty_print(r#"\f.\x.\y. f y x"#, "(a -> b -> c) -> b -> a -> c");
    }

    #[test]
    fn check_annotation() {
        assert_type_check_pretty_print(
            r#"(\f.\x. f x) : (Integer -> Real) -> Integer -> Real"#,
            "(Integer -> Real) -> Integer -> Real",
        );
    }

    #[test]
    fn check_record() {
        assert_type_check_pretty_print(
            r#"{hello = 123, world = \x.x, fooBar = \y.y}"#,
            "{fooBar : a -> a, hello : Integer, world : b -> b}",
        );
    }

    #[test]
    fn check_empty_record_annotation() {
        assert_type_check_pretty_print(r#"{} : {}"#, "{}");
    }

    #[test]
    fn check_record_annotation() {
        assert_type_check_pretty_print(r#"{a = 123} : {a : Integer}"#, "{a : Integer}");
    }

    #[test]
    fn check_open_record() {
        assert_type_check_pretty_print(r#"\r. r.x"#, "{x : a | b} -> a");
    }

    #[test]
    fn check_let_multiple() {
        assert_type_check_pretty_print(r#"let x = 1 and y = x == 2 in y"#, "Bool");
    }

    #[test]
    fn check_let_shadowing() {
        assert_type_check_pretty_print(r#"let x = 1 and x = x == 2 in x"#, "Bool");
    }

    #[test]
    fn check_let_rec() {
        assert_type_check_pretty_print(
            r#"let rec f = \x. if x == 0 then x else f x in f"#,
            "Integer -> Integer",
        );
    }

    #[test]
    fn check_variant() {
        assert_type_check_pretty_print(r#"{|hello = 123|}"#, "{|hello : Integer | a|}");
    }

    #[test]
    fn check_empty_variant_annotation() {
        assert_type_check_pretty_print(r#"{||} : {||}"#, "{||}");
    }

    #[test]
    fn check_variant_annotation() {
        assert_type_check_pretty_print(
            r#"{|a = 123|} : {|a : Integer, b : Bool|}"#,
            "{|a : Integer, b : Bool|}",
        );
    }

    #[test]
    fn check_extended_variant() {
        assert_type_check_pretty_print(
            r#"case true of false -> {|a = true|}, true -> {|b = 1|}"#,
            "{|a : Bool, b : Integer | a|}",
        );
    }

    #[test]
    fn check_recursive() {
        assert_type_check_pretty_print(
            r#"
let rec map = \f.\xs.
	case xs of
		{| nil = {} |}
			-> {| nil = {} |},
		{| cons = { car = x, cdr = xr } |}
			-> {| cons = { car = f x, cdr = map f xr } |},
in map
            "#,
            r#"(a -> b) -> rec c. {|cons : {car : a, cdr : c}, nil : {} | d|} -> rec e. {|cons : {car : b, cdr : e}, nil : {} | f|}"#,
        );
    }

    #[test]
    fn check_not_generalize() {
        assert_eq!(
            type_check(r#"let id = \x.x in {a=id 1, b=id true}"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(31.into(), 35.into())),
                Type::Concrete(ConcreteType::Integer),
                Type::Concrete(ConcreteType::Bool),
            ))
        );
    }

    #[test]
    fn check_forall() {
        assert_type_check_pretty_print(
            r#"let id : forall a. a -> a = \x.x in {a = id 1, b = id true}"#,
            r#"{a : Integer, b : Bool}"#,
        );
    }

    #[test]
    fn check_bad_forall_concrete_type() {
        assert_eq!(
            type_check(r#"let id : forall a. a -> Integer = \x.x in {a=id 1, b=id true}"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(19.into(), 31.into())),
                Type::Concrete(ConcreteType::Fn {
                    lhs: Box::new(Type::Var(Var::from(0))),
                    rhs: Box::new(Type::Concrete(ConcreteType::Integer)),
                }),
                Type::Concrete(ConcreteType::Fn {
                    lhs: Box::new(Type::Concrete(ConcreteType::Integer)),
                    rhs: Box::new(Type::Concrete(ConcreteType::Integer)),
                }),
            ))
        );
    }

    #[test]
    fn check_bad_forall_different_variables() {
        assert_eq!(
            type_check(r#"let id : forall a. forall b. a -> b = \x.x in {a=id 1, b=id true}"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(29.into(), 35.into())),
                Type::Concrete(ConcreteType::Fn {
                    lhs: Box::new(Type::Var(Var::from(0))),
                    rhs: Box::new(Type::Var(Var::from(1))),
                }),
                Type::Concrete(ConcreteType::Fn {
                    lhs: Box::new(Type::Var(Var::from(0))),
                    rhs: Box::new(Type::Var(Var::from(0))),
                }),
            ))
        );
    }

    #[test]
    fn check_rec_annotation() {
        assert_type_check_pretty_print(
            r#"let n : rec tail. {| nil : {}, cons : {car : Integer, cdr : tail} |}
            = {| cons = { car = 1, cdr = {| nil = {} |} } |} in n"#,
            r#"rec a. {|cons : {car : Integer, cdr : a}, nil : {}|}"#,
        );
    }

    #[test]
    fn check_rec_annotation_mismatch() {
        assert_eq!(
            type_check(
                r#"let n : rec tail. {| nil : {}, cons : {car : Integer, cdr : tail} |}
                = {| cons = { car = 1, cdr = {| nil = 1 |} } |} in n"#
            ),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(8.into(), 68.into())),
                Type::Concrete(ConcreteType::EmptyRecord),
                Type::Concrete(ConcreteType::Integer),
            ))
        );
    }

    #[test]
    fn check_two_rec_annotations() {
        assert_type_check_pretty_print(
            r#"
let cons : forall a.
      a
      -> rec rest. {| cons : { car : a, cdr : rest }, nil : {} |}
      -> rec rest. {| cons : { car : a, cdr : rest }, nil : {} |}
    = \x.\xr. {| cons = { car = x, cdr = xr } |} in cons
                "#,
            r#"a -> rec b. {|cons : {car : a, cdr : b}, nil : {}|} -> rec c. {|cons : {car : a, cdr : c}, nil : {}|}"#,
        );
    }

    #[test]
    fn check_polymorphic_constraint() {
        assert_eq!(
            type_check(r#"\x. x + x"#),
            Err(TypeCheckError::remaining_constraints(Span::Range(
                TextRange::new(0.into(), 9.into())
            )))
        );
    }

    #[test]
    fn check_unbound_identifier() {
        assert_eq!(
            type_check(r#"xyz"#),
            Err(TypeCheckError::unbound_identifier(
                Span::Range(TextRange::new(0.into(), 3.into())),
                Identifier::from("xyz")
            ))
        );
    }

    #[test]
    fn check_condition_wrong_type() {
        assert_eq!(
            type_check(r#"if 1 then true else false"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(3.into(), 4.into())),
                Type::Concrete(ConcreteType::Bool),
                Type::Concrete(ConcreteType::Integer),
            ))
        );
    }

    #[test]
    fn check_condition_branches_differ() {
        assert_eq!(
            type_check(r#"if true then 1 else 1.0"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(20.into(), 23.into())),
                Type::Concrete(ConcreteType::Integer),
                Type::Concrete(ConcreteType::Real),
            ))
        );
    }

    #[test]
    fn check_cycle() {
        // Fixed point combinator contains a cycle
        assert_matches!(
            type_check(r#"\f. (\x. f (x x)) (\x. f (x x))"#),
            Err(TypeCheckError {
                span: Span::None,
                kind: TypeCheckErrorKind::UnificationError(UnificationError::Cycle(_, _))
            })
        );
    }

    #[test]
    fn check_type_mismatch() {
        assert_eq!(
            type_check(r#"1 + 1.1"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(4.into(), 7.into())),
                Type::Concrete(ConcreteType::Integer),
                Type::Concrete(ConcreteType::Real),
            ))
        );
    }

    #[test]
    fn check_type_constraint_failed() {
        assert_eq!(
            type_check(r#"true + 1"#),
            Err(TypeCheckError::constraint_failed(
                Span::Range(TextRange::new(0.into(), 4.into())),
                Type::Concrete(ConcreteType::Bool),
                HashSet::from([ConcreteType::Integer, ConcreteType::Real])
            ))
        );
    }

    #[test]
    fn check_wrong_annotation() {
        assert_eq!(
            type_check(r#"true : Integer"#),
            Err(TypeCheckError::type_mismatch(
                Span::Range(TextRange::new(0.into(), 4.into())),
                Type::Concrete(ConcreteType::Bool),
                Type::Concrete(ConcreteType::Integer),
            ))
        );
    }

    #[test]
    fn check_bad_annotation() {
        assert_matches!(
            type_check(r#"true : Integer + 2"#),
            Err(TypeCheckError {
                kind: TypeCheckErrorKind::BadTypeAnnotation(_),
                span: _,
            })
        );
    }

    #[test]
    fn check_record_missing_field() {
        assert_matches!(
            type_check(r#"(\r. r.x) {y = 1}"#),
            Err(TypeCheckError {
                kind: TypeCheckErrorKind::TypeMismatch(..),
                span: _,
            })
        );
    }

    #[test]
    fn check_record_field_type_mismatch() {
        assert_matches!(
            type_check(r#"(\r. r.x + 1) {x = true}"#),
            Err(TypeCheckError {
                kind: TypeCheckErrorKind::TypeMismatch(..),
                span: _,
            })
        );
    }
}
