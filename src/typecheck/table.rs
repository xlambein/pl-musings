use std::collections::{BTreeSet, HashSet};

use crate::types::ConcreteType;

use super::{Constraint, UnifType, UnifVar, UnificationError};

/// Kind of type variable occurrence found when doing an "occurs" check.
#[derive(Clone, Copy)]
enum Occurrence {
    /// Variable doesn't occur in type.
    None,
    /// Variable occurs in type, but only in some variants.
    Indirect,
    /// Variable occurs in type in all variants.
    Direct,
}

impl Occurrence {
    /// Combine two [`Occurrence`]s, `or`-ing their "directness".
    fn or(self, other: Self) -> Self {
        match (self, other) {
            (_, Occurrence::Direct) | (Occurrence::Direct, _) => Occurrence::Direct,
            (_, Occurrence::Indirect) | (Occurrence::Indirect, _) => Occurrence::Indirect,
            (Occurrence::None, Occurrence::None) => Occurrence::None,
        }
    }

    /// Combine two [`Occurrence`]s, `and`-ing their "directness".
    ///
    /// If both occurrences are the same, the result will be the same too.
    /// Otherwise the result will be [`Occurrence::Indirect`].
    fn and(self, other: Self) -> Self {
        match (self, other) {
            (Occurrence::None, Occurrence::None) => Occurrence::None,
            (Occurrence::None, _) | (_, Occurrence::None) => Occurrence::Indirect,
            (Occurrence::Indirect, _) | (_, Occurrence::Indirect) => Occurrence::Indirect,
            (Occurrence::Direct, Occurrence::Direct) => Occurrence::Direct,
        }
    }
}

/// A set where the presence of a value is linked to a specific scope.
///
/// Adding a value to a [`ScopedSet`] creates a new borrowed [`ScopedSet`]
/// which, when dropped, will remove the value from the set.  This ensures
/// that the value exists in the set only as long as the reference is live.
enum ScopedSet<'s, T: Clone + std::cmp::Eq + std::hash::Hash> {
    Owned(HashSet<T>),
    Borrowed { set: &'s mut HashSet<T>, value: T },
}

impl<'s, T: Clone + std::cmp::Eq + std::hash::Hash> Drop for ScopedSet<'s, T> {
    fn drop(&mut self) {
        match self {
            Self::Owned(_) => {}
            Self::Borrowed { set, value } => {
                if !set.remove(&value) {
                    panic!("value already removed from scope")
                }
            }
        }
    }
}

impl<'s, T: Clone + std::cmp::Eq + std::hash::Hash> ScopedSet<'s, T> {
    /// Create an empty [`ScopedSet`].
    pub fn new() -> Self {
        Self::Owned(HashSet::new())
    }

    /// Create a new [`ScopedSet`] that ensures a given value is in the set.
    ///
    /// If the set already contained the value, it means another scope already
    /// "owns" this value, and so `None` is returned.
    pub fn with<'b>(&'b mut self, value: T) -> Option<ScopedSet<'b, T>>
    where
        's: 'b,
    {
        let set = match self {
            ScopedSet::Owned(ref mut set) => set,
            ScopedSet::Borrowed { set, .. } => set,
        };
        if set.insert(value.clone()) {
            Some(ScopedSet::Borrowed { value, set })
        } else {
            None
        }
    }

    /// Check whether the [`ScopedSet`] contains the given value.
    pub fn contains<'b>(&'b mut self, value: &T) -> bool
    where
        's: 'b,
    {
        let set = match self {
            ScopedSet::Owned(ref mut set) => set,
            ScopedSet::Borrowed { set, .. } => set,
        };
        set.contains(value)
    }
}

impl<'s, T: Clone + std::cmp::Eq + std::hash::Hash> Default for ScopedSet<'s, T> {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
struct Node {
    value: UnifType,
    size: usize,
}

#[derive(Debug)]
pub struct UnificationTable {
    parent: Vec<Node>,
    constraints: Vec<Option<Constraint>>,
}

impl UnificationTable {
    pub fn new() -> Self {
        Self {
            parent: vec![],
            constraints: vec![],
        }
    }

    pub fn find_deep(&mut self, ty: UnifType) -> UnifType {
        self.find_deep_(ty, &mut ScopedSet::new())
    }

    fn find_deep_(&mut self, ty: UnifType, visited: &mut ScopedSet<'_, UnifVar>) -> UnifType {
        match self.find(ty) {
            UnifType::Concrete(cty) => {
                UnifType::Concrete(cty.map(|ty| Box::new(self.find_deep_(*ty, visited))))
            }
            ty @ UnifType::Var(_) => ty,
            UnifType::Recursive(mut v, inner) => {
                while let UnifType::Var(u) = &self.parent[v.0].value {
                    v = u.clone();
                }
                if let Some(mut visited) = visited.with(v) {
                    UnifType::Recursive(v, Box::new(self.find_deep_(*inner, &mut visited)))
                } else {
                    UnifType::Var(v)
                }
            }
        }
    }

    pub fn find(&mut self, ty: UnifType) -> UnifType {
        self.find_(ty, &mut ScopedSet::new())
    }

    fn find_(&mut self, ty: UnifType, visited: &mut ScopedSet<'_, UnifVar>) -> UnifType {
        match ty {
            UnifType::Var(v) => match &self.parent[v.0].value {
                UnifType::Var(u) if *u == v => UnifType::Var(v),
                ty => {
                    let ty = ty.clone();
                    self.find_(ty, visited)
                }
            },
            ty => ty,
        }
    }

    pub fn constraints_on(&self, UnifVar(i): UnifVar) -> Option<&Constraint> {
        self.constraints.get(i)?.as_ref()
    }

    fn occurs(&self, var: UnifVar, ty: &UnifType) -> Occurrence {
        match ty {
            UnifType::Recursive(v, inner) => {
                if *v == var {
                    Occurrence::Direct
                } else {
                    self.occurs(var, inner)
                }
            }
            UnifType::Var(other) => {
                if *other == var {
                    Occurrence::Direct
                } else {
                    Occurrence::None
                }
            }
            UnifType::Concrete(ty) => match ty {
                ConcreteType::Fn { lhs, rhs } => self.occurs(var, lhs).or(self.occurs(var, rhs)),
                ConcreteType::OpenRecord { ty, rest, .. } => {
                    self.occurs(var, ty).or(self.occurs(var, rest))
                }
                ConcreteType::OpenVariant { ty, rest, .. } => {
                    self.occurs(var, ty).and(self.occurs(var, rest))
                }
                ConcreteType::Type
                | ConcreteType::Bool
                | ConcreteType::Integer
                | ConcreteType::Real
                | ConcreteType::EmptyRecord
                | ConcreteType::EmptyVariant => Occurrence::None,
            },
        }
    }

    pub fn unify(&mut self, ty0: UnifType, ty1: UnifType) -> Result<(), UnificationError> {
        self.unify_(ty0, ty1, &mut ScopedSet::new())
    }

    fn unify_vars(&mut self, u: UnifVar, v: UnifVar) -> Result<(), UnificationError> {
        // If variables are the same, exit early
        if u == v {
            return Ok(());
        }

        // Parent will be the bigger tree
        let (u, v) = if self.parent[u.0].size < self.parent[v.0].size {
            (v, u)
        } else {
            (u, v)
        };

        // Unify constraints on both variables
        let constraints = match (self.constraints[u.0].take(), self.constraints[v.0].take()) {
            (None, None) => None,
            (None, Some(constraints)) | (Some(constraints), None) => Some(constraints),
            (Some(c0), Some(c1)) => {
                let c: HashSet<_> = c0.intersection(&c1).cloned().collect();
                if c.is_empty() {
                    return Err(UnificationError::IncompatibleConstraints(c0, c1));
                } else {
                    Some(c)
                }
            }
        };
        self.constraints[u.0] = constraints;

        // Set the size of `u` to be the size of both trees
        self.parent[u.0].size = self.parent[u.0].size + self.parent[v.0].size;

        // Set the parent of `v` to `u`
        self.parent[v.0].value = UnifType::Var(u);

        Ok(())
    }

    fn unify_(
        &mut self,
        ty0: UnifType,
        ty1: UnifType,
        visited: &mut ScopedSet<'_, (UnifVar, UnifVar)>,
    ) -> Result<(), UnificationError> {
        let ty0 = self.find(ty0);
        let ty1 = self.find(ty1);

        match (ty0, ty1) {
            (UnifType::Concrete(cty0), UnifType::Concrete(cty1)) => match (cty0, cty1) {
                (ConcreteType::Type, ConcreteType::Type) => Ok(()),
                (ConcreteType::Bool, ConcreteType::Bool) => Ok(()),
                (ConcreteType::Integer, ConcreteType::Integer) => Ok(()),
                (ConcreteType::Real, ConcreteType::Real) => Ok(()),
                (ConcreteType::EmptyRecord, ConcreteType::EmptyRecord) => Ok(()),
                (ConcreteType::EmptyVariant, ConcreteType::EmptyVariant) => Ok(()),
                (
                    ConcreteType::Fn {
                        lhs: lhs0,
                        rhs: rhs0,
                    },
                    ConcreteType::Fn {
                        lhs: lhs1,
                        rhs: rhs1,
                    },
                ) => {
                    let (lhs0, rhs0, lhs1, rhs1) = (*lhs0, *rhs0, *lhs1, *rhs1);
                    self.unify_(lhs0.clone(), lhs1.clone(), visited)
                        .map_err(|err| {
                            UnificationError::DomainMismatch(
                                self.find_deep(lhs0),
                                self.find_deep(lhs1),
                                Box::new(err),
                            )
                        })?;
                    self.unify_(rhs0.clone(), rhs1.clone(), visited)
                        .map_err(|err| {
                            UnificationError::CodomainMismatch(
                                self.find_deep(rhs0),
                                self.find_deep(rhs1),
                                Box::new(err),
                            )
                        })
                }
                (
                    cty0 @ ConcreteType::OpenRecord { .. },
                    cty1 @ ConcreteType::OpenRecord { .. },
                ) => {
                    let ty0 = UnifType::Concrete(cty0);
                    let ty1 = UnifType::Concrete(cty1);

                    let fields0 = ty0.fields();
                    let fields1 = ty1.fields();

                    let keys0 = BTreeSet::from_iter(fields0.keys().copied());
                    let keys1 = BTreeSet::from_iter(fields1.keys().copied());

                    // Unify the type of keys in common
                    let keys_common = keys0.intersection(&keys1);
                    for key in keys_common {
                        self.unify_(fields0[key].clone(), fields1[key].clone(), visited)?;
                    }

                    // Add missing fields to left type
                    let missing0 = keys1.difference(&keys0).copied();
                    let mut rest0 = ty0.fields_rest().clone();
                    if let UnifType::Concrete(ConcreteType::EmptyRecord) = rest0 {
                        let missing0: Vec<_> = missing0.cloned().collect();
                        if !missing0.is_empty() {
                            return Err(UnificationError::RecordMissingFields(
                                self.find_deep(ty0.clone()),
                                self.find_deep(ty1.clone()),
                                missing0,
                            ));
                        }
                    } else {
                        for key in missing0 {
                            let rest = UnifType::Var(self.new_type_var());
                            let open_record = UnifType::Concrete(ConcreteType::OpenRecord {
                                field: key.clone(),
                                ty: Box::new(fields1[key].clone()),
                                rest: Box::new(rest.clone()),
                            });
                            self.unify_(rest0.clone(), open_record, visited)?;
                            rest0 = rest;
                        }
                    }

                    // Add missing fields to right type
                    let missing1 = keys0.difference(&keys1).copied();
                    let mut rest1 = ty1.fields_rest().clone();
                    if let UnifType::Concrete(ConcreteType::EmptyRecord) = rest1 {
                        let missing1: Vec<_> = missing1.cloned().collect();
                        if !missing1.is_empty() {
                            return Err(UnificationError::RecordMissingFields(
                                self.find_deep(ty1.clone()),
                                self.find_deep(ty0.clone()),
                                missing1,
                            ));
                        }
                    } else {
                        for key in missing1 {
                            let rest = UnifType::Var(self.new_type_var());
                            let open_record = UnifType::Concrete(ConcreteType::OpenRecord {
                                field: key.clone(),
                                ty: Box::new(fields0[key].clone()),
                                rest: Box::new(rest.clone()),
                            });
                            self.unify_(rest1.clone(), open_record, visited)?;
                            rest1 = rest;
                        }
                    }

                    // Make sure the two types stay in sync by ensuring their
                    // rest is the same.  This also closes the other record if
                    // one is already closed.
                    self.unify_(rest0, rest1, visited)
                }
                (
                    cty0 @ ConcreteType::OpenVariant { .. },
                    cty1 @ ConcreteType::OpenVariant { .. },
                ) => {
                    let ty0 = UnifType::Concrete(cty0);
                    let ty1 = UnifType::Concrete(cty1);

                    let fields0 = ty0.fields();
                    let fields1 = ty1.fields();

                    let keys0 = BTreeSet::from_iter(fields0.keys().copied());
                    let keys1 = BTreeSet::from_iter(fields1.keys().copied());

                    // Unify the type of keys in common
                    let keys_common = keys0.intersection(&keys1);
                    for key in keys_common {
                        self.unify_(fields0[key].clone(), fields1[key].clone(), visited)?;
                    }

                    // Add missing fields to left type
                    let missing0 = keys1.difference(&keys0).copied();
                    let mut rest0 = ty0.fields_rest().clone();
                    if let UnifType::Concrete(ConcreteType::EmptyVariant) = rest0 {
                        let missing0: Vec<_> = missing0.cloned().collect();
                        if !missing0.is_empty() {
                            return Err(UnificationError::VariantMissingFields(
                                self.find_deep(ty0.clone()),
                                missing0,
                            ));
                        }
                    } else {
                        for key in missing0 {
                            let rest = UnifType::Var(self.new_type_var());
                            let open_variant = UnifType::Concrete(ConcreteType::OpenVariant {
                                field: key.clone(),
                                ty: Box::new(fields1[key].clone()),
                                rest: Box::new(rest.clone()),
                            });
                            self.unify_(rest0.clone(), open_variant, visited)?;
                            rest0 = rest;
                        }
                    }

                    // Add missing fields to right type
                    let missing1 = keys0.difference(&keys1).copied();
                    let mut rest1 = ty1.fields_rest().clone();
                    if let UnifType::Concrete(ConcreteType::EmptyVariant) = rest1 {
                        let missing1: Vec<_> = missing1.cloned().collect();
                        if !missing1.is_empty() {
                            return Err(UnificationError::VariantMissingFields(
                                self.find_deep(ty1.clone()),
                                missing1,
                            ));
                        }
                    } else {
                        for key in missing1 {
                            let rest = UnifType::Var(self.new_type_var());
                            let open_variant = UnifType::Concrete(ConcreteType::OpenVariant {
                                field: key.clone(),
                                ty: Box::new(fields0[key].clone()),
                                rest: Box::new(rest.clone()),
                            });
                            self.unify_(rest1.clone(), open_variant, visited)?;
                            rest1 = rest;
                        }
                    }

                    // Make sure the two types stay in sync by ensuring their
                    // rest is the same.  This also closes the other variant if
                    // one is already closed.
                    self.unify_(rest0, rest1, visited)
                }
                (ty0, ty1) => Err(UnificationError::TypeMismatch(
                    self.find_deep(UnifType::Concrete(ty0)),
                    self.find_deep(UnifType::Concrete(ty1)),
                )),
            },
            (UnifType::Recursive(u, inner0), UnifType::Recursive(v, inner1)) => {
                // Ensure we only check this pair once
                if let Some(mut visited) = visited.with((u, v)) {
                    self.unify_vars(u, v)?;
                    self.unify_(*inner0, *inner1, &mut visited)?;
                }
                Ok(())
            }
            (UnifType::Recursive(u, _), UnifType::Var(v))
            | (UnifType::Var(v), UnifType::Recursive(u, _)) => {
                let constraints = match (self.constraints[u.0].take(), self.constraints[v.0].take())
                {
                    (None, None) => None,
                    (None, Some(constraints)) | (Some(constraints), None) => Some(constraints),
                    (Some(c0), Some(c1)) => {
                        let c: HashSet<_> = c0.intersection(&c1).cloned().collect();
                        if c.is_empty() {
                            return Err(UnificationError::IncompatibleConstraints(c0, c1));
                        } else {
                            Some(c)
                        }
                    }
                };
                self.constraints[u.0] = constraints;

                self.parent[v.0].value = UnifType::Var(u);
                Ok(())
            }
            (UnifType::Recursive(_, inner), ty @ UnifType::Concrete(_))
            | (ty @ UnifType::Concrete(_), UnifType::Recursive(_, inner)) => {
                self.unify_(*inner, ty, visited)?;
                Ok(())
            }
            (UnifType::Var(u), UnifType::Var(v)) => self.unify_vars(u, v),
            (UnifType::Var(v), ty @ UnifType::Concrete(_))
            | (ty @ UnifType::Concrete(_), UnifType::Var(v)) => {
                if let Some(constraints) = self.constraints[v.0].take() {
                    if let UnifType::Concrete(inner) = &ty {
                        if !constraints.contains(&inner.clone().map(|_| ())) {
                            return Err(UnificationError::ConstraintFailed(
                                self.find_deep(ty.clone()),
                                constraints,
                            ));
                        }
                    } else {
                        unreachable!("already covered by other patterns")
                    }
                }
                // Check whether the variable occurs in `ty`
                let ty_deep = self.find_deep(ty.clone());
                match self.occurs(v, &ty_deep) {
                    Occurrence::None => {
                        // If it doesn't, set `ty` as its parent
                        self.parent[v.0].value = ty;
                    }
                    Occurrence::Indirect => {
                        // If it does in some variants, make `v` recursive
                        self.parent[v.0].value = UnifType::Recursive(v.clone(), Box::new(ty));
                    }
                    Occurrence::Direct => {
                        // If it does in all variants, raise an error (this
                        // type cannot be constructed)
                        return Err(UnificationError::Cycle(v, self.find_deep(ty)));
                    }
                }
                Ok(())
            }
        }
    }

    pub fn new_type_var(&mut self) -> UnifVar {
        let v = UnifVar(self.parent.len());
        self.parent.push(Node {
            value: UnifType::Var(v),
            size: 1,
        });
        self.constraints.push(None);
        v
    }

    pub fn new_type_var_with_constraints(
        &mut self,
        constraints: HashSet<ConcreteType<()>>,
    ) -> UnifVar {
        let v = UnifVar(self.parent.len());
        self.parent.push(Node {
            value: UnifType::Var(v),
            size: 1,
        });
        self.constraints.push(Some(constraints));
        v
    }
}
