use assert_cli::Assert;

#[test]
fn test_repl() {
    // Repl exits without error
    Assert::main_binary().unwrap();

    // Repl skips whitespaces
    Assert::main_binary().stdin("    \n\n").unwrap();

    // Repl can do one eval
    Assert::main_binary()
        .stdin("2 + 5")
        .stdout()
        .is("7 : Integer")
        .unwrap();

    // Repl can do multiple evals
    Assert::main_binary()
        .stdin("1 - 3\n4 / 2")
        .stdout()
        .is("-2 : Integer\n2 : Integer")
        .unwrap();

    // Repl handles multiline expressions
    Assert::main_binary()
        .stdin("1 -\n3")
        .stdout()
        .is("-2 : Integer")
        .unwrap();

    // Repl displays errors properly
    Assert::main_binary()
        .stdin(
            "let y = 10
in xyz",
        )
        .stderr()
        .contains(
            "\x1b[1;31merror\x1b[0m: unbound identifier `xyz`
in REPL, on line 2, column 4:

    in xyz
       \x1b[1;31m^^^\x1b[0m
",
        )
        .unwrap();
}
